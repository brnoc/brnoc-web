var user;
var data;

   function set_status(text, col = "green"){
     document.getElementById('statusBar').innerHTML = text;
     document.getElementById('statusBar').className = (col);
   }

   function fullscreen(){
     document.getElementById('attendanceSection').classList.toggle('fullscreen');
     document.getElementById('fullscreen').classList.toggle('inv');
     document.getElementById('compress').classList.toggle('inv');
   }

   //load and process data
   function load(){
     $.ajax({url: host + 'API/users/get?&columns=["id","firstname","lastname","email","img","contact","speaker","organizer","socks","paid","attended","created","age","jizdne","vs","released","consent"]',
     success: function(result){
       set_status(result.message, "green");
       var rows = [];

       data = result.data;

       Array.prototype.forEach.call(data, row => {

         var keys = ["id","organizer","speaker","attended","released","consent","jizdne","paid"];
         keys.forEach(key => {
           row[key] = parseInt(row[key]);
         });
       })



       Array.prototype.forEach.call(data, row => {
         var s = 0;
         row.socks = JSON.parse(row.socks);
         row.socks.forEach(i => s+=i);

         var pay = 50;
         if(row.organizer == "1" || row.speaker == "1"){
           pay -= 50;
           if(s > 0)pay -= 75;
         }
         pay += s*75;
         pay -= parseInt(row.paid);
         row.pay = pay;

       });
       render(data);
     },
     error: function(xhr,status,error){
       console.log(xhr);
       set_status(status + " "+ xhr.status +" " + ": " + error + ". See more info in console.", "red");
     }
  });

   }

   function reproces_user(row){
     var s = 0;
     row.socks.forEach(i => s+=i);

     var pay = 50;

     if(row.organizer == "1" || row.speaker == "1"){
       pay -= 50;
       if(s > 0)pay -= 75;
     }

     pay += s*75;
     pay -= parseInt(row.paid);
     row.pay = pay;
   }

   function compare(a,b,key,ord = 1){
     if(a[key] > b[key]) return 1*ord;
     if(a[key] < b[key]) return -1*ord;
     return 0;
   }

   var sortKey = "id";
   var sortDir = true;
   function sort(col){
     if(sortKey == col) sortDir = !sortDir;
     else sortKey = col;


    if(sortDir)data.sort((a,b) => compare(a,b,col));
    else data.sort((a,b) => compare(b,a,col));

     render(data);
   }

   function search(){
     var s = document.getElementById('serchBar').value.toLowerCase();
     var dataNew = [];
     data.forEach(row => {
       name = row.firstname + row.lastname;
       if(name.toLowerCase().includes(s)) dataNew.push(row);
     });
     render(dataNew);

   }

   function open_user_detail(id){
     document.getElementById('cover').classList.remove('inv');
     var user_detail = document.getElementById('userDetail');
     user_detail.classList.remove('inv');

     data.forEach(row => {if(row.id == id) user = row});

     $("#userDetailName").html(user.firstname + " " + user.lastname);
     var speaker = document.getElementById("userDetailSpeakerIcon");
     var org = document.getElementById("userDetailOrgIcon");
     var attended = document.getElementById("userDetailAttended");
     var orders = document.getElementById('ordersTable');
     var pay = document.getElementById('userDetailPay');
     document.getElementById('userDetailPayInputWrapper').classList.remove("tableDisplay");
     document.getElementById('userDetailPayInput').value="";

     consent = document.getElementById('userDetailConsentWrapper');

     speaker.classList.remove("off");
     speaker.classList.remove("yes");
     org.classList.remove("off");
     org.classList.remove("yes");
     attended.classList.remove("no");
     attended.classList.remove("yes");
     orders.classList.remove("no");
     orders.classList.remove("yes");
     pay.classList.remove("no");
     pay.classList.remove("yes");
     pay.classList.remove("over");
     consent.classList.remove('plus18');
     consent.classList.remove('consF');
     consent.classList.remove('consT');

     speaker.classList.add((user.speaker == 1)?"yes":"off");
     org.classList.add((user.organizer == 1)?"yes":"off");
     attended.classList.add((user.attended == 1)?"yes":"no");
     consent.classList.add((user.age == 3)?"plus18":(user.consent == 1)?"consT":"consF");

     pay.classList.add((user.pay > 0)?"no":(user.pay < 0)?"over":"yes");
     $("#pay").html(user.pay);

     var all = 0;
     for(i = 0; i < 5; i++){
       all += user.socks[i];
       orders.children[1].children[0].children[i].innerHTML = user.socks[i];
     }

     orders.classList.add((all > 0)?(user.released == 1)?"yes":"no":"yes");


     console.log(user);
   }

   function close_users_detail(){
     document.getElementById('cover').classList.add('inv');
     document.getElementById('userDetail').classList.add('inv');
   }

   function toggle(col){
     var txt = "přidat";
     var set = 1;
     if(user[col] == 1){
      txt = "odebrat";
      set = 0;
      }
     //if(!confirm("Opravdu chcete "+txt+" roli "+col+" uživateli "+user.firstname+" "+user.lastname+"?"))return;
     $.ajax({
       url:"<?=base_url()?>API/users/set",
       type: "POST",
       data: {
         columns:'[["'+col+'",'+set+']]',
         where: '[["id","=",'+user.id+']]',
       },
       success: (response) => {
         set_status(response.status+": "+response.message+" ("+response.data+")")
         console.log(response);
         user[col] = set;
         reproces_user(user);
         render(data);
         open_user_detail(user.id);
       },
       error: function(xhr,status,error){
         console.log(xhr);
         set_status(status + " "+ xhr.status +" " + ": " + error + ". See more info in console.", "red");
       }
     });
   }

   function toggle_release(){
     if(user.pay > 0)
      alert("Ponožky nemohou být vydány, dokud účastník nezaplatí.");
      else toggle("released");
   }

   function open_pay_form(){
     document.getElementById('userDetailPayInputWrapper').classList.add("tableDisplay");
   }

   function pay(){
     document.getElementById('userDetailPayInputWrapper').classList.remove("tableDisplay");
     amount = parseInt(document.getElementById('userDetailPayInput').value);
     amount = isNaN(amount)?0:amount;

     var paid = parseInt(user.paid) + amount;

     $.ajax({
       type: "POST",
       url: host+"API/users/set",
       data:{
         columns:'[["paid",'+paid+']]',
         where: '[["id","=",'+user.id+']]',
       },
       success:(response) => {
         set_status(response.status+": "+response.message+" ("+response.data+")")
         console.log(response);
         user.paid = paid;
         reproces_user(user);
         render(data);
         open_user_detail(user.id);
       },
       error:(xhr,status,error) => {
         console.log(xhr);
         set_status(status + " "+ xhr.status +" " + ": " + error + ". See more info in console.", "red");
       },
     })



   }
