let lectures = [];
let ttTable = [];
let lectureId2Index = [];
let SHAPE = "x5:0x5:44x4:32x2:2";
// const SHAPE = "x3:466x2:44|x4:664";
let splits = new Set();
let pauses = [];
loadLectures();
generateTimetable(SHAPE);
let mergedCells = {};
let dragging = false;

haluzitko.onUpdate = function (score, bestTable, iterProg, updateTable=true) {
    setScore(score, iterProg);
    if(updateTable){
        putToTimeTable(bestTable);
    }
    ttTable = bestTable;
};

haluzitko.onUpdateTable = function (newTable) {
    ttTable = newTable;
    updateCollisions();
};

haluzitko.onFinished = function (newTable) {
    let progLine = document.getElementById("progressLine");
    progLine.style.width = "";
    ttTable = newTable;
    putToTimeTable(ttTable);
    updateCollisions();
};

function setUITable(newTTable) {
    getScore(newTTable, haluzitko.votes, haluzitko.restrictions, true);
    putToTimeTable(newTTable);
    ttTable = newTTable;
}

dragula({
    isContainer: function (el) {
        return el.classList.contains('ttEntry') || el.classList.contains('lectureStore');
    },
    moves: function (el, source, handle, sibling) {
        return true;
    },
    accepts: function (el, target, source, sibling) {
        if(target.classList.contains("ttEntry")) {
            let cellPos = getCellPos(target);
            let len = getLectLen(el);
            let index = getLectIndex(el);

            if( !canFit(cellPos, len, index, ttTable)){
                return false;
            }

            return canPlaceRules(lectures[index], cellPos, ttTable);
        }
        return true;
    },
    invalid: function (el, handle) {
        return false;
    },
    direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
    copy: false,                       // elements are moved by default, not copied
    copySortSource: false,             // elements in copy-source containers can be reordered
    revertOnSpill: false,              // spilling will put the element back where it was dragged from, if this is true
    removeOnSpill: false,              // spilling will `.remove` the element, if this is true
    mirrorContainer: document.body,    // set the element that gets mirror elements appended
    ignoreInputTextSelection: true     // allows users to select input text, see details below
})
    .on("shadow", function (el, container, source) {
        let index = getLectIndex(el);
        let lecture = lectures[index];

        if(lecture.x !== undefined)
        {
            let pos = {x: lecture.x, y: lecture.y};
            for (let i=0; i<lecture.segments; i++) {
                ttTable[pos.y + i][pos.x] = undefined;
            }
            lecture.x = undefined;
            lecture.y = undefined;
            lecture.hx = undefined;
            lecture.hy = undefined;
        }

        if(container.classList.contains("ttEntry")){

            let pos = getCellPos(container);
            lecture.x = pos.x;
            lecture.y = pos.y;
            lecture.hx = pos.x;
            lecture.hy = pos.y;
            for (let i=0; i<lecture.segments; i++) {
                ttTable[pos.y + i][pos.x] = lecture;
            }
        }

        updateCollisions();
    })
    .on("drag", function(el, source){
        dragging = true;
    })
    .on("drop", function(el, target, source, sibling){
        if(target.classList.contains("lectureStore")){
            sortStore()
        }
    })
    .on("dragend", function(el){
        dragging = false;
        getScore(ttTable, haluzitko.votes, haluzitko.restrictions, true);
        updateCollisions();
    })
    .on("out", function (el, container,  source) {
    });

document.addEventListener('event', (event) => {
    event = event || window.event;
    if(event.ctrlKey) {
        alert("Ctrl");
    }
});

function resetShape() {
    let newShape = prompt("nastavte nový tvar\n" +
        "xn:abc - nastaví šířku na n, vytvoří bloky o velikosti a, b, c segmentů\n" +
        "| - prodloužení pauzy o jeden blok\n" +
        "příklad: x4:466|x2:4\n\n" +
        "(vyprázdní aktuální rozvrh)", SHAPE);

    console.log(newShape);

    if (newShape !== null) {
        setShape(newShape);
    }
}

function setShape(shape) {
    haluzitko.clearTT();
    SHAPE = shape;
    ttTable.length = 0;
    splits.clear();
    pauses.length = 0;
    let table =document.getElementById("timeTable");

    while ( table.rows.length > 1 ) {
        table.deleteRow(1);
    }

    generateTimetable(SHAPE);
}

function generateTimetable( shape ){
    let table = document.getElementById("timeTable");

    let pauseCount = 0;
    let lastPause = false;

    let width = 1;
    let setWidth = false;
    let rowNum = 0;
    for(let i=0; i<shape.length; i++){
        let curLastPause = lastPause;
        lastPause = false;

        let c = shape[i];
        if(c === "x"){
            setWidth = true;
            continue;
        }
        if(setWidth){
            width = +c;
            setWidth = false;
            i++;
            continue;
        }
        if(c !== "|"){
            curLastPause = false;
            for(let dy=0; dy<+c; dy++){
                ttTable.push(new Array(width));
                let tr = table.insertRow();
                tr.id = "ttRow"+rowNum;
                for(let x=0; x<width; x++)
                {
                    let td = document.createElement("td");
                    td.id = "ttCell" + x + "-" + rowNum;
                    td.classList.add("ttEntry");
                    tr.appendChild(td);
                }
                rowNum++;
            }
        }
        splits.add(rowNum);
        if (curLastPause) {
            pauses[pauses.length - 1].duration++;
        } else {
             pauses.push({
                from: rowNum + pauseCount,
                duration: 1,
            });
        }
        pauseCount++;
        lastPause = true;

        let split = table.insertRow();
        split.classList.add("split");
        for(let x=0; x<width; x++) {
            let td = document.createElement("td");
            td.classList.add("split");
            split.appendChild(td);
        }
    }
}

function getCellPos( cellEl ) {
    let cellIndex = cellEl.id.replace("ttCell", "");
    let indexList = cellIndex.split("-");

    return {x: +indexList[0], y: +indexList[1]};
}

function getLectIndex( lectEl ) {
    let index = lectEl.id.replace("lecture", "");
    return +index;
}

function getLectLen( lectEl ) {
    let index = getLectIndex(lectEl);
    return lectures[index].segments
}

function canFit( pos, len, index, ttable) {
    let x = pos.x;
    let y = pos.y;

    for( let i=0; i<len; i++){
        //check gaps
        if( i<len-1 && splits.has(y+i+1) ) {
            return false;
        }
        //check fit
        if(y+i < ttable.length && x < ttable[y+i].length){
            if(ttable[y + i][x] && ttable[y + i][x].index !== index){
                return false;
            }
        } else {
            return false;
        }
    }
    return true
}

function getCellBelow( cellEl ){
    let pos = getCellPos(cellEl);
    let newCellId = "ttCell" + pos.x + "-" + (pos.y+1);
    return document.getElementById(newCellId);
}

function setHideCell( element, hide ) {
    if ( hide ) {
        element.style.display = "none";
    } else {
        element.style.display = "table-cell"
    }
}

function setCollidingLecture(id) {

}

function lectureFromTTCoord( x, y ) {
    let cellID = "ttCell" + x + "-" + y;
    let cell = document.getElementById( cellID );
    let div =  cell.childNodes[0];
    if(div){
        return lectures[div.id.replace("lecture", "")];
    }
}


function clearCollisions() {
    let table = document.getElementById("timeTable");

    divs = document.getElementsByClassName("lecture");

    for(let div of divs){
        div.classList.remove("col");
    }

}

function sortStore() {
    let store = document.getElementById("lectureStore");
    let stored = Array.from(store.getElementsByClassName("lecture"));

    stored.sort(lectureCompare);

    for(let el of stored){
        store.appendChild(el);
    }
}

function lectureCompare(a, b) {
    let la = lectures[getLectIndex(a)];
    let lb = lectures[getLectIndex(b)];

    if (la.segments < lb.segments) {
        return -1;
    }
    if (la.segments > lb.segments) {
        return 1;
    }

    if (la.area < lb.area) {
        return -1;
    }
    if (la.area > lb.area) {
        return 1;
    }

    if (la.duration < lb.duration) {
        return -1;
    }
    if (la.duration > lb.duration) {
        return 1;
    }

    if (la.author < lb.author) {
        return -1;
    }
    if (la.author > lb.author) {
        return 1;
    }

    return 0;
}

function isColiding(la, lb) {
    if(la && lb) {
        return la.author === lb.author;
    }
    return false;
}
