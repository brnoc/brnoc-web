const BASE_URL = typeof host !== 'undefined' && host ? host : "https://brnoc.cz/";
// const BASE_URL = "../";

const BLOK_LEN = 30

//####################################
//######### LECTURES #################
//####################################

function loadLectures() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let talks = JSON.parse(this.responseText);
            console.log(JSON.stringify(talks));
            createLecturesFromTalks(talks);
            generateLectureElements();
            loadRules();
        }
    };
    xmlhttp.open("GET", BASE_URL + "API/talks/get/", true);
    xmlhttp.send();
}

function createLecturesFromTalks(talks) {
    let index = 0;
    lectures = [];
    lectureId2Index = [];
    for(let talk of talks){
        if (talk.type === "W")
            continue;

        let lecture = {
            id: +talk.id,
            index: index,
            author: talk.speaker,
            title: talk.title,
            area: getArea(talk.subject),
            segments: talk.length/BLOK_LEN,
            len: "len"+talk.length/BLOK_LEN,
            duration: talk.length,
            rules: []
        };

        lectures.push(lecture);
        lectureId2Index[lecture.id] = index;
        index++;
    }
}

function generateLectureElements() {
    let store = document.getElementById("lectureStore");

    for(let lecture of lectures) {
        let el = document.createElement("div");
        el.id = "lecture" + lecture.index;
        el.name = "lecture" + lecture.index;
        el.classList.add("lecture");
        el.classList.add(lecture.len);
        el.classList.add(lecture.area);

        let text = document.createTextNode( lecture.title );
        let node = document.createElement("h5");
        node.title = lecture.title;
        node.appendChild(text);
        el.appendChild(node);

        text = document.createTextNode( lecture.author );
        node = document.createElement("h6");
        node.title = lecture.author;
        node.appendChild(text);
        el.appendChild(node);

        node = document.createElement("div");
        node.id="rule" + lecture.index;
        node.classList.add("rule");
        el.appendChild(node);

        text = document.createTextNode( "" + lecture.duration + " min" );
        node = document.createElement("div");
        node.classList.add("duration");
        node.appendChild(text);
        el.appendChild(node);

        node = document.createElement("div");
        node.id="stats" + lecture.index;
        node.classList.add("stats");
        el.appendChild(node);

        store.appendChild(el);
    }

    sortStore();
    haluzitko.votes = loadVotes();
}

//####################################
//############ VOTES #################
//####################################

function loadVotes() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let votes = [];
            let lectureStats = new Array(lectures.length).fill(0);
            let votersInput = JSON.parse(this.responseText);
            for(let voterVotes of votersInput){
                // convert from lecture ids to indexes for better search
                if (voterVotes.length === 0)
                    continue;
                let voterChoice = new Array(lectures.length).fill(0);
                for(let i = 0; i<voterChoice.length; i++) {
                    if (voterVotes[i+1] === undefined) {
                        voterChoice[i] = 3;
                    } else if (voterVotes[i+1] === null) {
                        voterChoice[i] = 3;
                    } else {
                        voterChoice[i] = voterVotes[i+1];
                    }
                }

                let exps = voterChoice.map( (n) => Math.exp(n) );
                let sum = exps.reduce( (a,b) => a+b );
                // let sum = voterChoice.reduce((last, cur) => last + Math.exp(cur));
                let out = voterChoice.map((n) => Math.exp(n) / sum);
                for(let i = 0; i<out.length; i++) {
                    lectureStats[i] += out[i];
                    out[i] /= lectures[i].segments;
                }
                votes.push(out);
            }
            haluzitko.votes = votes;
            showLectureStats(lectureStats);
        }
    };
    xmlhttp.open("GET", BASE_URL + 'API/talks/get_votes', true);
    xmlhttp.send();
}

function sortVotes(votes) {
    let users = {};

    for(let vote of votes){
        if(users[vote.user] === undefined){
            users[vote.user] = [];
        }
        users[vote.user].push({id: vote.talk, vote: vote.value})
    }

    let outVotes = [];
    for(let key of Object.keys(users)){
        let user = {id: key, votes: users[key]};
        outVotes.push(user);
    }

    return outVotes;
}

function showLectureStats(stats) {
    for(let i = 0; i<stats.length; i++) {
        let div = document.getElementById("stats"+i);
        div.innerHTML = "" + stats[i].toFixed(2);
    }
}

//####################################
//############ RULES #################
//####################################

function loadRules() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let rules = JSON.parse(this.responseText);
            for(let rule of rules){
                let index = lectureId2Index[rule.l1];
                let lecture = lectures[index];
                if (lecture == undefined)
                    continue;
                lecture.rules.push(rule);
                let div = document.getElementById("rule"+index);
                if(div.innerHTML !== ""){
                    div.innerHTML += ", ";
                    div.title += "\n";
                }
                div.innerHTML += ruleRep(rule);
                div.title += ruleRep(rule);
            }
        }
    };

    xmlhttp.open("GET", BASE_URL + "js/haluzitko/rules.json", true);
    xmlhttp.send();

}

//####################################
//########### EXPORT #################
//####################################

function export2html() {
    console.log("exporting");
    let html ="" +
        "<head><title>Export harmonogramu</title></head>\n" +
        "<body>\n" +
        "   <table style=\"border-collapse: collapse;\">\n";

    let done = new Set();

    for(let y = 0; y<ttTable.length; y++) {
        html += "       <tr>\n";
        let nextRow = "";
        for (let x = 0; x < ttTable[y].length; x++) {
            let lecture = ttTable[y][x];
            if(lecture){
                if(!done.has(lecture.index)){
                    html += "           <td style=\"border: 1px solid gray;\" ";
                    done.add(lecture.index);
                    html += " rowspan=\"" + ( 2*lecture.segments ) + "\">";
                    html += "<b>";
                    html += lecture.title;
                    html += "</b><br>";
                    html += lecture.author;
                    html += "</td>\n"
                }
            } else {
                html += "<td style=\"border: 1px solid gray;\"/>"
                nextRow += "<td style=\"border: 1px solid gray;\"/>"
            }
        }
        html += "       </tr>\n";
        html += "       <tr>" + nextRow + "</tr>\n";
    }

    html += "" +
        "   </table>\n" +
        "</body>\n";
    let tableWin = window.open("", "MsgWindow");
    tableWin.document.write(html);
}

//####################################
//############# SAVE #################
//####################################

function exportToDatabase() {
    let lectureTimes = getLectureTimes(lectures);
    let score = document.getElementById("score").innerHTML;

    let name = prompt("Zadejte jméno");
    if (name === null || name === "") {
        return;
    }
    let id = prompt("Zadejte id");
    if (name === null || name === "") {
        return;
    }

    let exportObject = {
        id: +id,
        name: name,
        score: +score,
        shape: SHAPE,
        lectures: lectureTimes,
        pauses: pauses,
    };


    sendTtDefToDatabase(exportObject);

    let list = document.getElementById("loadList");
    if (list.style.visibility === "visible") {
        list.style.visibility = "hidden";
        loadTimetableDefs();
    }
}

function sendTtDefToDatabase(ttDef) {
    let request = new XMLHttpRequest();
    let url = BASE_URL + "API/schedules/set";
    let params = "id=" + ttDef.id + "&data=" + JSON.stringify(ttDef);
    request.open('POST', url, true);

    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.withCredentials = true;
    request.onreadystatechange = function() {//Call a function when the state changes.
        if(request.readyState === 4 && request.status !== 200) {
            alert("Uložení rozvrhu selhalo!\n\n" + request.responseText);
        }
    };
    request.send(params);
}

function getPauseCount(y) {
    let count = 0;
    for (let pause of pauses) {
        if (pause.from <= y) {
            count += pause.duration;
            y += pause.duration;
        }
    }

    return count;
}

function getLectureTimes(curLectures) {
    let exportLectures = [];

    for (let lecture of curLectures) {
        if (lecture.y !== undefined) {
            exportLectures[lecture.id] = {
                from: lecture.y + getPauseCount(lecture.y),
                room: lecture.x,
                y: lecture.y,
                // name: lecture.title,
                duration: lecture.segments
            };
        }
    }
    return exportLectures;
}

//####################################
//############# LOAD #################
//####################################

function loadTimetableDefs() {
    let list = document.getElementById("loadList");
    list.innerHTML = "";

    if (list.style.visibility === "visible") {
        list.style.visibility = "hidden";
        return
    }

    list.style.visibility = "visible";
    list.innerHTML = "<li>načítání...</li>";

   downloadTimetableDefs();
}

function putTimetableDefs(ttDefs) {
    let list = document.getElementById("loadList");
    list.innerHTML = "";

    for(let table of ttDefs) {
        let li = document.createElement("li");
        let div = document.createElement("div");

        div.onclick = function () {loadTimetable(table)};

        let text = document.createTextNode( "" + table.id + " " + table.name );
        let node = document.createElement("span");
        node.classList.add("loadList-name");
        node.title = table.name;
        node.appendChild(text);
        div.appendChild(node);

        text = document.createTextNode( "" + table.score );
        node = document.createElement("span");
        node.classList.add("loadList-score");
        node.appendChild(text);
        div.appendChild(node);

        li.appendChild(div);
        list.appendChild(li);
    }
}

function downloadTimetableDefs() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            let ttdefs = [];
            let ttdefdefdefs = JSON.parse(this.responseText);

            for (let ttdefdef of ttdefdefdefs) {
                try {
                    let ttdef = JSON.parse(ttdefdef.data);
                    ttdef.id = ttdefdef.id;
                    ttdefs.push(ttdef);
                }
                catch(err) {
                    console.log("Error while parsing TimeTable Deffinition\n", ttdefdef);
                    console.log(err);
                }
            }

            putTimetableDefs(ttdefs);
        }
    };
    xmlhttp.open("GET", BASE_URL + "API/schedules/get", true);
    xmlhttp.withCredentials = true;
    xmlhttp.send();
}

function loadTimetable(timetableDef) {
    console.log("Loading timtableDef", timetableDef);

    setShape(timetableDef.shape);
    let newttTable = makeNewTTtable(ttTable);

    let index = -1;
    for (let lectureDef of timetableDef.lectures) {
        index++;
        if (lectureDef === null) {
            continue;
        }

        let lecture = lectures[lectureId2Index[index]];

        if (lecture === undefined) {
            continue;
        }

        for(let dy = 0; dy<lecture.segments; dy += 1){
            if (lectureDef.y === undefined) {
                continue;
            }
            newttTable[lectureDef.y + dy][lectureDef.room] = lecture;
        }

    }

    setUITable(newttTable);
}

function getArea(subject) {

    if( getArea.dict === undefined) {
        getArea.dict = {
            chemie:         "sci",
            fyzika:         "tech",
            pedagogika:     "soc",
            biologie:       "sci",
            informatika:    "inf",
            informarika:    "inf",
            geografie:      "sci",
            kartografie:    "sci",
            zeměpis:        "sci",
            kultura:        "soc",
            psychologie:    "soc",
            cestování:      "soc",
            medicína:       "sci",
            matematika:     "tech",
            astronomie:     "tech",
            filozofie:      "soc",
            společe:        "soc",
            soci:           "soc",
            robotika:       "tech",
            auta:           "tech",
            budouc:         "tech",
            sex:            "soc",
            poezie:         "soc",
            hudba:          "soc",
            drama:          "soc",
        };
    }

    for (let key in getArea.dict) {
        if (getArea.dict.hasOwnProperty(key)) {
            if (subject.toLowerCase().includes(key)) {
                return getArea.dict[key];
            }
        }
    }

    console.log(subject);
    return "other";
}
