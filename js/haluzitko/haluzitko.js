haluzitko = {
    snapToEven: true,
    inProgress: false,
    finished: false,

    vyhaluz: async function() {
        const NO_INCREASE_LIMIT = 30000;
        const UPDATE_TIMEOUT = 50;

        if(this.inProgress){
            this.finished = true;
            return;
        }
        this.setInProgress(true);
        if(!this.loadGif.get_loading()){
            this.loadGif.play();
        }

        this.finished = false;
        let prehaluzed = false;
        let noIncrease = 0;
        let timeStamp = Date.now();
        let iterNum = 0;

        //recreate TimeTable
        let newttTable = makeNewTTtable(ttTable);
        populate(newttTable, lectures);
        let bestScore = getScore(newttTable, this.votes, this.restrictions, true);
        let bestTable = copyTable(newttTable);
        let bestChanged = true;
        let updateNum = 0;
        putToTimeTable(newttTable);

        while(!this.finished ){
            iterNum++;
            if(prehaluzed){
                // make permutations
                let numPerm = randint(1,5);
                newttTable = copyTable(bestTable, newttTable);
                for (let i=0; i<numPerm; i++){
                    permutate(newttTable);
                }
            } else {
                newttTable = makeNewTTtable(ttTable);
                populate(newttTable, lectures);
            }

            let newScore = getScore(newttTable, this.votes, this.restrictions, false);

            if(newScore <= bestScore){
                noIncrease++;
                if(noIncrease > NO_INCREASE_LIMIT){
                    if(prehaluzed){
                        this.finished = true;
                    } else {
                        prehaluzed = true;
                        noIncrease = 0;
                    }
                }
            } else{
                noIncrease = 0;
                bestScore = newScore;
                bestTable = copyTable(newttTable, bestTable);
                bestChanged = true;
            }

            let curTime = Date.now();
            if(curTime - timeStamp > UPDATE_TIMEOUT){
                let updateTable = bestChanged && updateNum % 10 === 0;
                let lineFract = noIncrease/NO_INCREASE_LIMIT/2;
                if(prehaluzed) {
                    lineFract += 0.5;
                }
                this.onUpdate(bestScore, bestTable, lineFract, updateTable);
                timeStamp = curTime;
                if(updateTable){
                    bestChanged = false;
                }
                updateNum++;
                await sleep(1)
            }
        }

        updateLectureTimes(bestTable, lectures);
        this.onFinished(bestTable);

        this.setInProgress(false);
        return 0;
    },

    fillTT: function(){
        let newttTable = makeNewTTtable(ttTable);

        populate(newttTable, lectures);
        getScore(newttTable, this.votes, this.restrictions, true);
        updateLectureTimes(newttTable, lectures);

        this.onFinished(newttTable);
    },

    clearTT: function () {
        clearTimeTableElements();
        let newttTable = makeNewTTtable(ttTable);
        getScore(newttTable, this.votes, this.restrictions, true);

        updateLectureTimes(newttTable, lectures);
        this.onFinished(newttTable);
    },

    setInProgress: function (prog) {
        this.inProgress = prog;
        let haluzbtn = document.getElementById("haluzBtn");
        if(prog){
            haluzbtn.classList.add("inProgress");
            haluzbtn.style.color = "#f1f1f1";
            haluzbtn.innerHTML = "Haluzení"
        } else {
            haluzbtn.classList.remove("inProgress");
            haluzbtn.style.color = "";
            haluzbtn.innerHTML = "Vyhaluz"
        }
    },

    onUpdate: function () {},
    onFinished: function () {},
    onUpdateTable: function () {},
};

haluzitko.restrictions = [];
haluzitko.loadGif = new SuperGif({
    gif: document.getElementById('loadGif'),
    loop_mode: 'auto',
    auto_play: 0,
    draw_while_loading: false,
    show_progress_bar: false,
    max_width: 200,
    on_end: function () {
        if(!haluzitko.inProgress){
            haluzitko.loadGif.pause()
        }
    }
});
haluzitko.loadGif.load( function () {
    haluzitko.loadGif.move_relative(199)
    if(haluzitko.inProgress){
        haluzitko.loadGif.play();
    }
});


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function randint(a, b) {
    return Math.floor(Math.random() * (b-a+1)) + a
}

function copyTable(table, old=undefined) {
    if(old === undefined) {
        let copy = [];
        for (let y = 0; y < table.length; y++) {
            let row = [];
            for (let x = 0; x < table[y].length; x++) {
                row.push(table[y][x])
            }
            copy.push(row);
        }
        return copy;
    } else {
        for (let y = 0; y < table.length; y++) {
            for (let x = 0; x < table[y].length; x++) {
                old[y][x] = table[y][x];
            }
        }
        return old;
    }
}

function populate(ttTable, lectures) {
    let maxLen = 6;

    for(let curLen=maxLen; curLen>0; curLen--){
        let freeSpaces = new Set();
        for(let y=0; y<ttTable.length; y++) {
            for(let x=0; x<ttTable[y].length; x++){
                let pos = {x, y};
                if(canFit(pos, curLen, -1, ttTable)){
                    freeSpaces.add(""+x+"-"+y)
                }
            }
        }

        for(let lecture of lectures.filter( lect => lect.segments === curLen)){
            let ordered = Array.from(freeSpaces.values());
            if(ordered.length === 0){
                break;
            }
            for(let tryNum=0; tryNum < 20; tryNum++){
                let index = randint(0, ordered.length-1);
                let posAr = ordered[index].split("-");
                let pos = {x: +posAr[0], y: +posAr[1]};

                if( canPlaceRules(lecture, pos, ttTable, false) ){
                    lecture.hx = pos.x;
                    lecture.hy = pos.y;
                    for(let dy=-curLen+1; dy<curLen; dy++){
                        let y = pos.y + dy;
                        freeSpaces.delete(""+pos.x+"-"+y);
                        if(dy >= 0){
                            ttTable[y][pos.x] = lecture;
                        }
                    }
                    break;
                }
            }
        }
    }
}

function permutate(table) {
    let numLect = lectures.length;
    let l1 = lectures[randint(0, numLect-1)];

    let freeSpaces = new Set();
    let oldPos = {x:l1.hx, y:l1.hy};
    removeLect(l1, table);
    // let pos2 = {x:l2.hx, y:l2.hy};
    for(let y=0; y<table.length; y++) {
        for(let x=0; x<table[y].length; x++){
            let pos = {x, y};
            if( canFit(pos, l1.segments, l1.index, table) &&
                canPlaceRules(l1, pos, table, false) ) {
                freeSpaces.add(""+x+"-"+y)
            }
        }
    }

    let newPos = Object.assign({}, oldPos);
    let ordered = Array.from(freeSpaces.values());
    if(ordered.length !== 0){
        let index = randint(0, ordered.length-1);
        let posAr = ordered[index].split("-");
        newPos = {x: +posAr[0], y: +posAr[1]};
    }

    if(newPos.hx !== undefined){
        putLect(l1, newPos, table);
    }

    return table;
}

function removeLect(lecture, table) {
    if(lecture.hx === undefined){
        return
    }
    for(let dy=0; dy<lecture.segments; dy++) {
        table[lecture.hy+dy][lecture.hx] = undefined
    }
    lecture.hx = undefined;
    lecture.hy = undefined;
}

function putLect(lecture, pos, table) {
    for(let dy=0; dy<lecture.segments; dy++) {
        table[pos.y+dy][pos.x] = lecture
    }
    lecture.hx = pos.x;
    lecture.hy = pos.y;
}

function putToTimeTable(newTTTable) {
    clearTimeTableElements();
    done = new Set();

    let table = document.getElementById("timeTable");

    for(let y = 0; y<newTTTable.length; y++) {
        for (let x = 0; x < newTTTable[y].length; x++) {
            let lecture = newTTTable[y][x];
            if(lecture){
                let id = "lecture" + lecture.index;
                let div = document.getElementById(id);
                let cell = document.getElementById("ttCell"+x+"-"+y);

                if( ! done.has(lecture.index) ){
                    cell.appendChild(div);
                    lecture.x = x;
                    lecture.y = y;
                    done.add(lecture.index);
                }
            }
        }
    }
    haluzitko.onUpdateTable(newTTTable);
}

function makeNewTTtable(oldTTtable) {
    let newttTable = new Array(oldTTtable.length);
    for (let i = 0; i < oldTTtable.length; i++) {
        newttTable[i] = new Array(oldTTtable[i].length);
    }

    return newttTable;
}

function updateLectureTimes(ttTable, lectures) {

}

function clearTimeTableElements() {
    let table = document.getElementById("timeTable");
    let store = document.getElementById("lectureStore");

    let isHeader = true;
    for(let row of table.rows){
        if(isHeader){
            isHeader = false;
            continue;
        }
        for(let cell of row.cells){
            let nodes = cell.childNodes;
            if(nodes.length > 0){
                let el = nodes[0];
                el.classList.remove("col");
                store.appendChild(nodes[0]);

                let lecture = lectures[getLectIndex(el)];
                lecture.x = undefined;
                lecture.hx = undefined;
                lecture.y = undefined;
                lecture.hy = undefined;

            }
        }
    }

    sortStore();
}

function setScore(score, iterProgress=0) {
    let scorePlace = document.getElementById("score");
    scorePlace.innerHTML = score.toFixed(2);
    let progLine = document.getElementById("progressLine");
    progLine.style.width = "" + Math.round(iterProgress*75) + "%";
}

function getScore(table, votes, restrictions, doUpdate=false) {
    let score = 0;
    for(let line of table){
        let lineScore = 0;
        for(voter of votes){
            lineScore += evaluateLine(line, voter);
        }
        score += lineScore;
    }

    if(doUpdate){
        setScore(score)
    }
    return score;
}

function evaluateLine(line, voter) {
    let votes = [];
    for(let lecture of line){
        if(lecture){
            votes.push(voter[lecture.index])
        }
    }

    if (votes.length === 0){
        return 0;
    }
    return Math.max.apply(Math, votes);
}
