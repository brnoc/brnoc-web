//
// function checkRules(lecture, pos, ttable) {
//     for (let rule of lecture.rules) {
//         let ok = true;
//         for (let x = 0; x < ttable[pos.y].length; x++) {
//             if(pos.x === x){
//                 continue;
//             }
//             if(ttable[pos.y][x].author === lecture.author){
//                 ok = false;
//             }
//         }
//     }
// }

function updateRules() {
    for(let lecture of lectures){
        if(lecture.x === undefined){
            continue;
        }
        for (let rule of lecture.rules) {
            let pos = {x:lecture.x, y:lecture.y};
            let ok = checkRule(rule, lecture, pos, ttTable);
            if(!ok){
                showRule(rule, lecture);
            }
        }
    }

    updateCollisions();
}

function canPlaceRules(lecture, pos, ttable, writeStatus=true) {
    if (writeStatus){
        clearRuleStatus()
    }
    for (let rule of lecture.rules) {
        let ok = checkRule(rule, lecture, pos, ttable);
        if( !ok && rule.critical){
            if(writeStatus){
                writeRuleStatus(rule, lecture, pos, ttable)
            }
            return false;
        }
    }
    return true;
}

function writeRuleStatus(rule, lecture, pos, ttable) {
    let rulediv = document.getElementById("ruleStatus");
    let text = "";
    let l = {};
    switch(rule.type) {
        case "inroom":
            text = "Špatná místnost.<br>Správně v " + rule.room;
            break;
        case "intime":
            text = "Špatný čas.<br>Správně od " + rule.min + " do " + rule.max;
            break;
        case "nocolide":
            text = "Koliduje s " + lectures[lectureId2Index[rule.l2]].title;
            break;
        case "after":
            l = lectures[lectureId2Index[rule.l2]];
            text = "Má být po " + l.title;
            break;
        case "before":
            l = lectures[lectureId2Index[rule.l2]];
            text = "Má být před " + l.title;
            break;
        default:
            console.log("unknown rule", rule)
    }
    rulediv.innerHTML = text;
}

function clearRuleStatus(rule) {
    document.getElementById("ruleStatus").innerHTML = "";
}

function ruleRep(rule) {
    let l = {};
    switch(rule.type) {
        case "inroom":
            return "v místnosti " + rule.room;
        case "intime":
            return "od " + rule.min + " do " + rule.max;
        case "nocolide":
            return "nocolide";
        case "after":
            l = lectures[lectureId2Index[rule.l2]];
            return "po " + l.title;
        case "before":
            l = lectures[lectureId2Index[rule.l2]];
            return "před " + l.title;
        default:
            console.log("unknown rule", rule)
    }
}


function showRule(rule, lecture) {
    
}

function clearRules() {

}



function updateCollisions() {
    let cols = getCollisions();

    clearCollisions();
    for(let cur of cols){
        let div1 = document.getElementById(cur[0]);
        let div2 = document.getElementById(cur[1]);

        div1.classList.add("col");
        div2.classList.add("col");
    }
}

function getCollisions(y=undefined) {
    let cols = [];
    if(y === undefined){
    } else{
    }

    for(y = 0; y<ttTable.length; y++){
        for(let x0 = 0; x0<ttTable[y].length; x0++){
            for(let x1 = x0+1; x1<ttTable[y].length; x1++){
                if(isColiding(ttTable[y][x0], ttTable[y][x1])){
                    cols.push([ "lecture" + ttTable[y][x0].index,
                        "lecture" + ttTable[y][x1].index, ]);
                }
            }
        }
    }

    return cols;
}

function checkRule(rule, lecture, pos, ttable) {
    let ok = true;
    switch(rule.type) {
        case "inroom":
            ok = rule.room === pos.x;
            break;
        case "intime":
            ok = rule.min <= pos.y && rule.max >= pos.y;
            break;
        case "nocolide":
            for(let dy=0; dy<lecture.segments; dy++) {
                let y = pos.y + dy;
                for (let x = 0; x < ttable[y].length; x++) {
                    if(ttable[y][x] && ttable[y][x].id === rule.l2){
                        ok = false;
                        break;
                    }
                }
            }
            break;
        case "after":
            for(let y=pos.y; y<ttable.length; y++) {
                for (let x = 0; x < ttable[y].length; x++) {
                    if(ttable[y][x] && ttable[y][x].id === rule.l2){
                        ok = false;
                        break;
                    }
                }
            }
            break;
        case "before":
            for(let y=0; y<pos.y + lecture.segments; y++) {
                for (let x = 0; x < ttable[y].length; x++) {
                    if(ttable[y][x] && ttable[y][x].id === rule.l2){
                        ok = false;
                        break;
                    }
                }
            }
            break;
        default:
            console.log("unknown rule", rule)
    }

    return ok;
}
