class Table{
  constructor(conf){
    //set default config
    this.conf = {
      sortBy: -1, //id of column the table is sorted by
      sortOrd: true, //order true->ascending false->descending
      renderAvg: false, //render new line with average column value
      renderSum: false, //render new line with sum of column values
      renderMed: false, //render new line with median of column values
      head: [], //list of column names
      data: [], //array with data to display
      renderDd: false, //render dropdown
      dropdown: (row, key) => {var div = document.createElement('div'); div.innerHTML = JSON.stringify(row); return div;},
      dropdownKey:-1,
      rowCallback: false,
    }

    this.columns = [];

    //load config
    for(var key in conf){
      this.conf[key] = conf[key];
    }


    this.root = this.conf.root;

    //parse header
    Array.prototype.forEach.call(this.conf.head, item => {
      this.columns.push({
        name: item
      })
    });

    this.parse_data();
  }

  parse_data(){
    //empty data
    this.data = {
      head: [],
      body: [],
    }

    //parse data
    Array.prototype.forEach.call(this.conf.data, row => {
      var cells = [];
      row.forEach((val, key) => {
        var cellConfig = {
          value: val,
          render: (typeof this.columns[key].render != "undefined")?this.columns[key].render:false,
          compute: (typeof this.columns[key].compute != "undefined")?this.columns[key].compute:false,
        };


        if(typeof this.columns[key].setStyle != "undefined"){
          cellConfig.setStyle = this.columns[key].setStyle;
        }
        else cellConfig.setStyle = false;

        if(typeof this.columns[key].type != "undefined"){
          switch(this.columns[key].type){
            case 'bool':
            cellConfig.render = (val) => bool(val);
            cellConfig.setStyle = (val) => {return val?"background-color:green":"background-color:red"};
            break;
          }
        }
        cellConfig.row = row;
        cells.push(new Cell(cellConfig));
      });
      this.data.body.push(cells);
    });
  }

  render(){
    var root = this.root;
    if(typeof root == "undefinded"){
      console.log("Root undefined!");
      return false;
    }

    while(root.firstChild)root.firstChild.remove();


    //render header
    var thead = document.createElement("thead");
    var tr = document.createElement("tr");
    Array.prototype.forEach.call(this.columns, (column,key) => {
      var td = document.createElement("td");
      tr.appendChild(td);
      td.innerHTML = column.name;
      td.onclick = () => {this.sort(key);}
      if(typeof column.headerCallback != "undefined")td.onclick = () => {this.sort(key); column.headerCallback()};
      if(key == this.conf.sortBy) td.innerHTML += this.conf.sortOrd?" &#9650;":" &#9660;";

    });
    thead.appendChild(tr);
    root.appendChild(thead);

    //render body
    var tbody = document.createElement("tbody");
    this.data.body.forEach((row,key) => {
      var tr = document.createElement("tr");
      tr.className = "tablesTableRow ";


      if(this.conf.renderDd || this.conf.rowCallback){
        tr.className += " tablesRowClickable";
        tr.onclick = () => {
          this.conf.dropdownKey = (this.conf.dropdownKey == key)?-1:key;
          this.render();
        }
      }

      row.forEach(cell => {
        var td = document.createElement("td");
        td.className = cell.className;
        if(cell.style)td.style = cell.style;
        td.appendChild(cell.render);
        tr.appendChild(td);

      });
      tbody.appendChild(tr);

      // redner dropdown
      if(this.conf.renderDd && this.conf.dropdownKey == key){
        var dd = document.createElement("tr");
        var td = document.createElement("td");
        td.colSpan = "1000";
        td.appendChild(this.conf.dropdown(row, key));
        dd.className = "tablesDropdown ";
        dd.classList.add((key%2==0)?"even":"odd");
        dd.appendChild(td);
        tbody.appendChild(dd);
        tr.classList.add("tablesDropdownHead");
        var filler = document.createElement("tr");
        tbody.appendChild(filler);
      }
    });

    //render an additional row with column average value
    if(this.conf.renderAvg){
      var tr = document.createElement("tr");
      this.columns.forEach((col, key) => {
        var td = document.createElement("td");
        td.innerHTML = isNaN(this.col_avg(key))?"":"avg: "+parseInt(100*(this.col_avg(key)))/100;
        tr.appendChild(td);
      });
      tbody.appendChild(tr);
    }

    //render an additional row with column value sum
    if(this.conf.renderSum){
      var tr = document.createElement("tr");
      this.columns.forEach((col, key) => {
        var td = document.createElement("td");
        td.innerHTML = isNaN(this.col_sum(key))?"":"∑: "+parseInt(100*(this.col_sum(key)))/100;
        tr.appendChild(td);
      });
      tbody.appendChild(tr);
    }

    //render an additional row with column value sum
    if(this.conf.renderMed){
      var tr = document.createElement("tr");
      this.columns.forEach((col, key) => {
        var td = document.createElement("td");
        td.innerHTML = "M: "+this.col_med(key);
        tr.appendChild(td);
      });
      tbody.appendChild(tr);
    }

    root.appendChild(tbody);
  }

  set_column(pos, col){
    for(var key in col){
      this.columns[pos][key] = col[key];
    }
    this.parse_data();
  }

  // returns columns max/min value
  // max ... true -> max value, false -> min value
  col_max(col,max = true){
    var val = this.data.body[0][col].val;
    this.data.body.forEach(row => {
      val = max?Math.max(val, row[col].val):Math.min(val, row[col].val);
    });
    return val;
  }

//returns columns average float value
  col_avg(col){
    var sum = 0;
    var count = 0;

    this.data.body.forEach(row => {
      var val = parseFloat(row[col].value)
      val = isNaN(val)?0:val;
      sum += row[col].value;
      count++;
    });


    return sum/count;
  }

// returns sum of all columns float values
  col_sum(col){
    var sum = 0;
    this.data.body.forEach(row => {
      var val = parseFloat(row[col].value)
      val = isNaN(val)?0:val;
      sum += val;
    });
    return sum;
  }

// returns the median of column values
  col_med(col){
    var arr = this.data.body.map(row => row[col].value);
    if(typeof arr[0] == "string")arr.sort((a,b)=>{
      if(typeof a == typeof b == "string")return compare_string(a,b);
      return (a > b)?1:(a < b)?-1:0;
    });
    else arr.sort();
    return arr[parseInt(arr.length/2)];
  }

// sorts data body by column number
  sort(key = -1){
    if(this.conf.sortBy == key){
      this.conf.sortOrd = !this.conf.sortOrd;
    }else{
      this.conf.sortBy = key;
      this.conf.sortOrd = true;
    }

    Array.prototype.sort.call(this.data.body, (a,b) => {
      if(typeof a[this.conf.sortBy].value == "string" && typeof b[this.conf.sortBy].value == "string")return compare_string(a[this.conf.sortBy].value,b[this.conf.sortBy].value)*(this.conf.sortOrd?1:-1);

      if (a[this.conf.sortBy].value < b[this.conf.sortBy].value)
        return this.conf.sortOrd?-1:1;
      if (a[this.conf.sortBy].value > b[this.conf.sortBy].value)
        return this.conf.sortOrd?1:-1;
      return 0;
    });

    this.render();
  }

  get_row_key_by_value(col, val){
    var k = 0;
    this.data.body.forEach((row,key) =>{
      if(row[col].val == val) k = key;
    });
    return k;
  }

  get_column_values(col){
    var values = {};
    this.data.body.forEach(row => {
      var val = row[col].value;
      if(typeof values[val] == "undefined")values[val] = {value: 1};
      else values[val].value++;
    });
    return values;
  }
}


//cell
class Cell{
  constructor(data){
    this.val = data.value;
    this.compute = (val) => {return val};
    this.element = (val) => {
      var span = document.createElement("span");
      span.innerHTML = val;
      return span;
    }
    if(data.render)this.element = data.render;
    if(data.compute)this.compute = data.compute;
    if(data.className)this.className = data.className;
    this.row = data.row;
    this.setStyle = (!data.setStyle)?(val) => "":(val, row) => data.setStyle(val, row);
  }

  get style(){return this.setStyle(this.val, this.row);}

  //returns display value
  get value(){return this.compute(this.val, this.row);}

  //returns display element
  get render(){return this.element(this.val, this.row);}
}



/*
  Filtering
*/

class Filterer{
  constructor(data, root){
    this.filters = data;
    this.root = root;
  }

  filter(data){
    var dataNew = [];
    Array.prototype.forEach.call(this.filters, filter => {
      Array.prototype.forEach.call(data, row => {
        if(filter.filter(row)) dataNew.push(row);
      });
      data = dataNew;
      dataNew = [];
    });
    return data;
  }

  render(){
    if(!this.root)return 0;
    while(this.root.firstChild)this.root.firstChild.remove()
    this.filters.forEach(filter => {
      var div = document.createElement("div");
      div.className = "filter";
      var span1 = document.createElement("span");
      span1.innerHTML = (typeof filter.title !== 'undefined')?filter.title:"Filter";
      span1.className = "text";
      var span2 = document.createElement("span");
      span2.className = "fas fa-times-circle";
      span2.onclick = () => render(this.remove(filter).filter(data));
      div.appendChild(span1);
      div.appendChild(span2);
      this.root.appendChild(div);
    });
  }

  add(filt){
    if(!this.filters.includes(filt))this.filters.push(filt);
    console.log(this.filters);
    this.render();
    return this;
  }

  remove(filt){
    if(this.filters.includes(filt))this.filters.splice(this.filters.indexOf(filt),1);
    console.log(this.filters);
    this.render();
    return this;
  }

  toggle(filt){
    if(!this.filters.includes(filt))this.filters.push(filt);
    else this.filters.splice(this.filters.indexOf(filt),1);
    console.log(this.filters);
    this.render();
    return this;
  }
}

class Filter{
  constructor(conf){
    this.name = conf.name;
    this.operation = (typeof conf.operation !== 'undefined')?conf.operation:"";
    this.value = (typeof conf.value !== 'undefined')?conf.value:"";
    this.title = (typeof conf.title !== 'undefined')?conf.title:"";
  }

  filter(row){
    if(Array.isArray(row[this.name])){
      a = 0;x

    }
    switch(this.operation){
      case "!":
        if(!row[this.name]) return true;
        break;
      case "<":
        if(row[this.name] < this.value)return true;
        break;
      case ">":
        if(row[this.name] > this.value)return true;
        break;
      case "=":
        if(row[this.name] == this.value)return true;
        break;
      default:
       if(row[this.name]) return true;
    }
    return false;
  }
}


/*
  cell inner render functions
*/

// renders a bar representing percentual value of cell
// val ... cell value
// max ... 100%
// num ... display value ... default false
// col ... bar collor ... default in css
function bar(val, max, num = false, col = false){
  val = isNaN(parseFloat(val))?"0":parseFloat(val);
  max = isNaN(parseFloat(max))?"0":parseFloat(max);
  var per = parseFloat(100*(100*val/max))/100;

  var bar = document.createElement("span");
  bar.className = "tablesPercentBar";
  bar.style.width = per>100?100:per<0?0:per+"%";
  bar.title = val + " ("+parseInt(per*100)/100+"%)";
  if(col)bar.style.backgroundColor = col;

  if(num){
    var value = document.createElement("div");
    value.className = "tablesPercentBarValue";
    value.innerHTML = val;
    bar.appendChild(value);
  }

  return bar;
}



// renders a cell with Y/N text
// val ... cell value
function bool(val){
  var cell = document.createElement("div");
  cell.innerHTML = val?"Y":"N";

  return cell;
}

function text_cell(text){
  var cell = document.createElement('div');
  cell.innerHTML = text;
  return cell;
}


/*
  other functions
*/
//compares strings a and b
function compare_string(a,b){

  if(typeof a != "string")return -1;
  if(typeof b != "string")return 1;

  var alphabet = ["","0","1","2","3","4","5","6","7","8","9","a","A","ä","Ä","b","B","c","C","č","Č","d","D","ď","Ď","e","E","ě","Ě","é","É","ë","Ë","f","F","g","G","h","H","i","I","í","Í","j","J","k","K","l","L","ľ","Ľ","m","M","n","N","ň","Ň","o","O","ó","Ó","ö","Ö","ô","Ô","p","P","q","Q","r","R","ř","Ř","s","S","š","Š","t","T","ť","Ť","u","U","ú","Ú","ů","Ů","ü","Ü","v","V","w","W","x","X","y","Y","z","Z","ž","Ž"];

  var ai = alphabet.includes(a[0])?alphabet.indexOf(a[0]):-1;
  var bi = alphabet.includes(b[0])?alphabet.indexOf(b[0]):-1;


  if(ai > bi)return 1;
  if(ai < bi)return -1;
  if((a[0] != b[0]) && (bi == ai == -1)) return(a[0] > b[0])?1:-1;


  if(a.length > 0 && b.length > 0)
    return compare_string(a.substr(1),b.substr(1));

  return (a.length > b.length)?1:(a.length<b.length)?-1:0;
}
