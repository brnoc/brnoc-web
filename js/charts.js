class PieChart{
  constructor(data, root, conf = false){
    this.rawData = data;
    this.data = [];
    this.sum = 0;
    this.root = root;

    this.conf = {};
    this.conf.colors = ["red","orange","yellow","cyan","blue","purple"];
    this.conf.renderLegend = false;

    if(conf){
      for(var key in conf)this.conf[key] = conf[key];
    }

    this.render();
  }

  parse_data(){
    this.data = [];
    this.sum = 0;

    var i = 0;
    for(var row in this.rawData)this.sum+=this.rawData[row].value;
    for(var row in this.rawData)this.data.push({
      color: (typeof this.rawData[row].color == "undefined")?this.conf.colors[i++%this.conf.colors.length]:this.rawData[row].color,
      percent: this.rawData[row].value/this.sum,
    });

  }

  set_data(data){
    this.rawData = data;
    return this;
  }

  render(){
    this.parse_data();
    const svgEl = this.root;
    while(svgEl.firstChild)svgEl.firstChild.remove();
    svgEl.setAttribute("viewBox","-1 -1 2 2");
    svgEl.style.transform = "rotate(-90deg)";
    let cumulativePercent = 0;

    function getCoordinatesForPercent(percent) {
      const x = Math.cos(2 * Math.PI * percent);
      const y = Math.sin(2 * Math.PI * percent);
      return [x, y];
    }

    this.data.forEach(slice => {
      // destructuring assignment sets the two variables at once
      const [startX, startY] = getCoordinatesForPercent(cumulativePercent);

      // each slice starts where the last slice ended, so keep a cumulative percent
      cumulativePercent += slice.percent;

      const [endX, endY] = getCoordinatesForPercent(cumulativePercent);

      // if the slice is more than 50%, take the large arc (the long way around)
      const largeArcFlag = slice.percent > .5 ? 1 : 0;

    	// create an array and join it just for code readability
      const pathData = [
        `M ${startX} ${startY}`, // Move
        `A 1 1 0 ${largeArcFlag} 1 ${endX} ${endY}`, // Arc
        `L 0 0`, // Line
      ].join(' ');

      // create a <path> and append it to the <svg> element
      const pathEl = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      pathEl.setAttribute('d', pathData);
      pathEl.setAttribute('fill', slice.color);
      svgEl.appendChild(pathEl);
    });

    if(this.conf.renderLegend){
        if(typeof this.legend == 'undefined')this.legend = document.createElement('div');
        this.legend.className = "chartsPieChartLegend";
        this.legend.innerHTML = "";
        var data = this.rawData;
        var i = 0;
        for(var k in data){
          this.legend.innerHTML+=`<div class="chartsPieChartLegendItem"><span style="color:${
            (typeof data[k].color == "undefined")?this.conf.colors[i++%this.conf.colors.length]:data[k].color
          }">⬤</span>  ${k} (${data[k].value})</div>`;
        }
        svgEl.parentElement.appendChild(this.legend);
    }
  }
}
