<?php
/**
 * Created by PhpStorm.
 * User: norik
 * Date: 25.12.15
 * Time: 2:22
 */

namespace App\Presenters;

use Nette,
	App\Model;



class RegisteredPresenter extends Nette\Application\UI\Presenter
{
	/** @var  @var Nette\Databe\Context */
	private $database;
	public $iter = 0;
	private $era;
	public $aera;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
		$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
		$this->era = $settings->era;
		$this->aera = $settings->era;
	}


	public function renderDefault()
	{
		$this->template->aera = $this->aera;
		$this->template->iter = $this->iter;
		if ($this->iter<10 and $this->iter%2==0) {
			$this->template->users = $this->database->table('user')
				->where('era = ?', $this->aera)
				->order('name COLLATE utf8_czech_ci');
		} elseif ($this->iter<10 and $this->iter%2==1) {
			$this->template->users = $this->database->table('user')
				->where('era = ?', $this->aera)
				->order('last_name COLLATE utf8_czech_ci');
		} else {
			$this->template->users = $this->database->query('SELECT * FROM user ORDER BY RAND()');
		}
		$this->redrawControl('list');
	}

	public function renderAdmin()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->template->users = $this->database->table('user')
			->where('era = ?', $this->era)
			->order('id DESC');
	}

	public function handleDelete($id)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->database->table('user')
			->where('era = ?', $this->era)
			->where('id = ?', $id)->delete();
	}

	public function handleChange($value, $aera)
	{
		$this->iter = $value + 1;
		$this->aera = $aera;
	}

	public function handlePreviousEra($aera)
	{
		$this->aera = max($aera - 1, 1);
	}

	public function handleNextEra($aera)
	{
		$this->aera = min($aera + 1, $this->era);
	}
}
