<?php
/**
 * Created by PhpStorm.
 * User: norik
 * Date: 27.12.15
 * Time: 1:59
 */


namespace App\Presenters;

use Nette,
	Nette\Application\UI\Form,
	Nette\Mail\Message,
	Nette\Mail\SendmailMailer,
	Helpers;



class EmailSenderPresenter extends Nette\Application\UI\Presenter
{

	/** @var  @var Nette\Databe\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	protected function createComponentEmailSenderForm()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$form = new Form;

		$form->addText('email', 'Příjemce');

		$form->addText('subject', 'Předmět')
			->setRequired();

		$form->addTextArea('text', 'Zpráva')
			->setRequired()
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('title', 'Žere to HTML znaky. (Naopak samotný "ENTER" neudělá nic.)');;;

		$form->addSubmit('send', 'Odeslat email');

		$form->onSuccess[] = array($this, 'emailSenderFormSucceeded');

		Helpers::bootstrapForm($form);

		return $form;
	}


	public function emailSenderFormSucceeded($form, $values)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->flashMessage('Email úspěšně odeslán. Doufám, že to nebyl troll', 'success');

		$mail = new Message;
		$mail->setFrom('pus@brnoc.cz', 'Pravoúhlý sněm')
			//->addTo('pus@brnoc.cz')
			->setSubject($values->subject)
			->setHTMLBody($values->text);

		if ($values->email == 'all' or $values->email == 'everyone') {
			$emails = $this->database->table('user')->where('id > 0')->fetchAll();

			foreach ($emails as $email) {
					$mail->addBcc($email->email);
			}
		} elseif ($values->email == 'talk' or $values->email == 'talks') {
			$emails = $this->database->table('talk')->where('hidden = ?', 0)->fetchAll();

			foreach ($emails as $email) {
					$mail->addBcc($email->user->email);
			}
		} else {
			$user = $this->database->table('user')->where('era = ?', NULL)->where('email LIKE ?',$values->email)->fetch();
			$full_name = $user->name . ' ' . $user->last_name;

			$mail->addTo($values->email, $full_name);
		}

		$mailer = new SendmailMailer;
		$mailer->send($mail);
		$this->redirect('this');

	}

}
