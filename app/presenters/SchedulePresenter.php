<?php
/**
 * Created by PhpStorm.
 * User: norik
 * Date: 27.12.15
 * Time: 15:29
 */

namespace App\Presenters;

use Nette,
	Nette\Mail\Message,
	Nette\Mail\SendmailMailer,
	Nette\Application\UI\Form,
	Nette\Database\SqlLiteral,
	App\Model,
	Helpers;



class SchedulePresenter extends Nette\Application\UI\Presenter
{
	/** @var  @var Nette\Databe\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function renderDefault()
	{
		$this->template->talks = $this->database->table('talk')->where('hidden = ?', 0)
			->order('name ASC');
	}


	public function handleDelete($id)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->database->table('talk')->where('id = ?', $id)->update(Array('hidden' => 2));

		//$this->redirect();
	}

	public function handleHot($id)
	{

		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$temp = $this->database->table('talk')->where('id = ?', $id)->fetch();
		if ($temp->hot == 0)
		{
			$this->database->table('talk')->where('id = ?', $id)->update(Array('hot' => 1));
		} else {
			$this->database->table('talk')->where('id = ?', $id)->update(Array('hot' => 0));
		}


	}
}