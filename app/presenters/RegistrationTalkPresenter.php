<?php
/**
 * Created by PhpStorm.
 * User: norik
 * Date: 27.12.15
 * Time: 1:59
 */


namespace App\Presenters;

use App\Model\RegistrationTalkModel;

use Nette,
	Nette\Application\UI\Form,
	Nette\Mail\Message,
	Nette\Mail\SendmailMailer,
	Helpers;



class RegistrationTalkPresenter extends Nette\Application\UI\Presenter
{

	/** @var RegistrationTalkModel */
	private $registrationTalkModel;

	/** @var  @var Nette\Databe\Context */
	private $database;


	public function __construct(RegistrationTalkModel $registrationTalkModel, Nette\Database\Context $database)
	{
		$this->registrationTalkModel = $registrationTalkModel;
		$this->database = $database;
	}

	protected function createComponentRegisterTalkForm()
	{
		$form = new Form;

		$form->addText('name', 'Název')
			->addRule(Form::MAX_LENGTH, 'Nevlezlo by se nám to do výpisu', 64)
			->setRequired();

		$form->addText('subject', 'Obor')
			->addRule(Form::MAX_LENGTH, 'Víc jak 42 znaků nepotřebuješ', 42)
			->setRequired();

		$form->addTextArea('decription', 'Popis')
			->setRequired()->addRule(Form::MAX_LENGTH, 'Poznámka je pasdříliš dlouhá', 1000);

		$form->addText('lenght', 'Odhadovaná délka')
			->setRequired()->addRule(Form::INTEGER, 'Délka musí být číslo.')
			->addRule(Form::RANGE, 'Délka musí být v rozmezí od 15 do 120 minut.', array(15, 120))
			->setType('number');

		$form->addText('email', 'Email')
			->setRequired()->addRule($form::EMAIL)
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('title', 'Zadej email, kterým ses přihlásil jako účastník. Neregistrovaní účastníci nemohou přednášet.');;

		$form->addSubmit('send', 'Registrovat přednášku');

		$form->onSuccess[] = array($this, 'registerTalkFormSucceeded');

		Helpers::bootstrapForm($form);

		return $form;
	}


	public function registerTalkFormSucceeded($form, $values)
	{
		$temp = $this->registrationTalkModel->createTalk($values);

		if ($temp)
		{
			$this->flashMessage('Registrace proběhla úspěšně, podívej se do své emailové schránky', 'success');

			$mail = new Message;
			$mail->setFrom('pus@brnoc.cz', 'Pravoúhlý sněm')
				->addTo($values->email, 'Přednášející')
				->addTo('pus@brnoc.cz')
				->setSubject('Potvrzení registrace přednášky | BrNOC 2016')
				->setHTMLBody("<p>Milý účastníku,<br>
					Tvá přednáška <strong>„.$values->name.“</strong> byla zaregistrována do systému. V nejbližších dnech Tě informujeme, zda bylo tvé téma schváleno.
					O přesném množství času, který budeš na svoji přednášku mít tě informujeme, až budeme mít zaregistrovaných více přednášek.<br />
					Pokud budeš chtít proplatit jízdné, hned nám <a href=\"mailto:pus@brnoc.cz\">napiš</a>.<br />
					Těšíme se na Tebe 2.‒4. 12. 2016 v Brně.</p><br>

					<p>V případě jakýchkoliv dotazů <a href=\"mailto:pus@brnoc.cz\">nás kontaktuj</a>.<br />
					<a href=\"http://www.brnoc.cz/right-angle/#right-angle\">Pravoúhlý sněm</a></p> ");



			$mailer = new SendmailMailer;

			$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
			if ($settings->local != 1) $mailer->send($mail);

		} else {
			$this->flashMessage('Účastník s tímto emailem neexistuje', 'danger');
		}
		$this->redirect('this');

	}

}
