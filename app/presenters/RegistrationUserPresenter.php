<?php
/**
 * Created by PhpStorm.
 * User: norik
 * Date: 25.12.15
 * Time: 19:05
 */

namespace App\Presenters;

use Nette,
	Nette\Application\UI\Form,
	Nette\Utils\DateTime,
	Nette\Mail\Message,
	Nette\Mail\SendmailMailer,
	Helpers;


class RegistrationUserPresenter extends Nette\Application\UI\Presenter
{
	/** @var  @var Nette\Databe\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	protected function createComponentRegisterUserForm()
	{
		$form = new Form;

		$form->addText('first_name', 'Jméno')
			->addRule(Form::MAX_LENGTH, 'Víc jak 42 znaků nepotřebuješ', 42)
			->setRequired();

		$form->addText('last_name', 'Příjmení')
			->addRule(Form::MAX_LENGTH, 'Víc jak 42 znaků nepotřebuješ', 42)
			->setRequired();

		$form->addText('nick', 'Přezdívka')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('data-placement', 'top')
			->setAttribute('title', 'Není povinná')
			->addRule(Form::MAX_LENGTH, 'Víc jak 42 znaků nepotřebuješ', 42);;

		$form->addText('school', 'Škola')
			->setRequired();

		$form->addText('email', 'Email')
			->setRequired()->addRule($form::EMAIL);

		$form->addText('mystery_topic', 'Co bys rád slyšel v Mystery room')
			->setRequired()
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Napiš jaké téma bys rád slyšel a my ho přidáme do klobouku v Mystery room. Pokud máš odvahu sám zkusit v Mystery room přednášet, zaškrtni následující políčko.');

		$form->addCheckbox('mystery_volunteer', 'Chtěl bych přednášet v Mystery room')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('title', 'Pokud máš odvahu sám zkusit v Mystery room přednášet, zaškrtni toto políčko.');

		$form->addCheckbox('first_day', 'Zúčastním se z pátku na sobotu (2.‒3. 12.)');

		$form->addCheckbox('second_day', 'Zúčastním se ze soboty na neděli (3.‒4. 12.)')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('title', 'Meziprogram + přednášková noc');

		$form->addText('favorite_math', 'Oblíbená matematická věta')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Není povinné');
		;

		$form->addText('favorite_physics', 'Oblíbený fyzikální princip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Není povinné');

		$form->addText('favorite_chemistry', 'Oblíbená organická sloučenina')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('title', 'Není povinné');

		$form->addSubmit('send', 'Registrovat se');

		$form->onSuccess[] = array($this, 'registerUserFormSucceeded');

		Helpers::bootstrapForm($form);

		return $form;
	}

	public function registerUserFormSucceeded($form, $values)
	{
		$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
		$era = $settings->era;

		$selection = $this->database->table('user');
		$temp = $selection->where('era LIKE ?', $era)->where('email LIKE ?', $values->email)->fetch();
		if (!$temp and ($values->first_day or $values->second_day)) {
			$this->database->table('user')->insert([
				'name' => $values->first_name,
				'last_name' => $values->last_name,
				'nick' => $values->nick,
				'school' => $values->school,
				'email' => $values->email,
				'mystery_topic' => $values->mystery_topic,
				'mystery_volunteer' => $values->mystery_volunteer,
				'favorite_math' => $values->favorite_math,
				'favorite_physics' => $values->favorite_physics,
				'favorite_chemistry' => $values->favorite_chemistry,
				'created' => new DateTime(),
				'era' => $era,
				'first_day' => $values->first_day,
				'Second_day' => $values->second_day
			]);

			$mail = new Message;
			$mail->setFrom('pus@brnoc.cz', 'Pravoúhlý sněm')
				->addTo($values->email, $values->name.' '.$values->last_name)
				->addTo('ron.norik@gmail.com')
				->setSubject('Potvrzení registrace | BrNOC 2016')
//				->setBody("Ahoj, \nbyl jsi přihlášen jako účastník BrNOCi 2016. \n
//					Důležité informace: \n
//					Akce se koná od 16:00 15. 4. do 10:00 16. 4.
//					S sebou něco na spaní (třeba spacák a karimatku), pokud jsi velký hladovec, vem si další jídlo,
//					ale se základním pokrytím hladu můžeš počítat (#bagety).\n
//					\nBrNOC tým");

				//					<p>Gratulujeme!<br />
//					Právě jsi byl(a) zaregistrován(a) na nejlepší akci všech dob (tedy až do našeho druhého ročníku).<br />
//					O tom nejdůležitějším tě budeme včas informovat mailem, ale pokud chceš mít informace z první ruky,
//					nezapomeň dál sledovat <a href=\"http://www.brnoc.cz\">web</a>.<br /><br />
//					Těšíme se na Tebe 18.‒19. 3. 2016 v Brně.</p>
//
//					<p>V případě jakýchkoliv dotazů <a href=\"mailto:pus@brnoc.cz\">nás kontaktuj</a>.<br />
//					<a href=\"http://www.brnoc.cz/right-angle/#right-angle\">Pravoúhlý sněm</a></p>
				->setHTMLBody("
					<p>Gratulujeme!<br />
					Právě jsi byl(a) zaregistrován(a) na druhý ročník nejlepší akce všech dob (tedy až do našeho 3. ročníku). <br>
					O tom nejdůležitějším tě budeme včas informovat mailem, ale pokud chceš mít informace z první ruky,
					nezapomeň dál sledovat <a href=\"http://www.brnoc.cz\">web</a>.<br />
					Plánujeme hned několik novinek:<br />
					<ul>
						<li>Dvě přednáškové noci po sobě (není problém jít jen na jednu)</li>
						<li>Sobotní odpolední meziprogram</li>
						<li>Možnost koupit si tričko (brzy přibyde na webu)</li>
					</ul><br>

					Těšíme se na Tebe 2.‒4. 12. 2016 v Brně.</p>

					<p>V případě jakýchkoliv dotazů <a href=\"mailto:pus@brnoc.cz\">nás kontaktuj</a>.<br />
					<a href=\"http://www.brnoc.cz/right-angle/#right-angle\">Pravoúhlý sněm</a></p>");

			$mailer = new SendmailMailer;

			$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
			if ($settings->local != 1) $mailer->send($mail);

			$this->flashMessage('Registrace proběhla úspěšně. Zaslali jsme Ti email s dalšími informacemi.', 'success');
		} else if (!($values->first_day or $values->second_day)) {
			$this->flashMessage('Musíš zaškrtnout alespoň jednu noc.', 'warning');
		} else {
			$this->flashMessage('Tento email už byl použit.', 'danger');
		}
		$this->redirect('this');
	}

	//private function//					Nazdar účastníku,<br><br>
//
//					včera jsme zveřejnili <a href=\"http://www.brnoc.cz/schedule/\">harmonogram</a>.
//					Připomínám, že je BrNOC z pátku na sobotu 18. - 19. 3.. Škola bude otevřena od 17:30 a evidovat na akci budeme masově do 18:15.
//					Pokud už teď víš, že přijdeš později, <a href=\"mailto:pus@brnoc.cz\">napiš nám</a>.
//					V případě problémů (nestíháš, dobýváš se do školy a nikdo neotvírá) volej na 774 533 316, popřípadě další čísla,
//					která budou v <a href=\"http://www.brnoc.cz/contact/\">kontaktech</a>, ať ti přijdeme otevřít. Na pozdě příchozí,
//					kvůli kterým budeme muset chodit ze super přednášek, se nebudeme dívat mile, tak raději přijď včas a s předstihem.<br><br>
//
//					Co s sebou:
//						<ul>
//						  <li><strong>přezůvky</strong></li>
//						  <li>spacák, karimatku (pokud budete přespávat - o přestávce / po přednáškách je možné odejít)</li>
//						  <li>dobrou náladu</li>
//						  <li>50 Kč</li>
//						</ul>
//					<br>
//
//					Co můžeš čekat:
//						<ul>
//						  <li>3 přednáškové místnosti</li>
//						  <li>1 Mystery room</li>
//						  <li>1 kecací místnost</li>
//						  <li>4 spací místnosti</li>
//						  <li>1 laboratoř (omezená kapacita - od 21:00 minimálně do 22:00 podle zájmu)</li>
//						  <li>jídlo (bagety, ovoce)</li>
//						  <li>pití (v rozumném množství na osobu)</li>
//						  <li>popůlnoční pravoúhlý dort (velikost dílku bude nepřímo úměrná počtu nespících účastníků)</li>
//						  <li>spoustu super lidí</li>
//						  <li>iluminátský koutek</li>
//						</ul>
//					<br>
//					Když budeš dělat bordel, nebo chodit v botách, ráno nám výrazně pomůžeš s úklidem. V každém případě bychom uvítali, pokud pomůžete dát třídy do pořádku. Na škole nebudeme sami, ale s <a href=\"http://ntrophy.cz/\">N-trophy</a>. K navigaci po škole hledej navigační šipky. Nebude žádná večerka, jen se nesmí po půlnoci mluvit u otevřeného okna a hlučet u spacích místností. Ráno <strong>bude</strong> budíček v 9:00.<br><br>


}
