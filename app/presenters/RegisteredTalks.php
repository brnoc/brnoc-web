<?php
/**
 * Created by PhpStorm.
 * User: norik
 * Date: 27.12.15
 * Time: 15:29
 */

namespace App\Presenters;

use Nette,
	Nette\Mail\Message,
	Nette\Mail\SendmailMailer,
	Nette\Application\UI\Form,
	Nette\Database\SqlLiteral,
	App\Model,
	Helpers;



class RegisteredTalksPresenter extends Nette\Application\UI\Presenter
{
	/** @var  @var Nette\Databe\Context */
	private $database;
	private $era;
	public $aera;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
		$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
		$this->era = $settings->era;
		$this->aera = $settings->era;
	}

	public function renderDefault()
	{
		$this->template->aera = $this->aera;
		$this->template->talks = $this->database->table('talk')->where('era = ?', $this->aera)->where('hidden = ?', 0)
			->order('name ASC');
		$this->redrawControl('list');
	}

	public function renderAdminHidden()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->template->talksHidden = $this->database->table('talk')
			->where('era = ?', $this->era)
			->where('hidden = ?', 1)
			->order('id DESC');
	}

	public function renderAdminShown()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->template->talksShown = $this->database->table('talk')
			->where('era = ?', $this->era)
			->where('hidden = ?', 0)
			->order('id DESC');
	}

	public function renderAdminShownListed()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->template->talksShownListed = $this->database->table('talk')
			->where('era = ?', $this->era)
			->where('hidden = ?', 0)
			->order('votes DESC');
	}


	public function handleDelete($id)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->database->table('talk')->where('id = ?', $id)->update(Array('hidden' => 2));

		//$this->redirect();
	}

	public function handleHot($id)
	{

		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$temp = $this->database->table('talk')->where('id = ?', $id)->fetch();
		if ($temp->hot == 0)
		{
			$this->database->table('talk')->where('id = ?', $id)->update(Array('hot' => 1));
		} else {
			$this->database->table('talk')->where('id = ?', $id)->update(Array('hot' => 0));
		}


	}

	public function handleSpare($id)
	{

		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$temp = $this->database->table('talk')->where('era = ?', $this->era)->where('id = ?', $id)->fetch();
		if ($temp->hot == 0)
		{
			$this->database->table('talk')->where('id = ?', $id)->update(Array('hot' => 2));

			$chosenTalk = $this->database->table('talk')->where('id = ?', $id)->fetch();

//			$count = $this->database->table('talk')->where('hot = ?', 2)->count('*');
//
//			$count += 1;
//
//			$mail = new Message;
//			$mail->setFrom('pus@brnoc.cz', 'Pravoúhelný sněm')
//				->addTo($chosenTalk->user->email, 'Přednášející') //i bez valuse to nejede
//				->addTo('pus@brnoc.cz')
//				->setSubject('Náhradní přednáška | BrNOC 2016')
//				->setHTMLBody("<p>Milý účastníku,<br>
//					Tvá přednáška <strong>".$chosenTalk->name."</strong> byla označena za náhradní. To znamená, že ti bude umožněno přednášet jako náhradník
//					za případné chybějící přednášející. Proto ti bohužel nemůžeme dopředu říct, jestli budeš přednášet a kolik na to budeš mít času. Náhradnící se budou vybírat
//					podle daného pořadí. Jsi ".$count.". náhradník.<br />
//					Toto by neměla být Tvá jediná přednáška, proto pokud budeš chtít proplatit jízdné, hned nám <a href=\"mailto:pus@brnoc.cz\">napiš</a>.<br />
//					Těšíme se na Tebe 18.‒19. 3. 2016 v Brně.</p><br>
//
//					<p>V případě jakýchkoliv dotazů, nebo pokud nechceš být náhradník, <a href=\"mailto:pus@brnoc.cz\">nás kontaktuj</a>.<br />
//					<a href=\"http://www.brnoc.cz/right-angle/#right-angle\">Pravoúhlý sněm</a></p> ");

			$mail = new Message;
			$mail->setFrom('pus@brnoc.cz', 'Pravoúhelný sněm')
				->addTo($chosenTalk->user->email, 'Přednášející') //i bez valuse to nejede
				->addTo('pus@brnoc.cz')
				->setSubject('Schválení přednášky | BrNOC 2016')
				->setHTMLBody("<p>Milý účastníku,<br>
					Tvá přednáška <strong>".$chosenTalk->name."</strong> byla schválena.
					O přesném množství času, který budeš na svoji přednášku mít, tě informujeme, až budeme mít zaregistrovaných ještě více přednášek.<br />
					Pokud budeš chtít proplatit jízdné, hned nám <a href=\"mailto:pus@brnoc.cz\">napiš</a>.<br />
					Těšíme se na Tebe 2.‒4. 12. 2016 v Brně.</p><br>

					<p>V případě jakýchkoliv dotazů <a href=\"mailto:pus@brnoc.cz\">nás kontaktuj</a>.<br />
					<a href=\"http://www.brnoc.cz/right-angle/#right-angle\">Pravoúhlý sněm</a></p> ");



			$mailer = new SendmailMailer;

			$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
			if ($settings->local != 1) $mailer->send($mail);

			$this->database->table('talk')->where('id = ?', $id)->update(Array('hidden' => 0));
		} else {
			$this->database->table('talk')->where('id = ?', $id)->update(Array('hot' => 0));
		}


	}

	public function handleApprove($id)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$chosenTalk = $this->database->table('talk')->where('id = ?', $id)->fetch();

		$mail = new Message;
		$mail->setFrom('pus@brnoc.cz', 'Pravoúhelný sněm')
			->addTo($chosenTalk->user->email, 'Přednášející') //i bez valuse to nejede
			->addTo('pus@brnoc.cz')
			->setSubject('Schválení přednášky | BrNOC 2016')
			->setHTMLBody("<p>Milý účastníku,<br>
					Tvá přednáška <strong>".$chosenTalk->name."</strong> byla schválena.
					O přesném množství času, který budeš na svoji přednášku mít, tě informujeme, až budeme mít zaregistrovaných ještě více přednášek.<br />
					Pokud budeš chtít proplatit jízdné, hned nám <a href=\"mailto:pus@brnoc.cz\">napiš</a>.<br />
					Těšíme se na Tebe 18.‒19. 3. 2016 v Brně.</p><br>

					<p>V případě jakýchkoliv dotazů <a href=\"mailto:pus@brnoc.cz\">nás kontaktuj</a>.<br />
					<a href=\"http://www.brnoc.cz/right-angle/#right-angle\">Pravoúhlý sněm</a></p> ");



		$mailer = new SendmailMailer;

		$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
		if ($settings->local != 1) $mailer->send($mail);

		$this->database->table('talk')->where('id = ?', $id)->update(Array('hidden' => 0));

		//$this->redirect();
	}

	public function handleHide($id)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->database->table('talk')->where('id = ?', $id)->update(Array('hidden' => 1));
	}


	protected function createComponentRateTalkForm()
	{
		$form = new Form;

		$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
		$era = $settings->era;

		$form->addText('email', 'Email')
			->setRequired()->addRule($form::EMAIL)
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('title', 'Zadej email, kterým ses přihlásil jako účastník.');;

		$talks = $this->database->table('talk')->where('hidden = ?', 0)->where('era = ?', $era)->order('name')->fetchAll();

		$array = [];
		foreach ($talks as $talk) {
			if ($talk->user->nick != NULL) {
				$name = $talk->name.' - '.$talk->user->name.' „'.$talk->user->nick.'“ '.$talk->user->last_name;
				if ($talk->id == 22){$name = $talk->name.' - '.$talk->user->name.' „'.$talk->user->nick.'“ '.$talk->user->last_name.' a Mgr. Václav „PI“ Piskač';}
				if ($talk->id == 13){$name = $talk->name.' - '.$talk->user->name.' „'.$talk->user->nick.'“ '.$talk->user->last_name.' a Stanislav Tabbrovec';}
			} else {
				$name = $talk->name.' - '.$talk->user->name.' '.$talk->user->last_name;
			}
			$array[$talk->id] = $name;
		}

		$form->addSelect('first', 'Vyber nejlepší přednášku:', $array)
			->setRequired('Prosím');

		$form->addSelect('second', 'Vyber 2. nejlepší přednášku:', $array)
			->setRequired('Prosím');

		$form->addSelect('third', 'Vyber 3. nejlepší přednášku:', $array)
			->setRequired('Prosím');

		$form->addCheckbox('shirt', 'Měl bys za rok zájem o BrNOC tričko?');

		$form->addCheckbox('again', 'Přijedeš i za rok?');

		$form->addCheckbox('longer', 'Kdyby byla BrNOC 3 dny, přijedeš na obě?');

		$form->addText('rate', 'Celková spokojenost')
			->setType('number')
			->addRule(Form::INTEGER)
			->addRule(Form::RANGE, 'Od 0 do 10',[0, 10])
			->setDefaultValue(10)
			->setRequired()
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('title', '0 - fakt špatný | 10 - naprosto boží');

		$form->addText('rateFood', 'Spokojenost s jídlem')
			->setType('number')
			->addRule(Form::INTEGER)
			->addRule(Form::RANGE, 'Od 0 do 10',[0, 10])
			->setDefaultValue(10)
			->setRequired()
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('title', '0 - fakt špatný | 10 - naprosto boží');

		$form->addText('rateTalks', 'Spokojenost s přednáškami')
			->setType('number')
			->addRule(Form::INTEGER)
			->addRule(Form::RANGE, 'Od 0 do 10',[0, 10])
			->setDefaultValue(10)
			->setRequired()
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('title', '0 - fakt špatný | 10 - naprosto boží');

		$form->addText('ratePus', 'Spokojenost s organizací')
			->setType('number')
			->addRule(Form::INTEGER)
			->addRule(Form::RANGE, 'Od 0 do 10',[0, 10])
			->setDefaultValue(10)
			->setRequired()
			->setAttribute('data-toggle', 'tooltip')
			->setAttribute('data-placement', 'top')
			->setAttribute('data-trigger', 'hover')
			->setAttribute('title', '0 - fakt špatný | 10 - naprosto boží');

		$form->addTextArea('dream', 'Co bys chtěl na příští BrNOCi?')
			->addRule(Form::LENGTH, 500)
			->setAttribute('placehoder', 'Vířivka, sauna...');

		$form->addTextArea('message', 'Intimní vzkaz Pravoúhlému sněmu')
			->addRule(Form::LENGTH, 500)
			->setAttribute('placehoder', 'Jen to řekni...');

		$form->addCheckbox('iluminati', 'Iluminati confirmed')
			->setRequired('Q.E.D.');


		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = array($this, 'rateTalkFormSucceeded');

		Helpers::bootstrapForm($form);

		return $form;
	}


	public function rateTalkFormSucceeded($form, $values)
	{
		$users = $this->database->table('user');
		$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
		$era = $settings->era;

		$temp = $users->where('era = ?', $era)->where('email = ?', $values->email)->limit(1)->fetch();

		if ($temp){
			if ($temp->voted == 0) {
				$this->database->table('user')->where('era = ?', $era)->where('email', $values->email)->update([
					'voted' => 1,
					'first' => $values->first,
					'second' => $values->second,
					'third' => $values->third,
					'shirt' => $values->shirt,
					'again' => $values->again,
					'longer' => $values->longer,
					'rate' => $values->rate,
					'rateFood' => $values->rateFood,
					'rateTalks' => $values->rateTalks,
					'ratePus' => $values->ratePus,
					'dream' => $values->dream,
					'message' => $values->message,
				]);
				$this->flashMessage('Odpovědi úspěšně odeslány', 'success');
			} else {
				$this->flashMessage('Účastník s tímto emailem už hlasoval', 'danger');
			}
		} else {
			$this->flashMessage('Účastník s tímto emailem neexistuje', 'danger');
		}


//		$temp = $this->registrationTalkModel->createTalk($values);
//
//		if ($temp)
//		{
//			$this->flashMessage('Registrace proběhla úspěšně, podívej se do své emailové schránky', 'success');
//
//			$mail = new Message;
//			$mail->setFrom('pus@brnoc.cz', 'Pravoúhlý sněm')
//				->addTo($values->email, 'Přednášející')
//				->addTo('pus@brnoc.cz')
//				->setSubject('Potvrzení registrace přednášky | BrNOC 2016')
//				->setHTMLBody("<p>Milý účastníku,<br>
//					Tvá přednáška <strong>„.$values->name.“</strong> byla zaregistrována do systému. V nejbližších dnech Tě informujeme, zda bylo tvé téma schváleno.
//					O přesném množství času, který budeš na svoji přednášku mít tě informujeme, až budeme mít zaregistrovaných více přednášek.<br />
//					Pokud budeš chtít proplatit jízdné, hned nám <a href=\"mailto:pus@brnoc.cz\">napiš</a>.<br />
//					Těšíme se na Tebe 18.–19. 3. 2016 v Brně.</p><br>
//
//					<p>V případě jakýchkoliv dotazů <a href=\"mailto:pus@brnoc.cz\">nás kontaktuj</a>.<br />
//					<a href=\"http://www.brnoc.cz/right-angle/#right-angle\">Pravoúhlý sněm</a></p> ");
//
//
//
//			$mailer = new SendmailMailer;
//
//			$mailer->send($mail);
//		} else {
//			$this->flashMessage('Účastník s tímto emailem neexistuje', 'danger');
//		}
		$this->redirect('this');

	}

	public function handlePreviousEra($aera)
	{
		$this->aera = max($aera - 1, 1);
	}

	public function handleNextEra($aera)
	{
		$this->aera = min($aera + 1, $this->era);
	}
}
