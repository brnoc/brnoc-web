<?php
/**
 * @author Ronald Luc
 */

namespace App\Presenters;

use Nette;

class StatisticsPresenter extends Nette\Application\UI\Presenter
{
	/** @var  @var Nette\Databe\Context */
	private $database;
	private $era;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
		$settings = $this->database->table('settings')->where('id = ?', 1)->fetch();
		$this->era = $settings->era;
	}


	public function renderAdmin()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:admin');
		}

		$this->template->mysteryTopic = $this->database->table('user')->where('era = ?', $this->era)->fetchAll();

		$this->template->ourSchool = $this->database->table('user')->where('school LIKE ? OR school LIKE ?', '%Jaroš%', '%jaroš%')->where('era = ?', $this->era)->count('*');

		$this->template->mysteryVolunteer = $this->database->table('user')->where('mystery_volunteer')->where('era = ?', $this->era)->count('*');

		$this->template->talks = $this->database->table('talk')->order('lenght')->where('era = ?', $this->era)->fetchAll();

		$this->template->talksApproved = $this->database->table('talk')->where('hidden = ?', 0)->order('lenght')->where('era = ?', $this->era)->fetchAll();

		$this->template->talkers = $this->database->table('talk')->where('era = ?', $this->era)->count("DISTINCT user_id");

		$this->template->talksLength = $this->database->table('talk')->where('era = ?', $this->era)->sum('lenght');

		$this->template->talksLengthApproved = $this->database->table('talk')->where('hidden = ?', 0)->where('era = ?', $this->era)->sum('lenght');

		$this->template->talksVotes = $this->database->table('talk')->where('hidden = ?', 0)->where('era = ?', $this->era)->order('votes DESC')->fetchAll();

		$this->template->numberOfVoters = $this->database->table('user')->where('voted', 1)->where('era = ?', $this->era)->count('*');

		$this->template->usersNtrophy = $this->database->table('user')->where('ntrophy', 1)->where('era = ?', $this->era)->fetchAll();

		$this->template->mysteryApproved = $this->database->table('mystery')->where('id > 0')->where('era = ?', $this->era)->fetchAll();

		$this->template->d1 = $this->database->table('user')->where('era = ?', $this->era)->where('first_day = 1')->count('*');

		$this->template->d2 = $this->database->table('user')->where('era = ?', $this->era)->where('second_day = 1')->count('*');

		$this->template->dBoth = $this->database->table('user')->where('era = ?', $this->era)->where('first_day = 1')->where('second_day = 1')->count('*');
	}

	public function handleApprove($user_id)
	{

		$user = $this->database->table('user')->where('id', $user_id)->fetch();
		$count = $this->database->table('mystery')->where('name', $user->mystery_topic)->count('*');
		if ($count == 0) {
			$this->database->table('mystery')->insert([
				'name' => $user->mystery_topic,
				'year' => 2016,
				'era' => $this->era,
			]);
		} else {
			$this->database->table('mystery')->where('name', $user->mystery_topic)->delete();
		}

		$this->redrawControl('mystery');
	}
}
