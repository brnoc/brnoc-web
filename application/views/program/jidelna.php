<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="author" content="Pravoúhlý sněm">
    <meta nema="keywords" content="brnoc brněnská přednášková noc přednášky jaroška gymnázium brno třída kapitána jaroše"/>
    <meta name=”description” content="program akce">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#000000">

    <link rel="stylesheet" href="<?=base_url()?>css/jidelna.css"/>

  </head>
  <body>
    <div id="time">22:00</div>
    <?php foreach(["A","B","C","D"] as $room):?>
      <div class="room">
        <div class="roomTitle" id="<?=$room?>">Místnost <?=$room?></div>
        <div class="now">
          <div class="subtitle">Nyní:</div>
          <div class="talkTitle"><span class="speaker">Dominik Beck</span>: Libová přednáška</div>
        </div>
        <div class="next">
          <div class="subtitle">Následuje:</div>
          <div class="talkTitle"><span class="speaker">Jan Pokorný</span>: Ještě libovější přednáška</div>
        </div>
      </div>
    <?php endforeach;?>

    <script>
    function setTime(){
      var date = new Date();
      var string = date.getHours()+":"+date.getMinutes()+":"+nul(date.getSeconds());
      document.getElementById("time").innerHTML = string;
    }

    function nul(string){
      string = "000"+string;
      string = string.substring(string.length-2);
      return string;
    }

    setInterval(setTime, 1000);
    </script>

  </body>
</html>
