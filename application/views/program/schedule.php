<?php
$pauseTitles = [
  "Zahájení v aule",
  "I. Přestávka",
  "II. Přestávka",
  "III. Přestávka",
  "Zakončení v aule",
  "Volný program",
  "9:00 Budíček, snídaně, úklid školy"
];
foreach(json_decode($pauses) as $key => $value) {
  $title = "";
  if(isset($pauseTitles[$key]))$title = $pauseTitles[$key];
  $talks[] = [
    "title" => $title,
    "speaker" => "",
    "start" => $value[0], "end" => $value[1],
    "room"=>"",
    "type" => "P",
    "fullwidth" => boolval(in_array($key, array(0, 1, 2, 6,7,8,9,10,11,12))),
  ];
//  print_r($value);
}

/*$talks = array_merge($talks, [
  ["title" => "Workshop", "speaker" => "Nějakej týpek", "start" => 3, "end" => 14, "room" => 4, "type" => "W", "special_room" => "hudebna"],
  ["title" => "Pauza", "speaker" => "", "start" => 8, "end" => 8, "room" => 1, "type" => "P"],
  ["title" => "Pauza", "speaker" => "", "start" => 8, "end" => 8, "room" => 2, "type" => "P"],
  ["title" => "Pauza", "speaker" => "", "start" => 8, "end" => 8, "room" => 3, "type" => "P"],
])*/

$startOfSchedule = -2; //o kolik půlhodin je to posunuto oproti 18:00
$startOfTalks = 1; //o kolik půlhodin je to posunuto oproti 18:00
?>


<main>
  <h1>Rozvrh přednášek</h1>
  <section>
    <h2>Ke stažení</h2>
    <ul style="list-style-type: none">
      <li>Celý program v .pdf (~ 100 kB) (<a download="program-brnoc" href="<?=base_url()?>docs/program.pdf">Stáhnout</a> | <a target="_blank" href="<?=base_url()?>docs/program.pdf">Zobrazit</a>)</li>
      <li>Celý program v .png (~ 200 kB) (<a download="program-brnoc" href="<?=base_url()?>img/program/program.png">Stáhnout</a> | <a target="_blank" href="<?=base_url()?>img/program/program.png">Zobrazit</a>)</li>
    <!--  <li>Plánek 2. podlaží (1650&times;2340, 114 kB) (<a download="planek-brnoc-2" href="<?=base_url()?>img/planek/2-podlazi.png">Stáhnout</a> | <a target="_blank" href="<?=base_url()?>img/planek/2-podlazi.png">Zobrazit</a>)</li>-->
    <!--  <li>Plánek 3. podlaží (1650&times;2340, 103 kB) (<a download="planek-brnoc-3" href="<?=base_url()?>img/planek/3-podlazi.png">Stáhnout</a> | <a target="_blank" href="<?=base_url()?>img/planek/3-podlazi.png">Zobrazit</a>)</li>-->
    </ul>
  </section>
  <section id="scheduleSection">
    <h2>Normální program</h2>
    <div class="less650">Posouvej doprava/doleva pro další učebny</div>
    <div id="scheduleWrapper" class="show1">
      <div class="schedule">
        <div class="swipeRight header" onclick="swipe('right')"><i class="fas fa-chevron-left"></i></div>
        <div class="header col col1" style="grid-column: 2;">A</div>
        <div class="header col col2">B</div>
        <div class="header col col3">C</div>
        <div class="header col col4">D</div>
        <div class="header col col5">E</div>
        <div class="header col col6">W1</div>
        <div class="header col col7">W2</div>
        <!--<div class="header col col6"><span>W<sub>2</sub><span></div>
        <div class="header col col7"><span>W<sub>3</sub><span></div>-->
        <div class="swipeLeft header" onclick="swipe('left')"><i class="fas fa-chevron-right"></i></div>
      <?php for($i = 0; $i < 22;$i++):?>
        <div class="time" style="grid-row:<?=$i+2/*+$startOfSchedule*/?>; grid-column:1 / span 8"><span><?php
          $time = 1080+($i+$startOfSchedule)*30;
          echo ($time/60%24).":".substr("0".$time%60,-2);
          //echo ($time/60)>=24?"<br>sobota":"<br>pátek"; //odpoznámkovat pro dvoudenní akci
        ?></span></div>
      <?php endfor;?>
      <?php foreach($talks as $key => $talk):?>
        <div class="talkWrapper <?=isset($talk["type"])?$talk["type"]:""?> col col<?=$talk["room"]?>"style="
          grid-row: <?=intval($talk['start'])-$startOfSchedule+$startOfTalks?> / <?=intval($talk['end'])-$startOfSchedule+$startOfTalks?>;
          grid-column: <?=isset($talk['fullwidth'])?($talk['fullwidth']?'2 / span 5':'2 / 7'):intval($talk['room'])+1?>
          ">
          <div class="talkFront" onclick="window.location.assign('<?=base_url()?>program/list#talk<?=isset($talk["id"])?$talk['id']-1:""?>')">
            <div class="talkTitle">
              <?=$talk['title']?>
            </div>
            <div class="talkSpeaker">
              <?=$talk['speaker']?>
            </div>
            <div class="talkRoom">
              <?=isset($talk["special_room"])?$talk["special_room"]:""?>
            </div>
          </div>
        </div>
      <?php endforeach;?>
      <div>
    </div>
  </section>
</main>

<script>
document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;
var yDown = null;

function getTouches(evt) {
  return evt.touches ||             // browser API
         evt.originalEvent.touches; // jQuery
}

function handleTouchStart(evt) {
    xDown = getTouches(evt)[0].clientX;
    yDown = getTouches(evt)[0].clientY;
};

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
        if ( xDiff > 0 ) {
            swipe("left");
        } else {
            swipe("right");
        }
    } else {
        if ( yDiff > 0 ) {
            /* up swipe */
        } else {
            /* down swipe */
        }
    }
    /* reset values */
    xDown = null;
    yDown = null;
};

var colShow = 1;

function swipe(dir){
  if(dir == "left")colShow = Math.min(colShow+1,7);
  if(dir == "right")colShow = Math.max(colShow-1,1);
  document.getElementById("scheduleWrapper").classList.remove("show1");
  document.getElementById("scheduleWrapper").classList.remove("show2");
  document.getElementById("scheduleWrapper").classList.remove("show3");
  document.getElementById("scheduleWrapper").classList.remove("show4");
  document.getElementById("scheduleWrapper").classList.remove("show5");
  document.getElementById("scheduleWrapper").classList.remove("show6");
  document.getElementById("scheduleWrapper").classList.remove("show7");
  document.getElementById("scheduleWrapper").classList.add("show"+colShow);
}

function toggle(el){
  var talks = document.getElementsByClassName('talkWrapper');
  if(el.classList.contains("active"))
    el.classList.remove("active");
  else{
    Array.prototype.forEach.call(talks,talk => {
      talk.classList.remove('active');
    });
    el.classList.add('active');
  }
  console.log($(el));
}

</script>
