<?php
$votes = isset($_SESSION["user"])?json_decode($_SESSION["user"]["pref"]):false;

?>
<main>
  <h1>Hlasování</h1>
  <section>
    Označ přednášky od&nbsp;1&nbsp;(nejméně, vlevo) do&nbsp;5&nbsp;(nejvíce, vpravo) podle toho, jak moc bys na&nbsp;ně chtěl jít. Podle odpovědí účastníků pak vytvoříme rozvrh přednášek. (Hlasy se ukládají automaticky.)<br>
    Děkujeme, že hlasuješ, díky tomu ti můžeme udělat program na míru! <br>
    Výsledky hlasování o preferencích i o přednáškách budou po akci zveřejněny v tabulce.<br><br>

    <div class="legend">
      <div class="T leg" ><span class="fas fa-circle"></span> &hellip; přednáška</div>
      <div class="D leg" ><span class="fas fa-circle"></span> &hellip; vystoupení</div>
      <div class="S leg" ><span class="fas fa-circle"></span> &hellip; speciální host</div>
      <div class="W leg" ><span class="fas fa-circle"></span> &hellip; workshop</div>
      <div class="M leg" ><span class="fas fa-circle"></span> &hellip; mystery room</div>
    </div>
  </section>
  <section id="voteSection">
  <?php foreach($talks as $talk):?>
    <div class="talkWrapper <?=$talk["type"]?>">
      <div class="talkHead" onclick="toggle(this.parentElement)">
        <div class="talkTitle">
          <?=$talk['title']?>
        </div>
        <div class="talkSpeaker">
          <?=$talk['speaker']?><?=($talk["with"])?", ".$talk["with"]:""?>
        </div>
      </div>
      <div class="talkShort" onclick="toggle(this.parentElement)">
        <?=$talk['short']?> (<?=($talk["length"]<600)?$talk["length"]."&nbsp;min":"celý večer"?>)
      </div>
      <div class="talkLong" onclick="toggle(this.parentElement)">
        <div>
          <?=$talk['long']?>
          <p>
          Délka: <?=($talk["length"]<600)?$talk["length"]."&nbsp;minut":"celý večer"?>
        </div>
      </div>
      <div class="vote">
        <span class="icon fas fa-thumbs-down"></span>
        <?php $v = isset($votes[$talk["id"]])?$votes[$talk["id"]]:0?>
        <?php for($i = 1; $i<=5; $i++):?>
          <span onclick="vote(this,<?=$talk["id"]?>,<?=$i?>)" class="<?=($i==$v)?"bold":""?> cir"></span>
        <?php endfor;?>
        <span class="icon fas fa-thumbs-up"></span>
      </div>
    </div>
  <?php endforeach;?>
  </section>
  <script>
  function toggle(el){
    var talks = document.getElementsByClassName('talkWrapper');
    if(el.classList.contains("active"))
      el.classList.remove("active");
    else{
      Array.prototype.forEach.call(talks,talk => {
        talk.classList.remove('active');
      });
      el.classList.add('active');
    }
  }

  var votes = <?=$_SESSION["user"]["pref"]?>;

  function vote(el, talk, val){
    Array.prototype.forEach.call(el.parentElement.children, span => {span.classList.remove("bold")});
    el.classList.add("bold");
    votes[talk] = val;
    $.ajax({
      url : "<?=base_url()?>API/user/vote",
      type: "POST",
      data: {
        vote: JSON.stringify(votes),
      },
      success: (response) => {console.log(response)},
      error: (status, xhr, error) => {console.log(status, xhr, error)}
    });
  }
  </script>
</main>
