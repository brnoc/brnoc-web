<main>
  <h1>Seznam přihlášených přednášek</h1>
  <div class="legend">
    <div class="T leg" ><span class="fas fa-circle"></span> &hellip; přednáška</div>
    <div class="D leg" ><span class="fas fa-circle"></span> &hellip; vystoupení</div>
    <div class="S leg" ><span class="fas fa-circle"></span> &hellip; speciální host</div>
    <div class="W leg" ><span class="fas fa-circle"></span> &hellip; workshop</div>
    <div class="M leg" ><span class="fas fa-circle"></span> &hellip; mystery room</div>
    <div class="F leg" ><span class="fas fa-circle"></span> &hellip; film</div>
  </div>
  <section id="listSection">
  <?php foreach($talks as $talk):?>

        <div class="talkWrapper <?=$talk["type"]?>" onclick="toggle(this)" id="talk<?=$talk['id']?>">
          <div class="talkHead">
            <div class="talkTitle">
              <?=$talk['title']?>
            </div>
            <div class="talkSpeaker">
              <?=$talk['speaker']?><?=($talk["with"])?", ".$talk["with"]:""?>
            </div>
          </div>
          <div class="talkShort">
            <?=$talk['short']?> (<?=($talk["length"]<600)?$talk["length"]."&nbsp;min":"celý večer"?>)
          </div>
          <div class="talkLong">
            <div>
            <?=$talk['long']?>
            <p>
            Délka: <?=($talk["length"]<600)?$talk["length"]."&nbsp;minut":"celý večer"?>
          </div>
          </div>
        </div>

    <?php endforeach; ?>
  </section>
  <script>
  function toggle(el){
    var talks = document.getElementsByClassName('talkWrapper');
    if(el.classList.contains("active"))
      el.classList.remove("active");
    else{
      Array.prototype.forEach.call(talks,talk => {
        talk.classList.remove('active');
      });
      el.classList.add('active');
    }
  }
  </script>
</main>
