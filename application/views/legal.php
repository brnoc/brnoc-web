<main id="legal">
  <h1>Podmínky</h1>
  <section>
    <h2>Podmínky užívání stránek a&nbsp;ochrana osobních údajů</h2>
    <article>
      <h2>Podmínky užívání stránek</h2>
      <p>Provozovatelem těchto webových stránek <em>https://brnoc.cz</em> (dále jen &#8222;(webové) stránky&#8220;) je Pravoúhlý spolek, sídlem Na&nbsp;pískové cestě 267/60, Brno, IČO: 08574472 (dále jen &#8222;provozovatel&#8220;). Veškeré údaje a&nbsp;obsah nacházející se na&nbsp;webových stránkách podléhají autorskému zákonu č.121/2000 Sb. Bez&nbsp;souhlasu provozovatele je uživateli zakázáno užívat webové stránky jinak než pro&nbsp;vlastní potřebu, jiné použití je podmíněno povolením provozovatele.</p>
    </article>
    <article>
      <h2>Ochrana osobních údajů</h2>
      <p>Veškeré informace získané od uživatele jsou považovány za&nbsp;osobní údaje a&nbsp;jsou chráněny podle zákona č. 101/2000 Sb. Zpracování dat se řídí Obecným nařízením na ochranu osobních údajů, známým jako GDPR. Údaje nejsou poskytovány třetím osobám a&nbsp;slouží výhradně pro&nbsp;potřeby provozovatele.</p>
      <p>Tyto webové stránky využívají soubory cookies pro&nbsp;zlepšení kvality služeb. Za&nbsp;účelem statistik jsou shromažďovány některé údaje jako IP adresa zařízení nebo verze prohlížeče.</p>
      <p>Souhlasí-li uživatel, aby mu byl zasílán newsletter, dává současně souhlas s&nbsp;využitím osobních údajů, potřebných k vykonání této služby. Tento souhlas lze kdykoliv odvolat.</p>
      <p>Osobní údaje poskytnuté uživatelem budou dlouhodobě uchovávány pro&nbsp;zlepšení kvality služeb a&nbsp;statistické účely. Pokud uživatel přímo a&nbsp;důvodně požádá o smazání osobních informací z&nbsp;databází, většina těchto údajů bude odstraněna.</p>
      <p>Svou registrací na těchto webových stránkách uživatel souhlasí, že v&nbsp;případě své osobní účasti na&nbsp;akci BrNOC dává provozovateli těchto webových stránek právo na zveřejnění fotografií, na nichž uživatel vystupuje. Tento souhlas lze ze závažných důvodů odvolat. Provozovatel si vyhrazuje právo posoudit závažnost důvodů předložených uživatelem a&nbsp;rozhodnout o odvolání uživatelova souhlasu.</p>
    </article>
    <article>
      <h2>Odkazy na&nbsp;jiné stránky</h2>
      <p>Na&nbsp;webových stránkách se nacházejí odkazy na&nbsp;jiné webové stránky. Při&nbsp;návštěvě těchto stránek by se měl uživatel seznámit s&nbsp;pravidly užívání těchto webových stránek. Za&nbsp;jakékoliv problémy způsobené návštěvou těchto webových stránek nenese provozovatel odpovědnost.</p>
    </article>
    <article>
      <h2>Ustanovení</h2>
      <p>Provozovatel si vyhrazuje právo změnit Podmínky užívání a&nbsp;povinností uživatele je tyto změny sledovat. Provozovatel neručí za&nbsp;aktuálnost a&nbsp;správnost informací nacházejících se na&nbsp;webových stránkách a&nbsp;nenese odpovědnost za žádné škody způsobené navštívením a&nbsp;užitím stránek.</p>
      <p>Používáním webových stránek uživatel vyjadřuje souhlas s&nbsp;Podmínkami užívání stránek. V&nbsp;případě potíží se obraťte na&nbsp;správce webových stránek Daniela Perouta.</p>
    </article>
  </section>

  <section>
    <h2>Podmínky pořádání přednášek</h2>
    <article>
      <h3>Obecná ustanovení</h3>
      <p>Přednášejícím se stává takový uživatel, který zažádal o pořádání přednášky. Tento status ztrácí zamítnutím přednášky. O svém statutu je uživatel informován. Přednášejícím je odpuštěno vstupné.</p>
      <p>Zažádání o přednášku lze provést jak pomocí webového rozhraní (preferovaná varianta), tak pomocí <a href="mailto:pus@brnoc.cz">emailu</a>. Jako přednáška se považuje i workshop.</p>
    </article>
    <article>
      <h3>Zveřejnění přednášky</h3>
      <p>Společně se souhlasem k&nbsp;natáčením přednášky přednášející dává souhlas se zveřejněním své přednášky na platoformě <span class="fab fa-youtube"></span> YouTube či jiné platoformě třetí strany. Tato přednáška bude viditelná veřejně. Přednášející má právo svoji přednášku stáhnout.</p>
    </article>
  </section>
  <section id="tricka">
    <h2>Podmínky objednávání triček</h2>
    <article>
      <h3>Rezervace trička</h3>
      <p>Uživatel si může během registrace nebo v&nbsp;uživatelské sekci zarezervovat až tři trička v&nbsp;různých velikostech. Tím mu vzniká nárok při případném prodeji triček na&nbsp;akci požadovat rezervovaný počet triček v&nbsp;jím vybraných velikostech. V&nbsp;případě, že k&nbsp;prodeji triček na&nbsp;akci nedojde, uživatel <strong>nemá</strong> na&nbsp;svá rezervovaná trička nárok.</p>
      <p>Rezervace triček nemusí být vždy možná. Rezervace je časově omezena do&nbsp;data uvedeného na&nbsp;webových stránkách.</p>
    </article>
    <article>
      <h3>Objednání triček</h3>
      <p>Uživatel si může v uživatelské sekci předobjednat trička, tedy zaplatit svá rezervovaná trička nebo volná trička, jsou-li skladem. Tímto vzniká normální obchod mezi provozovatelem a&nbsp;uživatelem.</p>
      <p>Objednávání triček nemusí být vždy možné.</p>
    </article>
  </section>
  <section><em>Stav k 17.&nbsp;10.&nbsp;2019. 5. revize</em></section>
</main>

