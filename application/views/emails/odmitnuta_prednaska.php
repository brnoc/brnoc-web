<html>
  <body>
    <img src="<?=base_url()?>img/mail_header.png">
    <div>
      Ahoj <?=zavolejJmeno($user->firstname)?>,<br>
      tvá přednáška <b><?=$talk?></b> na letošní BrNOC byla odmítnuta. <br>
      Těšíme se na tebe na BrNOCi!
      <hr>
      Za Pravoúhlý sněm,<br>
      <?=$_SESSION["user"]["firstname"]?> <?=$_SESSION["user"]["lastname"]?>.
    </div>
  </body>
</html>
