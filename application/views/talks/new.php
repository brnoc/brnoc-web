<script>
  var a = [];
  function toggle(v){
    var i = a.indexOf(v);
    if (i === -1)
        a.push(v);
    else
        a.splice(i,1);
      document.getElementById("subjectSelect").value=JSON.stringify(a);
  }
</script>

<main>
  <?= validation_errors("<div class=\"msg error\">","</div>")?>
  <h1>Přihlášení nové přednášky</h1>
  <section id="formSection">
    <form method="post" action="<?=base_url()?>talks/new">
      <fieldset class="itemsMargins">
        <legend>Název a popis přednášky</legend>
        <input type="text" class="title" name="title" placeholder="Název přednášky..." value="<?=set_value('title')?>" required>
        <textarea name="short_desc" placeholder="Krátký popis vaší přednášky... (max 160 znaků)" maxlength="160" required><?=set_value('short_desc')?></textarea>
        <textarea name="long_desc" placeholder="Celý popis vaší přednášky... (max 1000 znaků)" maxlength="1000" required><?=set_value('long_desc')?></textarea>
        <label for="subject">Vyber, do kterých okruhů přednáška spadá:</label><input id="subjectSelect" type="hidden" name="subject" placeholder="Informatika, Matematika, Biologie, ..." max="20"required>

        <div class="subjectSelect">
        <?php foreach(["Matematika","Informatika","Biologie","Chemie","Fyzika","Pedagogika","Geografie","Kartografie","Zeměpis","Kultura","Psychologie","Cestování","Medicína","Astronomie","Filozofie","Společenské vědy","Sociologie","Robotika","Auta","Budoucnost","Sex","Poezie","Hudba","Drama"] as $subject):?>
          <div class="label" onclick="this.classList.toggle('active'); toggle(this.innerHTML)"><?=$subject?></div>
        <?php endforeach;?>
        </div>
      </fieldset>

      <fieldset class="itemsMargins">
        <legend>Délka přednášky</legend>
        <span>Délka přednášky v minutách<br>
        <em style="font-size: 0.9em">Jednomu přednášejícímu pravděpodobně nebude schváleno více než 120 přednáškominut.</em></span>
        <div class="slidecontainer">
          <input name="length" type="range" min="30" max="90" value="60" step="30" class="slider" id="myRange">
          <p>Zvolená délka je <span id="demo"></span> minut.</p>
        </div>
      </fieldset>
      <fieldset class="itemsMargins">
        <legend>Další požadavky</legend>
        <label for="special">Lepší vybavení učebny:<br>
        <em style="font-size: 0.9em">Samozřejmostí je, že v učebně, kde budeš přednášet, bude počítač, projektor a tabule na křídy/fixy. Máš nějaký speciální požadavek, o jehož zařízení bychom se mohli pokusit?</em></label>
        <textarea name="special" placeholder="např. reproduktory&hellip;"></textarea>
        <label class="radioContainer">Přeji si, aby moje jízdné bylo proplaceno<input name="jizdne" type="checkbox"><span class="checkmark"></span></label>
        <label class="radioContainer">Chci, aby moje přednáška byla natáčena<input onclick="" name="record" type="checkbox"><span class="checkmark"></span><br>(Z organizačních důvodů nebude možné natáčet a zpracovat všechny přednášky, proto prosíme potvrď, zda opravdu chceš natáčení.)</label>
      </fieldset>
      <fieldset>
        <strong>Upozornění:</strong><br>
        S přihlášením přednášky souhlasíte s <a href="<?=base_url()?>/legal">podmínkami</a> o pořádání přednášky.<br>
        <input type="submit" name="submit" value="Přihlásit"/>
      </fieldset>
    </form>
  </section>
</main>
<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
  output.innerHTML = this.value;
  if(this.value==42) {output.style.color = "red";}
  else {output.style.color = "inherit";}
}
</script>
