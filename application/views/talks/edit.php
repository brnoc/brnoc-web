<main>
  <h1>Editace detailů přednášky</h1>
  <form method="post" action="<?=base_url()?>talks/update/<?=$talk['id']?>">
    <p>
      <input type="text" class="title" name="title" placeholder="Název přednášky..." value="<?=$talk['title']?>" required><br>
      <textarea name="short_desc" placeholder="Krátký popis vaší přednášky... (max 160 zanků)" maxlength="160" required><?=$talk['short']?></textarea><br>
      <textarea name="long_desc" placeholder="Celý popis vaší přednášky... (max 1000 znaků)" maxlength="1000" required><?=$talk['long']?></textarea><br>
      Přednáška spadá do okruhu: <input type="text" name="subject" placeholder="Informatika, Matematika, Biologie, ..." max="20" value="<?=$talk['subject']?>" required>
    </p>

    <p>
      Na přednášku chci: <input type="number" name="length" min="15" max="90" step="15" value="<?=$talk['length']?>" required> minut.<br> (Hodnota bude zaokrouhlena na 15 minut, min 15, max 90.) <br>
      Jednomu přednášejícímu pravděpodobně nebude schváleno více než 120 přednáškominut.
    </p>

    <p>
      Samozřejmostí je, že v učebně, kde budeš přednášet, bude počítač, projektor a tabule na křídy / fixy. Máš ještě nějaké další požadavky?
      <textarea name="special"><?=$talk['special']?></textarea>
    </p>

    <p>
      Preferovaný čas přednášky:<br>
      <input type="radio" name="pref_time" value="1" required <?=($talk['pref_time'] == 1)?"checked":""?>> Pátek večer<br>
      <input type="radio" name="pref_time" value="2" required <?=($talk['pref_time'] == 2)?"checked":""?>> Sobota dopoledne<br>
      <input type="radio" name="pref_time" value="3" required <?=($talk['pref_time'] == 3)?"checked":""?>> Sobota odpoledne
    </p>

    <p>
      <input type="checkbox" name="jizdne" <?=($talk['jizdne'])?"checked":""?>> Přeji si proplatit jízdné</br>
    </p>

    <p>
      <input type="checkbox" name="record" <?=($talk['record'])?"checked":""?>> Přeji si aby moje přednáška byla natáčena</br>
    </p>

    <input type="submit" name="submit" value="Změnit"/>
  </form>
</main>
