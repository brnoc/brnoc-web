<main>
  <h1>Přednáška byla odmítnuta</h1>
  <section id="toBeConfirmed">
    <div class="talk active">
      <div class="pointer">
        <div class="title"><?=$talk['title']?></div>
        <div class="speaker"><?=$talk['speaker']?></div>
      </div>

      <div class="short_desc">

        <?=$talk['short_desc']?>
      </div>

      <div class="long_desc">

        <?=$talk['long_desc']?>
      </div>

      <div class="controls">
        <a href="<?=base_url()?>user/talks_admin"><div class="btn">Zpět do administrace</div></a>
        <a href="<?=base_url()?>talks/undo"><div class="btn">Vrátit</div></a>
      </div>
    </div>
  </section>
</main>
