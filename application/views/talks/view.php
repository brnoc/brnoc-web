<main>
  <h1><?=$talk['title']?></h1>
  Délka přednášky: <?=$talk['length']?> minut
  <p>Obor:<?$talk['subject']?>
  <p><?=$talk['short']?></p>
  <p><?=$talk['long']?></p>
  <p>Speciální požadavky:<br><?=$talk['special']?></p>
  <p>Proplatit jízdné:<?=$talk['jizdne']?"ano":"ne"?></p>
  <p>Čas přednášky:
    <?=($talk['pref_time']==1)?"Pátek večer":""?>
    <?=($talk['pref_time']==2)?"Sobota dopoledne":""?>
    <?=($talk['pref_time']==3)?"Sobota odpoledne":""?>
  </p>
  <p>Přednáška <?=$talk['record']?"bude":"nebude"?> nahrávána</p>
  <?php if($perm):?>
    <a class="notA"  href="<?=base_url()?>talks\edit\<?=$talk['id']?>"><div class="btn">Edit</div></a>
    <a class="notA"  href="<?=base_url()?>talks\delete\<?=$talk['id']?>"><div class="btn">Delete</div></a>
  <?php endif;?>
  <a class="notA"  href="<?=base_url()?>user\talks"><div class="btn">Zpět na profil</div></a>
</main>
