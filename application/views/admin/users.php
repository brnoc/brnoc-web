<?php
  $data = [];
  $sum = 0;
  $legend = [];
  foreach ($users as $user) {
    $data[] = [$user->id, $user->name, $user->firstname, $user->lastname, $user->email, $user->role];
    $chart[$user->role] = (isset($chart[$user->role]))?$chart[$user->role]+1:1;
    $sum++;
    if(!in_array($user->role,$legend))$legend[] = $user->role;
  }

  $colors = ["#7A0099", "#9D3BD6", "#F75C03", "#27E891", "#7A8489"];
  $chart_data = [
    "Admin" => ['color' => "#710099", 'value' => 0],
    "admin" => ['color' => "#9D3BD6", 'value' => 0],
    "speaker" => ['color' => "#F75C03", 'value' => 0],
    "user" => ['color' => "#27E891", 'value' => 0],
    "unverified" => ['color' => "#7A8489", 'value' => 0],
  ];
  foreach($users as $user){
    if($chart_data[$user->role])$chart_data[$user->role]['value']++;
  }

  $all = 0;
  $abs = [["Den", "Počet registrovaných účastníků"]];
  foreach($stats as $key => $value){
    if($key != "Den"){
    $all += $value[1];
    $abs[] = [$value[0], $all];}
  }

?>

<div id="cover" onclick="toggleChart(document.getElementById('curve_chart'))"></div>
<main id="users" class="sidebarLayout">
  <div class="left">
    <div class="title">
      <h1>Uživatelské účty</h1>
      Klikni na řádek pro zobrazení možností.
    </div>
    <div class="tableWrapper">
      <table id="table" class="tablesTable"></table>
    </div>
  </div>
  <div class="sidebar">
    <div class="chart">
      <b>Uživatelé účty:</b>
      <svg id="chart"></svg>


    </div>
    <div class="chart">
      <b>Registrace uživatelů:</b>
      <div class="toggleWrapper">
        Relativní
        <div class="toggle" onclick="this.classList.toggle('active'); toggleChartData();"><div class="ball"></div></div>
        Absolutní
      </div>
      <div id="curve_chart" style="width: 100%; min-width:20em;" onclick="toggleChart(this)"></div>
      (klikni pro zvětšení)
    </dvi>
  </div>
</main>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
var data = <?=json_encode($data);?>

var table = new Table({
  head: ["Id","Nick", "Jméno", "Příjmení", "Email", "Role"],
  data: data,
  root: document.getElementById("table"),
  renderDd: true,
  dropdown: (row, key) => {
    var div = document.createElement("div");
    var userRole=row[5].value;
    var html="";
    ["Admin","admin","speaker","user","unverified"].forEach(role => {
      html += "<div onclick=\"set_role('"+role+"',"+row[0].value+","+key+")\" class=\"roleButton\ "+((role==userRole)?"active":"")+"\">"+role+"</div>";
    })
    div.innerHTML = html;
    return div;
  }
});

table.render();


function set_role(role, id, key){
  console.log("Setting role: \""+role+"\" for user: "+id+".");
  $.ajax({
    url:"<?=base_url()?>API/users/set_role",
    type: "POST",
    data: {
      role: role,
      id: id,
    },
    success: (response) => {
      console.log(response);
      table.conf.data[key][5] = role;
      table.parse_data();
      table.render();
    },
    error: function(xhr,status,error){
      console.log(xhr);
    }
  });
}

  // render user role chart
  var chart = new PieChart(<?=json_encode($chart_data)?>, document.getElementById('chart'),{renderLegend:true});
  chart.render();
  console.log(<?=json_encode($stats)?>);

  // render user registration chart
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(() => drawChart(chartData));

  var relative = <?=json_encode($stats)?>;
  var absolute = <?=json_encode($abs)?>;
  var chartData = relative;

  function drawChart(data) {
    var data = google.visualization.arrayToDataTable(
      data
    );

    var options = {
      title: 'Registrace uživatelů',
      curveType: 'function',
      legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

    chart.draw(data, options);
  }

  function toggleChart(el){
    var cover = document.getElementById('cover');
    cover.classList.toggle("active");
    el.classList.toggle('active');
    while(document.getElementById("curve_chart").firstChild)document.getElementById("curve_chart").firstChild.remove();
    drawChart(chartData);
  }

  var chartAbs = false;
  function toggleChartData(){
    chartAbs = !chartAbs
    chartData = chartAbs?absolute:relative;
    while(document.getElementById("curve_chart").firstChild)document.getElementById("curve_chart").firstChild.remove();
    drawChart(chartData);
  }
</script>
