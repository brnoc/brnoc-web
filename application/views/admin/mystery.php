<?php
$app = [];
$new = [];
foreach($topics as $topic){
  if($topic->approved == "1")$app[] = ["",$topic->topic,$topic->id];
  else $new[] = [$topic->id,$topic->topic,""];
}
?>

<main id="mystery">
  <div class="head">
    <h1>Mystery room</h1>
  </div>
  <div class="body">
    <div class="tableWrapper left">
      <table class="tablesTable" id="newTable"></table>
    </div>
    <div class="icon"><i class="fas fa-exchange-alt"></i></div>
    <div class="tableWrapper right">
      <table class="tablesTable" id="approvedTable"></table>
    </div>
  </div>






  <script>
  var approvedData = <?=json_encode($app)?>;
  var newData = <?=json_encode($new)?>;

  var approvedTable = new Table({
    head:["Akce", "Schválená témata","Id"],
    data:approvedData,
    root: document.getElementById('approvedTable'),
  });
  approvedTable.set_column(0,{
    render: () => text_cell(`<div class="arrow"><i class="fas fa-arrow-left"></i><div>`)
  });
  approvedTable.render();

  var newTable = new Table({
    head:["Id","Navrhnutá témata", "Akce"],
    data:newData,
    root: document.getElementById('newTable'),
  });
  newTable.set_column(2,{
    render: (value, row) => {return text_cell(`<div class="arrow" onclick="confirm_topic(${row[0]});"><i class="fas fa-arrow-right"></i><div>`)}
  });
  newTable.render();

  function confirm_topic(topic){


      approvedTable.data.body.push(newTable.data.body[newTable.get_row_key_by_value(0,topic)]);
      newTable.data.body.splice(newTable.get_row_key_by_value(0,topic),1);
      approvedTable.render();

  }

  </script>
</main>
