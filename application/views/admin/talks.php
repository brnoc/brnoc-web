<?php
    function notNul($int, $else = 1){
      return ($int==0)?$else:$int;
    }


    //create table data array
    $talks_table_data = [];
    foreach($talks as $talk){
      $talks_table_data[] = [
        intval($talk["id"]),
        $talk["title"],
        $talk["speaker"],
        intval($talk["length"]),
        $talk["type"],
        $talk["status"],
        intval($pref[$talk["id"]]["sum"]/notNul($pref[$talk["id"]]["count"])*100)/100,
        $pref[$talk["id"]]["count"],
        intval($feedback[$talk["id"]]["sum"]/notNul($feedback[$talk["id"]]["count"])*100)/100,
        $feedback[$talk["id"]]["count"],
        $talk["timestamp"],
      ];
    }

    //create talk infro array
    $talks_info = [];
    foreach ($talks as $value) {
      $talks_info[$value["id"]] = [
        "id" => $value["id"],
        "long" => $value["long"],
        "short" => $value["short"],
        "pozn" => $value["special"],
        "length" => $value["length"],
        "speaker_id" => $value["speaker_id"],
        "status" => $value["status"],
        "with" =>$value["with"],
      ];
    }

    $speakers = [];
    foreach ($talks as $talk){
      $speakers[$talk["speaker_id"]] = [
        "name" => $talk["speaker"],
        "talkminutes_all" => isset($speakers[$talk["speaker_id"]])?$speakers[$talk["speaker_id"]]["talkminutes_all"]+$talk["length"]:$talk["length"],
        "talkminutes_confirmed" => isset($speakers[$talk["speaker_id"]])?($speakers[$talk["speaker_id"]]["talkminutes_confirmed"]+(($talk["status"] == "A")?$talk["length"]:0)):(($talk["status"] == "A")?$talk["length"]:0),

      ];
    }

?>
<main class="sidebarLayout">
  <div class="left">
    <div class="head">
      <h1>Administrace přednášek</h1>
      <i class="fas fa-exclamation-triangle" style="color:red"></i> Svítí-li obálka (vpravo), odešle se při schválení/odmítnutí přednášky automaticky email přednášjícímu.<br>
      <i class="fas fa-info-circle" style="color:#387dff"></i> Klikni na přednášku pro zobrazení možností.<br>
      <i class="fas fa-info-circle" style="color:#387dff"></i> Klikni na nadpis sloupce pro seřazení a zobrazení grafu.<br>
    </div>
    <div class="body">
      <div class="tableWrapper"><table id="talks" class="tablesTable"></table></div>
    </div>
  </div>
  <div class="sidebar">
    <div class="settings">
      <b>Nástroje:</b><br>
      <script>
        var send_mail = true;
        function toggle_mail(element){
          element.classList.toggle("active");
          send_mail = !send_mail;
          element.title = send_mail?"Odesílání emailů je zapnuté!":"Odesílání emailů je vypnuté.";
        }
      </script>
      <i class="fas fa-envelope active" onclick="toggle_mail(this)" title="Odesílání emailů je zapnuté!"></i>
    </div>
    <div class="chart">
      <b>Přednášky:</b><br>
      <svg id="chart"></svg>
    </div>
    <div class="stats">
      <b>Statistika:</b><br>
      <table>
        <tr title="Počet přednášejících"><td><i class="fas fa-chalkboard-teacher"></i></td><td> <?=$info["speakers"]?> speakers </td></tr>
        <tr title="Počet schválených přednášek"><td><i class="fas fa-chalkboard"></i></td><td> <?=$info["talks"]?> talks </td></tr>
        <tr title="Počet schválených přednáškominut"><td><i class="far fa-clock"></i></td><td> <?=$info["talk_minutes_confirmed"]?> min (<?=intval($info["talk_minutes_confirmed"]/1440)?>d <?=intval($info["talk_minutes_confirmed"]/60%24)?>h <?=intval($info["talk_minutes_confirmed"]%60)?>min) </td></tr>
        <tr title="Počet všech přednáškominut"><td><i class="fas fa-clock"></i></td><td> <?=$info["talk_minutes_all"]?> min (<?=intval($info["talk_minutes_all"]/1440)?>d <?=intval($info["talk_minutes_all"]/60%24)?>h <?=intval($info["talk_minutes_all"]%60)?>min) </td></tr>
      </table>
    </div>
    <div id="userInfo" class="userInfo">
      <b>Info o přednášejícím:</b>
    </div>
  </div>
  </main>

<script>
    var talks_data = <?=json_encode($talks_table_data)?>;
    var talks_info = <?=json_encode($talks_info)?>;
    var speakers = <?=json_encode($speakers)?>;
    var table = new Table({
      head: ["Id","Název", "Přednášející", "Délka", "Typ", "Schváleno","Preference","z","Hodnocení","z","Datum"],
      data: talks_data,
      root: document.getElementById("talks"),
      renderDd: true,
      dropdown: (row, key) => {
        var div = document.createElement('div')
        div.className = "dropdown";
        var info = talks_info[row[0].value];

        console.log(info);


        div.innerHTML = `
          <div class="info">
            <div class="short"><b>Krátký popis: </b><br>${info.short}</div>
            <div class="long"><b>Celý popis: </b><br>${info.long}</div>
            <div class="poznamka" ${(!info.pozn)?"style=\"display:none\"":""}><b>Poznámka přednášejícího:</b><br>${info.pozn}</div>
            <div class="spoluprednasejici" ${(!info.with)?"style=\"display:none\"":""}><b>Spolupřednášející:</b> ${info.with}</div>
          </div>
          <div class="controls">
            <div class="btn" style="background-color:green" onclick="confirm_talk(${info.id},${send_mail})">Schválit</div>
            <div class="btn" style="background-color:red" onclick="reject_talk(${info.id},${send_mail})">Odmítnout</div>
          </div>
        `;

        var speaker = speakers[info.speaker_id];

        document.getElementById("userInfo").innerHTML = `
          <b>Informace o přednášejícím:</b><br>
          <span class="fas fa-user"></span>: ${speaker.name}<br>
          <span class="far fa-clock" title="Schválené přednáškominuty"></span>: <span class="${(speaker.talkminutes_confirmed>120)?'alert':''}">${speaker.talkminutes_confirmed}</span> <span ${(info.status=="A")?"style=\"display:none\"":""} class="${(speaker.talkminutes_confirmed+info.length>120)?'alert':''}">(+${info.length})</span><br>
          <span class="fas fa-clock" title="Všechny registrované přednáškominuty"></span>: <span class="${(speaker.talkminutes_all > 120)?'alert':''}">${speaker.talkminutes_all}</span><br>
        `

        return div;
      },
      renderSum: true,
      renderAvg: true,
      renderMed: true,
    });
    table.set_column(2, {
      headerCallback:() => {
        var data = table.get_column_values(2);
        talkschart.rawData = data;
        talkschart.render();
      }
    })
    table.set_column(3, {
      render:(val) => text_cell(val + " min"),
      setStyle: (val) => (val>120)?"background-color:red":false,
      headerCallback:() => {
        var data = table.get_column_values(3);
        talkschart.rawData = data;
        talkschart.render();
      }
    });
    table.set_column(4, {
      render:(val) => {
        switch (val) {
          case "T": return text_cell("Talk");
          case "W": return text_cell("Workshop");
          case "D": return text_cell("Divadlo");
          case "S": return text_cell("Spec. host");
          case "M": return text_cell("M. Room");
          case "F": return text_cell("Film");
          default: return text_cell(val);
        }
      },
      setStyle:(val) => {
        switch (val) {
          case "T": return "background-color:#444";
          case "W": return "background-color:#004";
          case "D": return "background-color:#400";
          case "S": return "background-color:#040";
          case "M": return "background-color:#404";
          case "F": return "background-color:#B40";
        }
      },
      headerCallback:() => talkschart.set_data(parse_chart_data(table.get_column_values(4),{S:"Speciální host",W:"Workshop",D:"Divadlo",M:"Mystery room",T:"Talk"},{T:"#666",W:"#006",S:"#060",D:"#600",M:"#606"})).render(),
    });
    table.set_column(5, {
      render: (val) => {
        var txt;
        switch (val) {
          case "A":txt = "Schváleno"; break;
          case "R":txt = "Odmítnuto"; break;
          case "N":txt = "Nové"; break;
        }
        return text_cell(txt);
      },
      setStyle: (val) => {
        switch (val) {
          case "A": return "background-color:green";
          case "R": return "background-color:red";
          case "N": return "background-color:orange";
        }
      },
      headerCallback: () => talkschart.set_data(parse_chart_data(table.get_column_values(5),{N:"Nové",R:"Odmítnuto",A:"Schváleno",}, {R:"red",A:"green",N:"orange",})).render(),
    });

    table.set_column(6, {
      render: (val, row) => bar(val,5, true, "#8f00a1"),
    })

    table.set_column(8, {
      render: (val, row) => bar(val,5, true, "#4800A1"),
    })

    table.set_column(10, {
      render: (val) => text_cell(val.substring(0,10)),
      //compute: (val) => val.substring(0,10),
    });
    table.sort(10);

    function confirm_talk(id,mail = false){
      $.ajax({
        url:"<?=base_url()?>API/talks/confirm/"+id+"/"+mail,
        success: (response) => {
          table.data.body[table.get_row_key_by_value(0,id)][5].val = "A";
          table.render();
          console.log(response);
        },
        error: (xhr,status,error) => console.log(xhr),
      });
    }

    function reject_talk(id, mail = false){
      $.ajax({
        url:"<?=base_url()?>API/talks/reject/"+id+"/"+mail,
        success: (response) => {
          table.data.body[table.get_row_key_by_value(0,id)][5].val = "R";
          table.render();
          console.log(response);
        },
        error: (xhr,status,error) => console.log(xhr),
      });
    }

    function parse_chart_data(values, legend, colors){
      var data = {};
      for(var key in legend){
        if(!(typeof values[key] == "undefined")){
          data[legend[key]] = {value: values[key].value, color: colors[key]};
        };
      }
      return data;
    }

    var talkschart = new PieChart(
      parse_chart_data(table.get_column_values(4),{S:"Speciální host",W:"Workshop",D:"Divadlo",M:"Mystery room",T:"Talk"},{T:"#666",W:"#006",S:"#060",D:"#600",M:"#606"}),
      document.getElementById("chart"),
      {renderLegend:true}
    );

    function export_talks(){
      exp = [];
      expData = [];

      table.data.body.forEach(row => {
        exp.push([row[0].val, row[2].val, row[1].val, row[6].val, row[7].val, row[8].val, row[9].val]);
        expData[row[0].val] = {
        	short_desc:talks_info[row[0].val].short,
        	long_desc:talks_info[row[0].val].long,
        };
      });

      console.log("data", JSON.stringify(exp));
      console.log("desc", JSON.stringify(expData));

      var string = `
      <!doctype html>
      <html>
      <head>
        <style>
          body{
            background-color:black;
            color:white;
          }
        </style>
        <script src="<?=base_url()?>js/tables.js"><\/script>
        <link rel="stylesheet" href="<?=base_url()?>css/tables.css"/>
      </head>
      <body>
        <table id="table" class="tablesTable"></table>
        <script>

        var data = ${JSON.stringify(exp)};

        var desc = ${JSON.stringify(expData)};

        var table = new Table({
          head: ["Id","Přednášející", "Přednáška", "Průměr preferencí", "z", "Průměr hlasů", "z"],
          data: data,
          root: document.getElementById("table"),
          renderAvg: true,
          renderSum: true,
          renderMed: true,
          dropdown: (row, key) => {
            key = row[0].value;

            var div = document.createElement('div');
            div.innerHTML = '<div class="short">'+desc[key].short_desc+"</div>" + '<br><div class="long">'+desc[key].long_desc+"</div>";
            return div;
          },
          renderDd: true,
        });


        table.set_column(3,{
          render: (val) => bar(val,5, true, "#8f00a1"),
        });


        table.set_column(5,{
          render: (val) => bar(val,5, true),
        });
        table.render();
        <\/script>
        </body>
        </html>
        `;

        var win = window.open();
        win.document.title = "Export přednášek";
        win.document.write(string);
        win.stop();
        win.location.reload();
    }

  </script>
