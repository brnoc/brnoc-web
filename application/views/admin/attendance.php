

<!--<div id="email">
  <h2>Odeslat email</h2>
  <div id="emails"></div>
  <form method="post">
    <textarea id="mytextarea">
      <h1>Hello World</h1>
      <p>Lorem ipsum dolor sit amet...</p>
      <p>Za Pravoúhlý sněm, <br> <?=$_SESSION["user"]["firstname"]?> <?=$_SESSION["user"]["lastname"]?>.
    </textarea>
  </form>
  <div class="btn" id="send">Odeslat</div>
  <script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=dp3ssiaslhiyjch0hgx0zpn6vllqz2oh7uwnsiomnfoxhfw1'></script>
  <script>
    tinymce.init({
      selector: '#mytextarea',
      plugins: [
        'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        'save table contextmenu directionality emoticons template paste textcolor'
      ],
      toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | code',
      content_style:"body{color:white; background-color:black;}",
      height:300,
    });
    </script>
</div>-->

<main id="attendance">

  <div id="cover" onclick="close_users_detail()" class="inv"></div>

  <div id="userDetail" class="inv">
    <span class="fas fa-times close" onclick="close_users_detail()"></span>
    <h2>Detail uživatele: <span id="userDetailName"></span></h2>


      <div id="userDetailSpeakerIcon" onclick="toggle('speaker')" class="icon"><span class="fas fa-volume-up"></span></div>
      <div id="userDetailOrgIcon" onclick="toggle('organizer')" class="icon"><span class="fas fa-user-ninja"></span></div>



      <h3>Dorazil na akci</h3>
      <div id="userDetailAttended" onclick="toggle('attended')">
        Účastník <span class="attended">ne</span>dorazil na akci.
      </div>



      <h3>Zaplatit</h3>
      <div id="userDetailPay" onClick="open_pay_form()">
        Účastníkovi zbývá doplatit <span id="pay"></span> Kč.
      </div>
      <table id="userDetailPayInputWrapper">
        <tr>
          <td><input type="number" id="userDetailPayInput"></td>
          <td><div id="userDetailPayButton" onclick="pay()">Připsat platbu</div></td>
        </tr>
      </table>



      <style>
        #ordersTable thead{
          background-color:unset;
        }

        #ordersTable td{
          border:none;
        }
      </style>
      
      <h3 style="display:none">Objednávky</h3>
      <table id="ordersTable" style="display:none" onclick="toggle_release()">
        <thead >
          <tr><td>S</td><td>M</td><td>L</td>
        </thead>
        <tdbody>
          <tr><td>0</td><td>0</td><td>0</td>
        </tbody>
      </table>
        


      <div id="userDetailConsentWrapper">
        <h3>Souhlas rodičů</h3>
        <div class="consent plus18inner yes">Účatník je starší 18 let, není tedy povinen souhlas odevzdat.</div>
        <div class="consent consFinner no" onclick="toggle('consent')">Účastník je povinen odevzdat souhlas rodičů.</div>
        <div class="consent consTinner yes" onclick="toggle('consent')">Účastník odevzdal souhlas rodičů.</div>
      </div>

      <h3>Speciální sleva</h3>
      <div id="userDetailDiscWrapper">
        <div id="userDetailDisc" onClick="open_disc_form()"></div>
        <div class="inner">
          <input type="number" id="userDetailDiscInput"/><div class="userDetailDiscButton" onclick="save_disc()">Uložit</div>
        </div>
      </div>


      <h3>Poznámka</h3>
      <div id="userDetailCommentWrapper">
        <div id="userDetailComment" onclick="edit_comment()">Poznámka</div>
        <div id="userDetailSaveCommentButton" onclick="save_comment()">Uložit</div>
      </div>


  </div>

  <div class="left">
    <div class="attendanceSectionHead">
       <h1>Prezenční listina</h1>
       <div id="toolbar">
         <i onclick="load()" class="fas fa-sync-alt" title="Reload"></i> <input type="text" id="serchBar" placeholder="Hledej..." onkeyup="search()"/>
       </div>
     </div>
    <div class="tableWrapper">
       <table id="table" cellspacing="0px">
       <thead>
         <tr>
           <td onclick="sort('id')">Id</td>
           <td onclick="sort('firstname')" >Jméno</td>
           <td onclick="sort('lastname')" >Příjmení</td>
           <td onclick="sort('age')">Věk</td>
           
           <td onclick="sort('speaker')"  title="přednášející (speaker)"><i class="fas fa-volume-up"></i></td>
           <td onclick="sort('organizer')"  title="organizátor"><i class="fas fa-user-ninja"></i></td>
           <!--<td onclick="sort('socks')"  title="ponožky(92kč)"><i class="fas fa-socks"></i></td>-->
           <td onclick="sort('paid')" >Zaplaceno</td>
           <td onclick="sort('pay')" >Zaplatit</td>
           <td onclick="sort('attended')">Přišel</td>
           <td onclick="sort('consent')" title="souhlas rodičů"><span class="far fa-file-pdf"></td>
           <td onclick="sort('disc')">Sleva</td>

         </tr>
       </thead>
       <tbody id="tbody">
         <tr><td colspan="100" style="text-align:center;">
           <img src="<?=base_url()?>img/open.png" width="25%" id="loading" style="transition:1.5s; margin:5%">
            <script>
              var rotate = 1;
              var loading = document.getElementById("loading");
              setInterval(() => {loading.style.transform = "rotate("+(rotate++)*180+"deg)";}, 2000);
            </script>
         </td></tr>
       </tbody>
     </table>
   </div>
  </div>

  <div class="sidebar" id="sidebar">
    <div class="controls">

        <div class="btn" onclick="prezencka()">Automatický tisk prezenčky</div>
        <div class="btn" onclick="print_nametags(data)">Automatický tisk jmenovek</div>
        V případě problémů prosím zkontroluj, zda tvůj prohlížeč neblokuje vyskakovací okna.
    </div>
    <div id="tshirts">
        
    </div>
    <div id="userCount">

    </div>
    <div class="newFilter">
      <strong>Vytvořit nový filtr:</strong></p>
      <div class="inputs">
      <select id="newFilterNameInput">
        <option>sloupec</option>
        <?php foreach(["id","firstname","lastname","age","consent","speaker","organizer","orders","paid","pay","attended","disc"] as $val):?>
          <option value="<?=$val?>"><?=$val?></option>
        <?php endforeach;?>
      </select>
      <select id="newFilterOperationInput">
        <option>operace</option>
        <?php foreach(["","!","<",">","="] as $val):?>
          <option value="<?=$val?>"><?=$val?></option>
        <?php endforeach;?>
      </select>
      <input type="number" id="newFilterValueInput"/>
    </div>
    <div class="btn" onclick="new_filter()">Přidat</div>
    </div>


    <div id="filters" class="filters"></div>

    <div id="log" class="log"></div>
  </div>

</main>

<script>
  var user;
  var data;
  var currentData;

  function log_message(message, type){
    var mess = document.createElement('div');
    var span1 = document.createElement('span');
    var span2 = document.createElement('span');
    var d = new Date();
    span1.className = "time";
    span1.innerHTML = ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2)  + ":" + ('0' + d.getSeconds()).slice(-2);
    span2.className = "text"
    span2.innerHTML = message;
    mess.className = "message "+type;
    mess.appendChild(span1);
    mess.appendChild(span2);
    document.getElementById('log').appendChild(mess);
    document.getElementById('log').scrollTop = document.getElementById('log').scrollHeight;
  }

  log_message("Hello World!","");

  class Filterer{
    constructor(data, root){
      this.filters = data;
      this.root = root;
    }

    filter(data){
      var dataNew = [];
      Array.prototype.forEach.call(this.filters, filter => {
        Array.prototype.forEach.call(data, row => {
          if(filter.filter(row)) dataNew.push(row);
        });
        data = dataNew;
        dataNew = [];
      });
      return data;
    }

    render(){
      if(!this.root)return 0;
      while(this.root.firstChild)this.root.firstChild.remove()
      this.filters.forEach(filter => {
        var div = document.createElement("div");
        div.className = "filter";
        var span1 = document.createElement("span");
        span1.innerHTML = (typeof filter.title !== 'undefined')?filter.title:"Filter";
        span1.className = "text";
        var span2 = document.createElement("span");
        span2.className = "fas fa-times-circle";
        span2.onclick = () => render(this.remove(filter).filter(data));
        div.appendChild(span1);
        div.appendChild(span2);
        this.root.appendChild(div);
      });
    }

    add(filt){
      if(!this.filters.includes(filt))this.filters.push(filt);
      console.log(this.filters);
      this.render();
      return this;
    }

    remove(filt){
      if(this.filters.includes(filt))this.filters.splice(this.filters.indexOf(filt),1);
      console.log(this.filters);
      this.render();
      return this;
    }

    toggle(filt){
      if(!this.filters.includes(filt))this.filters.push(filt);
      else this.filters.splice(this.filters.indexOf(filt),1);
      console.log(this.filters);
      this.render();
      return this;
    }
  }

  class Filter{
    constructor(conf){
      this.name = conf.name;
      this.operation = (typeof conf.operation !== 'undefined')?conf.operation:"";
      this.value = (typeof conf.value !== 'undefined')?conf.value:"";
      this.title = (typeof conf.title !== 'undefined')?conf.title:"";
    }

    filter(row){
      if(Array.isArray(row[this.name])){
        a = 0;

      }
      switch(this.operation){
        case "!":
          if(!row[this.name]) return true;
          break;
        case "<":
          if(row[this.name] < this.value)return true;
          break;
        case ">":
          if(row[this.name] > this.value)return true;
          break;
        case "=":
          if(row[this.name] == this.value)return true;
          break;
        default:
         if(row[this.name]) return true;
      }
      return false;
    }
  }

  var filterer = new Filterer([], document.getElementById('filters'))

  function new_filter(){
    name = document.getElementById("newFilterNameInput").value; console.log(name);
    operation = document.getElementById("newFilterOperationInput").value; console.log(operation);
    value = document.getElementById("newFilterValueInput").value; console.log(value);

    var filter = new Filter({
      name: name,
      operation: operation,
      value: value,
      title: name,
    });

    console.log(filter);
    filterer.add(filter);
    render(filterer.filter(data));
  }

  function newsletter(){
    var ids = [];
    var emails = [];
    Array.prototype.forEach.call(filterer.filter(data), user => {
      ids.push(user.id);
      emails.push(user.email);
    });

    return {ids: ids, emails: emails};
  }

  function count(){
    var c = 0;
    Array.prototype.forEach.call(filterer.filter(data), user => {
      c += 1;
    });
    return c;
  }

  function print_list(data, title, alph = false){
    if(alph)data = sort_data(data,"lastname");
    var attendees = window.open("");
    var doc = attendees.document;
    doc.write("<style>td,tr{border:1px solid #000}.sign{width:40%}table{width:100%}td{padding:5px 8px}thead{font-weight:700}h1{margin:0}</style>");

    doc.write("<table cellspacing=\"0\"><thead><tr><td colspan=\"400\"><header><h1>Prezenční listina &ndash; "+ title +"</h1>Název akce: BrNOC 6<br>Datum: 13. &ndash; 14. 12. 2019 <br>Místo: Gymnázium Brno, třída Kapitána Jaroše</header></td></tr><tr><td>Jméno</td><td>Příjmení</td><td>Účast na akci</td></tr></thead><tbody>");

    Array.prototype.forEach.call(data, row => {
      doc.write("<tr>");
      doc.write("<td>"+row.firstname+"</td>");
      doc.write("<td>"+row.lastname+"</td>");
      doc.write("<td class=\"sign\"></td>");
      doc.write("</tr>");
    });

    doc.write("</tbody></table>");
    doc.title = "Prezenčka|"+title;

    attendees.stop();
  }

  var talks =<?=json_encode($talks)?>

  function print_nametags(data){
    var win = window.open("");
    var doc = win.document;
    doc.title = "BrNOC|Jmenovky";
    doc.write("<style>@import url(https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i);.org{background-color:#000; color:#FFF;}.org img{filter:invert(100%)}.tag{float:left;vertical-align:top;border:1px solid #000;page-break-inside:avoid;display:inline-block;height:5.2cm;width:9cm;position:relative;font-family:Roboto}.talkTitle,img{position:absolute}.name{padding:.5cm .5cm .2cm;font-weight:700;font-size:1.5em}img{height:3cm;right:1cm;bottom:.5cm}.role{font-style:italic;padding:0 .5cm .2cm}.talkTitle{right:1.25cm;bottom:1.8cm;width:2.85cm;height:1.45cm;text-align:center;overflow:hidden}</style>");
    doc.write("<div class=\"wrapper\">");

    var tags = [];

    var exceptions = {
      /*81: {
        img: "dozor",
        role: "Dozor",
      },
      82:{
        img: "dozor",
        role: "Dozor",
      },
      114:{
        img: "dozor",
        role: "Dozor",
      },
      149:{
        img: "dozor",
        role: "Dozor",
      },
      145:{
        img:"press",
        role: "Reportérka",
        cls: "blue",
      },
      182:{
        img:"press",
        role: "Reportérka",
        cls: "blue",
      },
      196:{
        img:"photo",
        role: "Fotografka",
        cls: "blue",
      },
      
      13:{
        img: "sklad",
        role: "Sklad",
        cls: "blue",
      },*/
      350:{
        img:"press",
        role: "Reportér",
      },
      190:{
        img:"photo",
        role: "Fotografka",
      },
      266:{
        talkTitle: "Jak vypadnout ze sterotypu, aneb studuj v zahraničí"
      },
      218:{
        talkTitle: "Jsem na vysoké a neumím vařit. Pomoc!"
      },
      306:{
        talkTitle: "Jak vypadnout ze sterotypu, aneb studuj v zahraničí"
      },
      289:{
        talkTitle: "My A Ne Ty! Tentokrát na Jarošce ????????"
      },
      232:{
        talkTitle: "Borneo - život na opuštěných ostrovech"
      },
      295:{
        talkTitle: "Neuvěříte vlastním očím"
      },
      331:{
        img: "dozor",
        role: "Dozor",
      },
      351:{
        img: "dozor",
        role: "Dozor",
      },
      352:{
        img: "dozor",
        role: "Dozor",
      },
      353:{
        img: "dozor",
        role: "Dozor",
      },
    };

    Array.prototype.forEach.call(data, user => {
      var role = ["Účastník"];
      var img = "guest";
      var talkTitle = "";
      var displayTitle = true;
      var cls = "";

      if(user.organizer){
        role.push("Organizátor");
        img = "org";
        cls = "org";
      }

      if(user.speaker){
        role.push("Přednášející");
        img = "speaker";
        talkTitle = talks[user.id];
      }


      if(typeof(exceptions[user.id])!="undefined"){
        if(typeof(exceptions[user.id].role)!="undefined") role.push(exceptions[user.id].role);
        if(typeof(exceptions[user.id].img)!="undefined") img = exceptions[user.id].img;
        if(typeof(exceptions[user.id].cls)!="undefined") cls = exceptions[user.id].cls;
        if(typeof(exceptions[user.id].talkTitle)!="undefined") talkTitle = exceptions[user.id].talkTitle;
      }



      var subtitle = "";
      role.forEach(i => subtitle+=i+"<br>");

      tags.push({
        name: user.firstname+" "+user.lastname,
        role: subtitle,
        img: img,
        talkTitle: talkTitle,
        cls: cls,
        sort: user.lastname,
      });

    });

    for(var i = 0; i < 9; i++)
    tags.push({
        name: "____________________",
        role: "Účastník",
        img: "guest",
        talkTitle: "",
        cls: "",
        sort: "A",
    });

    tags.sort((a,b) => {
      if(a.sort > b.sort) return 1;
      if(a.sort < b.sort) return -1;
      return 0;
    });


    tags.forEach(tag => {
      doc.write(`
          <div class="tag ${tag.cls}">
            <div class="name">${tag.name}</div>
            <div class="role">${tag.role}</div>
            <img src="<?=base_url()?>img/nametags/${tag.img}.png">
            <div class="talkTitle">
              <div>${tag.talkTitle}</div>
            </div>
          </div>
        `);
    });


    doc.write("</div>");
    win.stop();
  }

  function prezencka(){
    print_list(new Filterer([],null).add(new Filter({name:"speaker"})).filter(data),"Přednášející",true);
    print_list(new Filterer([],null).add(new Filter({name:"organizer"})).filter(data),"Organizátoři",true);
    print_list(new Filterer([],null).add(new Filter({name:"speaker", operation:"!"})).add(new Filter({name:"organizer", operation:"!"})).filter(data),"Účastníci",true);
  }

  function render(data){
    currentData = data;
    var tbody = document.getElementById('tbody');


    while (tbody.firstChild) {
      tbody.removeChild(tbody.firstChild);
    }
    Array.prototype.forEach.call(data, row => {
      var tr = document.createElement('tr');
      tr.onclick= () => {open_user_detail(row.id)};
      cells = [];
      var keys = ["id","firstname","lastname"];
      keys.forEach(key => {
        var td = document.createElement('td');
        td.title = key;
        td.innerHTML = row[key];
        td.className = "age"+row.age;
        tr.appendChild(td);
      });

      var td0 = document.createElement('td');
      td0.title = "age";
      td0.innerHTML = (row["age"]==3)?"x > 18":(row["age"] == 2)?"15 < x < 18":"x < 15";
      td0.className = "age"+row.age;
      tr.appendChild(td0);

      keys = ["speaker","organizer"];
      keys.forEach(key => {
        var td = document.createElement('td');
        td.title = key;
        td.innerHTML = (row[key]==1)?"Y":"N";
        td.className = (row[key]==1)?"yes":"no";
        cells.push(td);
      });

      
      var td = document.createElement('td');
      td.title = "orders";
      td.innerHTML = row.orders;
      //cells.push(td);



      //zaplaceno
      var td2 = document.createElement('td');
      td2.title = "paid";
      td2.innerHTML = row["paid"] + " Kč";
      cells.push(td2);

      var td3 = document.createElement('td');
      td3.title = "pay";
      td3.innerHTML = row["pay"]+" Kč";
      td3.className = "no";
      if(row["pay"] == 0) td3.className = "yes";
      cells.push(td3);

      var td4 = document.createElement('td');
      td4.title = "attended";
      td4.innerHTML = row.attended? "Y":"N";
      td4.className = row.attended? "yes":"no";
      cells.push(td4);

      keys = ["consent"];
      keys.forEach(key => {
        var td = document.createElement('td');
        td.title = key;
        td.innerHTML = (row[key]==1)?"Y":"N";
        td.className = (row[key]==1)?"yes":"no";
        cells.push(td);
      });

      var td5 = document.createElement('td');
      td5.title = "sleva";
      td5.innerHTML = row.disc+" Kč";
      td5.className = row.disc? "disc":"";
      cells.push(td5);

      cells.forEach(cell => tr.appendChild(cell));

      document.getElementById('tbody').appendChild(tr);
    });
    document.getElementById("userCount").innerHTML = `Celkem uživatelů: ${data.length}`;
  }

  function load(){
    $.ajax({url: '<?=base_url()?>API/users/get?&columns=["id","firstname","lastname","email","img","contact","speaker","organizer","orders","paid","attended","created","age","jizdne","vs","released","consent","comment","disc"]',
    success: function(result){
      log_message(result.message, "success");
      var rows = [];

      data = result.data;

      Array.prototype.forEach.call(data, row => {

        var keys = ["id","organizer","speaker","attended","released","consent","jizdne","paid","disc"];
        keys.forEach(key => {
          row[key] = parseInt(row[key]);
        });
      })


      var s = 0; var m = 0; var l = 0; 
      Array.prototype.forEach.call(data, row => {
          if(JSON.parse(row.orders)){
            s += JSON.parse(row.orders).S;
            m += JSON.parse(row.orders).M;
            l += JSON.parse(row.orders).L;
          }
      });
      


      Array.prototype.forEach.call(data, row => {
        var pay = 70;
        if(row.organizer == "1" || row.speaker == "1"){
          pay -= 70;
        }
        pay -= parseInt(row.paid);

        pay -= row.disc;

        var all = 0;
    var ord = JSON.parse(row.orders)?JSON.parse(row.orders):{S:0, M:0, L:0};
    ["S", "M", "L"].forEach((i,key) => {
      all += (ord)[i];});

        //pay+=(all)*150;

        row.pay = pay;

      });

      

      document.getElementById("tshirts").innerHTML = `Celkem objednáno triček:<br>S: ${s}<br>M: ${m}<br>L: ${l}`;
      

      render(data);
    },
    error: function(xhr,status,error){
      console.log(xhr);
      log_message(status + " "+ xhr.status +" " + ": " + error + ". See more info in console.", "error");
    }
    });

  }

  function reproces_user(row){
    var pay = 70;

    if(row.organizer == "1" || row.speaker == "1"){
      pay -= 70;
    }

    pay -= parseInt(row.paid);

    pay -= row.disc;

    var all = 0;
    var ord = JSON.parse(row.orders)?JSON.parse(row.orders):{S:0, M:0, L:0};
    ["S", "M", "L"].forEach((i,key) => {
      all += (ord)[i];});

    //pay+=(all)*150;

    row.pay = pay;
  }

  function compare(a,b,key,ord = 1){
    if(Array.isArray(a[key])){
      var asum = 0; var bsum = 0;
      Array.prototype.forEach.call(a,(val) => asum+=parseInt(val))
      Array.prototype.forEach.call(b,(val) => asum+=parseInt(val))
      if(asum > bsum) return 1*ord;
      if(asum < bsum) return -1*ord;
      return 0;
    }
    if(a[key] > b[key]) return 1*ord;
    if(a[key] < b[key]) return -1*ord;
    return 0;
  }

  var sortKey = "id";
  var sortDir = true;

  function sort(col){
    if(sortKey == col) sortDir = !sortDir;
    else sortKey = col;


    if(sortDir)currentData.sort((a,b) => compare(a,b,col));
    else currentData.sort((a,b) => compare(b,a,col));

    render(currentData);
  }

  function sort_data(data, col){
    if(sortDir)data.sort((a,b) => compare(a,b,col));
    else data.sort((a,b) => compare(b,a,col));
    return data;
  }

  function search(){
    var s = document.getElementById('serchBar').value.toLowerCase();
    var dataNew = [];
    filterer.filter(data).forEach(row => {
      name = row.firstname + row.lastname;
      if(name.toLowerCase().includes(s)) dataNew.push(row);
    });
    render(dataNew);

  }

  function open_user_detail(id){
    document.getElementById('cover').classList.remove('inv');
    var user_detail = document.getElementById('userDetail');
    user_detail.classList.remove('inv');

    data.forEach(row => {if(row.id == id) user = row});

    $("#userDetailName").html(user.firstname + " " + user.lastname);
    var speaker = document.getElementById("userDetailSpeakerIcon");
    var org = document.getElementById("userDetailOrgIcon");
    var attended = document.getElementById("userDetailAttended");
    var orders = document.getElementById('ordersTable');
    var pay = document.getElementById('userDetailPay');
    document.getElementById('userDetailPayInputWrapper').classList.remove("tableDisplay");
    document.getElementById('userDetailPayInput').value="";

    consent = document.getElementById('userDetailConsentWrapper');

    speaker.classList.remove("off");
    speaker.classList.remove("yes");
    org.classList.remove("off");
    org.classList.remove("yes");
    attended.classList.remove("no");
    attended.classList.remove("yes");
    orders.classList.remove("no");
    orders.classList.remove("yes");
    pay.classList.remove("no");
    pay.classList.remove("yes");
    pay.classList.remove("over");
    consent.classList.remove('plus18');
    consent.classList.remove('consF');
    consent.classList.remove('consT');

    speaker.classList.add((user.speaker == 1)?"yes":"off");
    org.classList.add((user.organizer == 1)?"yes":"off");
    attended.classList.add((user.attended == 1)?"yes":"no");
    consent.classList.add((user.age == 3)?"plus18":(user.consent == 1)?"consT":"consF");

    pay.classList.add((user.pay > 0)?"no":(user.pay < 0)?"over":"yes");
    $("#pay").html(user.pay);

    var all = 0;
    var ord = JSON.parse(user.orders)?JSON.parse(user.orders):{S:0, M:0, L:0};
    ["S", "M", "L"].forEach((i,key) => {
      all += (ord)[i];
      orders.children[1].children[0].children[key].innerHTML = ord[i];
    })
    

    orders.classList.add((all > 0)?(user.released == 1)?"yes":"no":"yes");

    //speciální sleva
    disc = document.getElementById("userDetailDisc");
    disc.innerHTML = user.disc+" Kč"
    disc.className = parseInt(user.disc)?"disc":"off";
    disc.parentElement.className = "";
    document.getElementById("userDetailDiscInput").value = user.disc?user.disc:0;

    //poznámka
    comment = document.getElementById("userDetailComment");
    comment.parentElement.className = "";
    comment.contentEditable = false;
    comment.innerHTML = user.comment?user.comment:"Klepněte pro přidání poznámky...";
    comment.className = user.comment?"comment":"";

    console.log(user);
  }

  function close_users_detail(){
    document.getElementById('cover').classList.add('inv');
    document.getElementById('userDetail').classList.add('inv');
  }

  function toggle(col){
    var txt = "přidat";
    var set = 1;
    if(user[col] == 1){
      txt = "odebrat";
      set = 0;
    }
    //if(!confirm("Opravdu chcete "+txt+" roli "+col+" uživateli "+user.firstname+" "+user.lastname+"?"))return;
    $.ajax({
      url:"<?=base_url()?>API/users/set",
      type: "POST",
      data: {
        columns:'[["'+col+'",'+set+']]',
        where: '[["id","=",'+user.id+']]',
      },
      success: (response) => {
        log_message(response.status+": "+response.message+" ("+response.data+")", "success")
        console.log(response);
        user[col] = set;
        reproces_user(user);
        render(data);
        open_user_detail(user.id);
      },
      error: function(xhr,status,error){
        console.log(xhr);
        log_message(status + " "+ xhr.status +" " + ": " + error + ". See more info in console.", "error");
      }
    });
  }

  function toggle_release(){
    if(user.pay > 0)
    alert("Trička nemohou být vydána, dokud účastník nezaplatí.");
    else toggle("released");
  }

  function open_pay_form(){
    document.getElementById('userDetailPayInputWrapper').classList.add("tableDisplay");
  }

  function pay(){
    document.getElementById('userDetailPayInputWrapper').classList.remove("tableDisplay");
    amount = parseInt(document.getElementById('userDetailPayInput').value);
    amount = isNaN(amount)?0:amount;

    var paid = parseInt(user.paid) + amount;

    $.ajax({
      type: "POST",
      url: "<?=base_url()?>API/users/set",
      data:{
        columns:'[["paid",'+paid+']]',
        where: '[["id","=",'+user.id+']]',
      },
      success:(response) => {
        log_message(response.status+": "+response.message+" ("+response.data+")", "success")
        console.log(response);
        user.paid = paid;
        reproces_user(user);
        render(data);
        open_user_detail(user.id);
      },
      error:(xhr,status,error) => {
        console.log(xhr);
        log_message(status + " "+ xhr.status +" " + ": " + error + ". See more info in console.", "error");
      },
    })
  }

  function edit_comment(){
    comment = document.getElementById("userDetailComment");
    comment.contentEditable = true;
    comment.parentElement.className = "active";
  }

  function save_comment(){
    comment = document.getElementById("userDetailComment");
    comment.contentEditable = false;
    comment.parentElement.className = "";

    set_data("comment",comment.innerHTML);
  }

  function open_disc_form(){
    disc = document.getElementById("userDetailDiscWrapper");
    disc.className = "active";

  }

  function save_disc(){
    disc = document.getElementById("userDetailDiscWrapper");
    disc.className = "";

    discount = document.getElementById("userDetailDiscInput").value;
    set_data("disc",discount);


  }

  function set_data(col, val){
    $.ajax({
      type: "POST",
      url: "<?=base_url()?>API/users/set",
      data:{
        columns:'[["'+col+'",'+JSON.stringify(val)+']]',
        where: '[["id","=",'+user.id+']]',
      },
      success:(response) => {
        log_message(response.status+": "+response.message+" ("+response.data+")", "success")
        console.log(response);
        user[col] = val;
        reproces_user(user);
        open_user_detail(user.id);
        render(data);
      },
      error:(xhr,status,error) => {
        console.log(xhr);
        log_message(status + " "+ xhr.status +" " + ": " + error + ". See more info in console.", "error");
      },
    });
  }

  function open_newsletter(){
    document.getElementById('cover').classList.remove('inv');
  }


  load();
  <?php if(!$payments):?>
    log_message("Žádné nové platby nebyly připsány.","payment");
  <?php endif;?>
  <?php foreach($payments as $row):?>
    log_message("Připsána platba: <?=$row["name"]?> VS:<?=$row["vs"]?>, <?=$row["value"]?>Kč", "payment");
  <?php endforeach;?>
</script>
