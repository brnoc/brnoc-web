<main id="home">
  <h1>Administrace</h1>
  <p>
    Výtej v administraci BrNOCi!
  </p>
  <p>
    Pro interaktivní prezenční listinu přejdi do sekce <a href="attendance">prezenčka</a>.<br>
    Pro schvalování přednášek přejdi do sekce <a href="talks">přednášky</a>.<br>
    Pro administraci uživatelských účtů přejdi do sekce <a href="users">účty</a>.<br>
    Pro tvrobu rozvrhu přejdi do sekce <a href="haluzitko">haluzítko</a>.<br>
  </p>
  <p>
    Něco nefunguje? Napiš!<br>
    <a class="notA" href="https://m.me/jakubradl2"> <i class="fab fa-facebook-messenger"></i> Messenger</a><br>
    <a class="notA" href="mailto:me@jradl.cz"> <i class="fas fa-envelope"></i> Email</a><br>
    <a class="notA" href="https://forms.monday.com/forms/20046507fb4fa8a1664491602fa57c52"> <i class="fas fa-comment"></i> Feedback</a><br>
  </p>
  <p>
    Zpět na <a href="<?=base_url()?>user">BrNOC</a>.
  </p>
</main>
