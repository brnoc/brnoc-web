<!DOCTYPE html>
<html>
<head>
  <!--page title-->
  <title>Administrace|<?=isset($title)?$title:"BrNOC"?></title>

  <!--meta-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#000000">

  <!--stylesheets-->
  <link rel="stylesheet" href="<?=base_url()?>css/admin/main.css"/>
  <link href="<?=base_url()?>fontawesome/css/all.min.css" rel="stylesheet">
  <?php if(isset($_SESSION['user'])):?><link rel="stylesheet" href="<?=base_url()?>css/logged.css"><?php endif;?>
  <?php if(isset($stylesheets)): foreach($stylesheets as $value):?>
  <link rel="stylesheet" href="<?=base_url()?>css/admin/<?=$value?>.css">
  <?php endforeach; endif;?>


  <!--scripts-->
  <script src="<?=base_url()?>js/w3.js"></script>
  <script src="<?=base_url()?>js/info_dev.js"></script>
  <?php if(isset($scripts)): foreach($scripts as $value):?>
  <script src="<?=base_url()?>js/<?=$value?>.js"></script>
  <?php endforeach; endif; ?>
</head>
<body class="menuClosed">
  <header>
    <a href="<?=base_url()?>admin/attendance" class="menuItem <?=isset($page)?(($page=="attendance")?"active":""):""?>"><div>Prezenčka</div></a>
    <a href="<?=base_url()?>admin/talks" class="menuItem <?=isset($page)?(($page=="talks")?"active":""):""?>"><div>Přednášky</div></a>
    <a href="<?=base_url()?>admin/users" class="menuItem <?=isset($page)?(($page=="users")?"active":""):""?>"><div>Účty</div></a>
    <a href="<?=base_url()?>admin/haluzitko" class="menuItem <?=isset($page)?(($page=="haluzitko")?"active":""):""?>"><div>Haluzítko</div></a>
    <a href="<?=base_url()?>admin/mystery" class="menuItem <?=isset($page)?(($page=="mystery")?"active":""):""?>"><div>Mystery</div></a>
    <a href=" " class="back"> </a>
    <a href="<?=base_url()?>user/admin" class="back"><div >✕</div></a>
    <a onclick="document.body.classList.toggle('menuClosed')" class="menuOpenButton"><i class="fas fa-bars"></i></a>
  </header>
