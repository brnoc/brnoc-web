<?php defined('BASEPATH') OR exit('No direct script access allowed');
$desc = "Podzimní BrNOC je již šestou Brněnskou přednáškovou nocí. Podle tradice jiných *rNOCí, i v Brně se jedná o akci, kde studenti přednáší studentům o&nbsp;svých odborných pracích, zájmech anebo prostě o tématech, která jim připadají důležitá."
?>
<!doctype html>
  <html>
  <head>
    <!-- Standardní meta-->
    <meta charset="UTF-8">
    <meta name="author" content="Pravoúhlý sněm">
    <meta nema="keywords" content="brnoc brněnská přednášková noc přednášky jaroška gymnázium brno třída kapitána jaroše"/>
    <meta name=”description” content="<?=$desc?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#000000">

    <!--Open Graph-->
    <meta property="og:title" content="BrNOC – brněnská přednášková noc">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.brnoc.cz/">
    <meta property="og:image" content="https://www.brnoc.cz/img/header.jpg">
    <meta property="og:description" content="<?=$desc?>">
    <meta property="og:site_name" content="BrNOC">
    <meta property="og:locale" content="cs_CZ">

    <!--Schema.org-->
    <meta itemprop="name" content="BrNOC">
    <meta itemprop="description" content="<?=$desc?>">

    <!--page title-->
    <title><?=isset($title)?$title."|":""?>BrNOC</title>

    <!--icon -->
    <link rel="icon" type="image/ico" href="<?=base_url()?>img/favicon.ico">

    <!--stylesheets-->
    <link rel="stylesheet" href="<?=base_url()?>css/main.css"/>
    <link href="<?=base_url()?>fontawesome/css/all.min.css" rel="stylesheet">
    <?php if(isset($_SESSION['user'])):?><link rel="stylesheet" href="<?=base_url()?>css/logged.css"><?php endif;?>
    <?php if(isset($stylesheets)): foreach($stylesheets as $value):?>
    <link rel="stylesheet" href="<?=base_url()?>css/<?=$value?>.css">
    <?php endforeach; endif;?>


    <!--scripts-->
    <script src="<?=base_url()?>js/w3.js"></script>
    <script src="<?=base_url()?>js/info_dev.js"></script>
    <?php if(isset($scripts)): foreach($scripts as $value):?>
    <script src="<?=base_url()?>js/<?=$value?>.js"></script>
    <?php endforeach; endif; ?>

  </head>
  <body id="body"><?php if(1):?>
    <?php if(isset($_SESSION['user'])) {
        $loggedURL="user";
        $loggedLogo="fa-user-circle";
        $loggedTitle="Uživatelská sekce";
        $logout = "<a href=\"".base_url()."sign/out\" class=\"mi\" data-balloon=\"Odhlášení\" data-balloon-pos=\"down\" data-balloon-length=\"medium\"><span class=\"headerIcon fas fa-sign-out-alt\"></span></a>";
        $loggedClass = "logged";
      } else {
        $loggedURL="sign/in";
        $loggedLogo="fa-sign-in-alt";
        $loggedTitle="Přihlášení";
        $logout="";
        $loggedClass = "";
      }?>
    <header class="<?=$loggedClass?>">
      <a href="<?=base_url()?>home" class="mi" data-balloon="Domů" data-balloon-pos="down" data-balloon-length="medium"><span class="headerIcon fas fa-eye"></span></a>
      <a href="<?=base_url()?>program" class="mi" data-balloon="Harmonogram" data-balloon-pos="down" data-balloon-length="medium"><span class="headerIcon fas fa-clipboard-list"></span></a>
      <a href="<?=base_url().$loggedURL?>" class="mi" data-balloon="<?=$loggedTitle?>" data-balloon-pos="down" data-balloon-length="medium"><span class="headerIcon fas <?=$loggedLogo?>"></span></a>
      <?=$logout?>
    </header>
  <?php endif;?>
