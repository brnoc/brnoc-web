<main>
  <h1> Přihlášení </h1>
  <?= validation_errors("<div class=\"msg error\">","</div>")?>
  <?= isset($message)?"<div class=\"msg $message[0]\">$message[1]</div>":""?>
  <form action="<?=base_url()?>sign/in" method="post" accept-charset="utf-8" class="center">
    <?=inputTemplate("<input name=\"name\" type=\"text\" value=\"" . set_value('name') . "\" placeholder=\"např. hugo.kokoska\" autocomplete=\"username\" required>", "Přihlašovací jméno")?><br>
    <?=inputTemplate("<input name=\"password\" type=\"password\" value=\"" . set_value('password') . "\" placeholder=\"hlavně nikomu neříkat!\" autocomplete=\"password\" required>", "Heslo")?><br>
    <input name="submit" type="submit" value="Přihlásit" />
  </form>
  Ještě nejste registrovaní? Registrujte se <a href="<?=base_url()?>sign/up">zde</a>.<br>
  Zapomenuté heslo? Obnovte si ho <a href="<?=base_url()?>user/forgotten">zde</a>.
</main>
