<main>
  <h1>Zrušení uživatelského účtu</h1>
  <form action="<?=base_url()?>user/deleteAcc/<?=$id?>/true" method="post">
    <section>

      Uživatelský účet: <br>
      id: <?=$user->id?><br>
      name: <?=$user->name?><br>
      <br>Všechny přednášky registrované pod tímto uživatelským účtem budou zrušeny.
      <div class="controls">
        <div class="btn yes" onclick="document.getElementsByTagName('form')[0].submit()">Smazat</div>
        <div class="btn no"onclick="window.history.go(-1);">Zpět</div><br><br>
        Tato akce nelze navrátit.
      </div>
    </section>
  </form>
  <script>
    function checkAll(elem){
      elem.classList.toggle('checked');
      check = elem.classList.contains('checked');
      var tbody = elem.parentElement.parentElement.parentElement.parentElement.children[1];
      Array.prototype.forEach.call(tbody.children, tr => {
        tr.children[0].children[0].checked = check;
      });
    }
  </script>
</main>
