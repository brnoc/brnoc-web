<link rel="stylesheet" href="<?=base_url()?>css/mystery.css"/>
<main>
  <script>
  var topics =
  <?php
    $print = [];
    foreach($topics as $topic)$print[] = $topic->topic;
    echo json_encode($print);
  ?>;
  var tasks = <?php
  echo json_encode([
    "Vyvolej bouřlivou diskuzi na téma:",
    "Znázorni pantomimou téma, diváci ho musí uhodnout:",
    "Za použití pouze citoslovců popiš téma, diváci ho musí uhodnout:",
    "Pomocí zvuků znázorni téma, diváci ho musí uhodnout:",
    "Měj krátkou přednášku ve verších na téma:",
    "Měj krátkou zpívanou přednášku na téma:",
    "Nakresli na tabuli téma, diváci ho musí uhodnout:",
  ])
  ?>

  function set_topic(){
    topic =  topics.splice(parseInt(Math.random()*topics.length),1)[0];
    task = tasks[parseInt(Math.random()*tasks.length)];
    document.getElementById("task").innerHTML = (typeof topic != 'undefined' && topic != null && topic)?task+" ":"";
    document.getElementById("topic").innerHTML = (typeof topic != 'undefined' && topic != null && topic)?topic:"Témata došla!";
    return topic;
  }
  </script>
  <h1>Mystery room</h1>
  <section id="taskSection">
    <h2>Tvůj úkol:</h2>
    <div class="taskWrapper">
      <span id="task"></span><span id="topic"></span>
    </div>
    <div onclick="set_topic()" class="newTopicButton">Nový úkol</div>
  </section>
</main>
