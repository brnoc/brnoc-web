<main>
  <h1>Obnovení hesla k uživatelskému účtu</h1>
Zadejte nové heslo k vašemu uživatelskému účtu.
<?=validation_errors("<div class=\"msg error\">","</div>")?>
<form method="post" action="<?=base_url()?>user/recover">
  <input type="password" name="pass" placeholder="heslo"/><br>
  <input type="password" name="pass_again" placeholder="heslo znovu"/><br>
  <input type="hidden" name="id" value="<?=$id?>"/>
  <input type="hidden" name="key" value="<?=$string?>"/>
  <input type="submit" name="submit" value="Změnit"/>
</form>
</main>
