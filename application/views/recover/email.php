<main>
<h1>Obnovení hesla k uživatelskému účtu</h1>
Pro obnovení hesla k vašemu uživatelskému účtu zadejte email. Pokud je s touto adresou registrován uživatelský účet, bude vám zaslán email s odkazem pro obnovení hesla.
<form method="post" action="<?=base_url()?>user/forgotten/true">
  <input type="email" name="email" placeholder="email@mail.com"/><br>
  <input type="submit" name="submit" value="Odeslat"/>
</form>
</main>
