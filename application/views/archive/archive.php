<style>
  section{
    background-color:#333;
    border-radius: 15px;
  }

  h1{
    width:100%;
  }

</style>

<main>
  <h1>Archiv</h1>
  <section onclick="window.location = 'archive/IV'" style="cursor:pointer">
    <h2>BrIVOC (14. - 15. 12. 2018)</h2>
    <img src="<?=base_url()?>img/archive/IV/header.jpg" width="100%"/>
  </section>

  <section onclick="window.location = 'archive/V'" style="cursor:pointer">
    <h2>BrNOC 5 (22. - 23. 3. 2019)</h2>
    <img src="<?=base_url()?>img/archive/V/header.jpg" width="100%"/>
  </section>

  <section onclick="window.location = 'archive/VI'" style="cursor:pointer">
    <h2>BrNOC 6 (13. - 14. 3. 2019)</h2>
    <img src="<?=base_url()?>img/archive/VI/header.png" width="100%"/>
  </section>
</main>
