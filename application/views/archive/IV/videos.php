<main>
  <h1>Nahrané přednášky &mdash; BrIVOC</h1>
  <style>
  .center{
    text-align: center;
  }
  iframe{
    border:1px solid #333;
  }

  </style>

  <div class="center">
  <hr>

  <iframe  width="560" height="315" src="https://www.youtube.com/embed/4D_r7Qqr62k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  <h2>Daniel Perout: Soudce Dáňa<h2>
  <hr>

  <iframe  width="560" height="315" src="https://www.youtube.com/embed/AvmMj31gh64" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  <h2>Matěj Lieskovský: Emergentní makrofyzika aneb Kapaliny jsou divné<h2>
  <hr>

  <iframe  width="560" height="315" src="https://www.youtube.com/embed/OMfljJTQEgI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  <h2>Jan Pokorný: Koncepce antikoncepce<h2>
  <hr>

  <iframe  width="560" height="315" src="https://www.youtube.com/embed/O9I1DeIk1sk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  <h2>Martin Dvořák: Umělá inteligence pro hry<h2>
  <hr>

  <iframe  width="560" height="315" src="https://www.youtube.com/embed/DGHWAuE-O9s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  <h2>Jiří Štrincl: Indian challenge<h2>
  <hr>
  </div>
</main>
