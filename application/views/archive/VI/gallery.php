<main>
  <h1>Fotogalerie &mdash; BrNOC 6</h1>
  <script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/dist/pa-embed-player.min.js" async></script>
  <script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/embed-ui.min.js" async></script>
<div class="pa-gallery-player-widget" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/i9xabY8FZ4WX4z2z8"
  data-title="BrNOC #6"
  data-description="42 new photos added to shared album">
  <object data="https://lh3.googleusercontent.com/qHKJ5COvQTny09E_VXMq4GWFWd3GDOeb0q0qs0IKKdO3g3RQE1oP4tIi17bSJZYRxafsYtog-yWN7yyNAk8ZLslxt7HmwZxXhTzc9zkVnFPPDbhLWUbOkAwIdV7g0mglxQJQ_7xL=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/WD2C9DSxUtVED_zAYnXeZkaC1G2KCTdPgeQCymI24bL7-9Iim-LXRrAemXBh9kCzJP7Y2rX7RH2DcJ1kUnRXT172PRcKpaRV-XBogEYf4XHQZM-_RFE7vlK-H6cW69abpQUZsE8F=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/AoEU7dOKyuYsc0W70zYMWwM9AYXFf1v48NWoNJcz0IMTpx78VMp6Q6gCS5VVCbf5e3WeuzrL1fV6sF8lasuFham8cNvIvYStF9AYQ8wJvkhqss_8OTn8BkQQGtfK5KBZD_XyUFLf=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/RSmSJSVu6HwUFCX4m-aHXViowf_c4CX5doENH_QNlleh90-FyLqz_nN9Y1IKqZSgTCgmOs82Rs8lpcpct4CKXDqFskV5tcOBO1v9hon-uZCHusK0f9elA1e-QnFduk25ubCLN-9X=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/YUJU1XziT8cuC6xapm0svCmI_nQbmvQaSYXoLprRmJy7TIsQAnOHvFmOa9ZrtmSZbev_J6MkHFDhzYv-8xAKcLsqZ-XHgo9x_tCulnsMBnm_D3CDU3qQOJi6lvNXDYlGR4YjtPCU=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/cyDPgej-83slLyvNRmHnsC4CMCv0lOmzhY98phX6hPtGNiPEy_twiyaTjB0d98khhN9ijCgTuJZflwywJ8FjnHtY1dsAwDeV1HV-YJhsjsKyL1dmjfIUsz-5wfemKadBNStawt4K=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/0aQcLZHlCxoLUafyCp7_BqtsZERk3RsvvioaatAH3xbyEJHv7S-PX8tCdzF9I15cupTM7vxvaerlfj_RMLgEtm1PIZ7tlzyKehTQaJPkBq6kv-L1SaxFr6Ct54aytTYWIzYRgk0h=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/92dX9DnWYEf9AK9Ho8y-AyJ1t8buaRp5IhN-llna5FScV1YoKsfcdtCp8KSEylY3-eERp6o_4IkogWFTJ1CFhVeu8Lh43moXA-o7A0D30LaN2B6vXLm9S1YlmHt_5rf_efWQk-Uf=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/tmi7XKQYc1iWi5S5KfusAmthx-Z7iIK1Igc5PVF4YqAnjnyBPfT4LMILWVk17CEjNoo0H2OKOWX5-SVznw5xjxoIZhKy5aSZ1qnl4plZ4NOjKFb2rGhD5gZ8PdfNnN1iv0OMCPcL=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/SqojJ2A6IdGKv1O3U22ilD5qPue1L_jujQPmphOUqBrOc9n6h7VV3W16bqMfqGEVt56LoDHudK2g9U0rfX6lGTNdECaVDaK5e1ypvYQ8bPHvxhdOuyuxdUrpybd1p9tqY3NDrh-k=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/a2i9Aw-11DC_u0TQgT0su5aD2Dbqb27g_J3dk4c0Tvlc5dUxw8QxyCl0zV3ES9Sb4FeULfdFbdR16NQPIAj_HxkIa6DVpSNn-mI41UOJU52IdqTZ0XZ13hERGtlvB2h02KqmpjiD=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/cULUCbr2NCRaM8JGQKDlk7SX-_Xs2xQHXZ1B0IWAy0ozXOqxNuVjufzHK1Z2ZoKh7cPralaLVoQxBggpjjHOKxpNuqaV8kbp0-P2dlMW_xKxGPftXe3BgtRuPWpM2a5aZ9gmLkZK=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/9TkiyqIMLoY-sJB90bS0aqLWeq3TWl7MkdhRH9WR8bnbDVfie87eo43OBbS6konAVGx04KcHAtDM8hoQObXpuJc72iw8ORUMCJ2TjKLtaeRua7OdTRIu0984ZIMEA4A2YZTXSbBr=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/vBjQOBH3vY8uiHzv7sJX7pX6OtyPxgvuJIZw1sZZFIvI0DP3uy5qTGvIl-idgjiaNiB2Dz6nKqD3Nzbr3PmEQEmucWeFua1sLaqxymtRdMmp1tulXnE4d7uri3WhqBQGiO_nLDs2=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/0g567cwrjS2n9eGMm0QlnZESnYTH4R8_QxjfithpcQaQUESbPylpmxbNT17YPTY7Y-mBjExZhB5Bzc_V4FFJQEeUBk-XnAxSmCFz3is74UrM47hwmfuYPPPwjxan_zqtndWLiafO=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/tnGj37eSkCubbMLJ_GAckFocZ96feG00FfU9P93uHN5jWs17gkbm4H0G8tbvbZoUZ1oKru8wABQ9fTmakkYwMzoaePkEv_mkX4HPChNKNN0g7Z1U4RbU7AqCNkO5eaCPtgOqUP20=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/sjBjeeMo_0RWdxijUEcFfpyjhtiZVGKBeob4vkXOFzBS8ERiQHyE2CRfOWTX2sl596pv6DCcBQqhapySQ3rP2QWBmRXrXBDx2eBlWd8KlFzRamYZ-7SZkuI-01sGIw_FybVTauH_=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/tZ0CT467p7H3J-m6WZrKzidIoG9lmWIfpgMcW2y2fyPByjUPxyIsQO3JNG32SQVSmAyLIArI85fhz7rqAobrw2SbELMi082uy5e2wWicdq5iajQgn0SMNVg38JDgVay5OYyNboeW=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/-vN3eY4qMTaMnKNipuF4yZO7FvegSvQw5XHJWltKXcfYbJHPr9s6C5TiIL8d1lLrNc6_1jTpNpwRU4ewIWcJc2UA0Vy256h_5qWDE5gUjLrUTrs16OC8I62iqb_TGO8PJXmFKSn-=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/twK0NcK5o7pAa9iwPIn0MPmsps60ZGKyH0g1zMFUbAM3PObHhgvBMP183kMmAetLRqyddqtoMWx1da0fczI2spmsUKAmK5kVLY0KDrnEBVDOeZu5S5tCZXkQGnDMbk4BZyxx1lE1=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/EKB6ZRLhAqcVlbuaYXHOxZqiKkK3-mGPQ8G40fxOEeQfhxA9f6hATvr4wQz10zoLKR3H-qSORKmhS5nDon3YvzAIKXChHjI182Q0_B-EC74HtRhPv-h5_iKUFQ_x4JRr_ywXHghW=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/US2HW1d_vR8ndjs8lwS2vK6BIKwAbv2k3io3ou2XG6tQjQu_J1Ir3_YylOOVIpCb6hEucjJeXjzDQwjYi0oueei7F4O-G9ipr5_nWdfsLik90xOw3B_6JmA597p5T1R0O-bjEnMW=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/wUdce97nFq_n_OQzJRyVjTQ1tMxXN_FS_OI1sLoYn33bCXbF8M1KfDKoP2m_nTN7aX8oW1jZFFxWYsxITIQpAZtIDO-nwy7ZQv8dOxisC0oQcShcHKerpnv1RODJVENohezYuRfu=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/BwbHaQhlprC-H-v_8q2t3_5La97H_2dJCBD0kiRcc4FSb7-EggG1qRtAP86g61UHtUqhQk2drdVCkqdVPZcAAcTrenoQYmsaPtDcZZr39EjV5Z_uKEcDm7Fxh_zpwCpjMUBKdJkE=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/r3PpdcAEaWYZPF4wCLxdoV5TSWAfRTo4xGPPwKZ3NgyJW_SyqfXqgzHn7euU60dRSEIca_FLSZzhfk__75WebVQ4-mi4OQeIwaia0s6LZRBTUZ_iJJ0F1exh_3z0kNaRDkho6uG6=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/aefAuWdYMhXKIacX4IdVNdy6lz5y1anYfcBjAcV1-8TKMskhoQdFdjMyq3vqNxWvAdnDp_O8H9YVRrWTS-i-ehqclfZabKTdcjjOG21ldGcRNMaaMb0YGl2s-pVxP-IOSUKBhsF4=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/lO84jKmXPlk885Og962wicEQmYv-xE6YdUO2qqBIjx6GGP9qFoX0gxvZ4gfj9gkFvItlcBRRlsGNQ5BOra5YTyJXhs71ZmmouK7NvAKHORWH8abt2qKkIMjVdE2rhgO0NWvxjq8F=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/IOFW_mi0SnKYPgQ_Tcd2ZoW_Qm-IQIlgwYf1yv-WFSNmXVMPbKUsGXiomXvWTKLfJeIliDDTWDAGyFNe9q94c6ey1ttUm_Eqmt_sYmZJ2rjWdYJ2ciXUm8KLDm_AH9l16JFpQTRJ=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/sdOtCAh9sGcyQirsI_H4FYxvsbGwoWcE5MOXfRqTVFO7G7udFpoXNmxoNg9uXgDx-0MFq_BNQwY_vnwOr6f4RpvuNHt-s7zR3jUmJyuUZzK8u8TqdMgTD5pT129yNUkFNbk0ZnNG=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/Hg7vacsPTKWtSZyfcwakr7l0xZAnEJlnZB-S6I_3-28yOypukeq4kW0GCO3BXpsethJkevnUjqmremUqYHHImdXD_EbFb8VXzMZ2FceaBV8fyUvcS2WANtMpKTTXbsKTR5xD5-V7=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/1bpYw-Mxa7uKpL1d-Uqq2tE0YscD_XQm5tRhBbmywn-Ixc0M5WooQ_K-ipsdnXVinVDWQCUU5AZM9jCsWAD_a_Bm1gJfDexoGptsCGmj_INeyI6_QDmncaAVgAd-9cCjuyMG2jvN=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/_eZCJlpeRJP9oDqHE6RrU8QbYkpXMCUQIUX4Tnkfwi0T26TjkqMWWSH0sTb0bZiLbA7MWfGhcXpBwi7tDIBpjSvhquPtX5R1S0kPMgXE7eK_L0qVLREG7DGfqn2Enujz3Ac--JGN=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/xNRQ1V-FsC9-9prGRw9RLB6KjZL48jZ8LfcWWseuoN8XGTDoilJTsv3Q6aW2AeTTWs8lfWAEXfPEQgLZk2qZNH_nRLkzkQU1bcXzQJJrV7Xfafwmd3iEE6b6O4AuPlcHVM-M4pVw=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/8psLZDnwb8Zwdlj-coe3o8kJl6nBZM3wqhv07oXuY13cUiyXOnZyWn4YV6qKmpa1F-wI0jBIwvk25YvaJsMJooSwzUIblTwBp_63ErLgHVCvhGiFhmt4vLYxuYZdFOfwOkNB5oa_=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/lYoert32-UQLp4lssfGYwv2R03kJXJWiePA2xG3f2aP2kEekT7od_y1cQirXBNvkrz4IVZ9YdcQXiGGq-5JDdVxG_T0SIEHX0eQ2X7gv5sa3j9PmO6-SBi_NlWZjojMcB0RPIfRH=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/rJ_u4dVokCtGvjmvJnoCV9rTpLjuRoOicmqDVLZiCD9jAnTuJ3oghiOA8kG53Je4J1HuTU6ToUHBFsZV3Roh3VlUFBw7hXE-Kr_pABrLSrGN2G7bxtWQPTfonzsWDGhFI6XbUb_-=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/WYsOGOmK-tw6v4KAWtw_qAqM6A1xmNyo7QrqsYmQPgND9Eey4zkuv3Nc1jAk_TKldmaNhBoICG1QgG8WXC-h-O6u3mJoAqSXx7rLqB12wNKjqRyXRRsN5GlzkwBkgHd3GIS9ntIg=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/PD4j3bhHQsy6gCnIVw5ZBmgA19VkWMbmtboDhIbWzozZTgP5wRTOdNmntIi5LeJpjCWw9uJ0yzXAXPrRN57RXZY3hmn2OC3fR09jd1DMKbuic2o5fpsMQzwpU31PhCWJrkT7kBU3=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/QpmdrAVFdgy5qJvkPy88lc8zbFl6sNWvzhjmNeIJwtKmggZmUh6aAVOuzWZ2mVSgLQH8ZxrKpYhYzUSx6bOe9iJjT6nTB4DPf6ydnrzr4FVUoJhU7vURagzcd_OvZXUT84Sv15ZE=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/Nr6x15n-22tHF03Tjpu63Jm14PQcYIw3vANjZgfzHm1aTAyTuqcMfVBOg_qetbnPeGASbNpPQ15t_0Is9v-PYzDHwAeoKzfrrUuLuQywRjrf4A4AKvSK6N6QJiJr2ROr5AtNf2Pu=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/ZZ_PW2VxgNakJSXOH9AbzXKqGXlIZ8zQWcvcaplvuxQ1PRNjX6AvbenEMdJtBSk4xZlQiGI_Vu3WZ1TJ7Cf_t7-5WO6Ct2Xcn120ySlLLTmy_Jl1dTxeD2VFFZC4YJ1PpZ1UT9Ym=w1920-h1080"></object>
  <object data="https://lh3.googleusercontent.com/N3oqGcwv5yTt_55WyG7Z06QrO1v16ukbie7ZQwZVsvKJfDehT8jXCC8VHnsDz-GBUUo3HDaaRynoVJlpBeJK-E7urMe6n2vlM8bW32xn6aeYgzu3PqrgdtibA42ZZiUapcA1WByD=w1920-h1080"></object>
</div>



</main>
