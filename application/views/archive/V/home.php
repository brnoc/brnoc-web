<style>
.grid{
  display:grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 1em;
  grid-template-rows: auto;
}

.item{
  text-align: center;
  position:relative;
  border:5px solid white;
  border-radius: 15px;
  cursor:pointer;
}

.item:hover{
  background-color: #FFF2;
}

.item img{
  width:50%;
  margin:2em auto;
}

.item .title{
  width:100%;
  padding:10% 0;
  position:absolute;
  bottom:0px;
  background-color: #0009;
}


</style>

<main>
  <h1>BrNOC 5 (22.&mdash;23. 3. 2019)</h1>
  <img src="<?=base_url()?>img/archive/V/header.jpg" width="100%">
  <div class="grid">
    <div class="item" onclick="window.location = '<?=base_url()?>archive/V/gallery'">
      <img src="<?=base_url()?>img/archive/image.svg">
      <div class="title">Galerie</div>
    </div>
    <div class="item" onclick="window.location = '<?=base_url()?>archive/V/talks'">
      <img src="<?=base_url()?>img/archive/talks.svg">
      <div class="title">Přednášky</div>
    </div>
    <div class="item" onclick="window.location = '<?=base_url()?>archive/V/videos'">
      <img src="<?=base_url()?>img/archive/video.svg">
      <div class="title">Videa</div>
    </div>
    <!--<div class="item" onclick="window.location = '<?=base_url()?>archive/IV/stats'">
      <img src="<?=base_url()?>img/archive/stats.svg">
      <div class="title">Statistiky</div>
    </div>-->
  </div>
</main>
