  </main>
  <footer>
    <div id="credits">
      <span>&copy; Pravoúhlý sněm 2018&mdash;2019</span><br>
      <span>Kontakt: <a href="mailto:pus@brnoc.cz">pus@brnoc.cz</a></span><br>
      <span>Pokud máš nějaké připomínky k webu &mdash; <a href="https://forms.monday.com/forms/20046507fb4fa8a1664491602fa57c52">dej nám vědět</a>!
    </div>
    <?php if(isset($_SESSION['user'])) {
      echo "<div id='footerUser'>Přihlášen jako <strong>".$_SESSION['user']['name']."</strong></div>";
    }
    ?>
    <div class="footerImage fI-2">
      <img src="<?=base_url()?>img/open.png"/>
    </div>
    <div class="footerImage fI-3">
      <a href="http://www.jcmm.cz"><img src="<?=base_url()?>img/logo_JCMM.png"/></a>
    </div>
    <div class="footerImage fI-1">
      <?php /*<svg>
        <image xlink:href="<?=base_url()?>img/logo_jaroska.svg" src="<?=base_url()?>img/logo_jaroska.png"/>
      </svg>
      */?>
      <a href="https://jaroska.cz"><img src="<?=base_url()?>img/logo_jaroska.svg" class="footerImageSvg"/></a>
    </div>
  </footer>
</body>
</html>
