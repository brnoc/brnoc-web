<?php
function dlazdice($BrDEN, $pusold, $tileName, $tileFunc, $tileImg, $tileEmail) {
  echo "<div class='tileContainer'><div class='tileCard'><div class='tileFront'>";
  if($tileImg!="none") {
    echo("<img alt='$tileName' src='" . base_url() . "img/pus/" . $tileImg . ".jpg'>");
  } else if($pusold===1){
    $ktereOkoMamPouzitLucko = $BrDEN?"img/closed_black.png'>":"img/closed.png'>";
    echo("<img alt='$tileName' src='" . base_url() . $ktereOkoMamPouzitLucko);
  } else if($pusold===2){
    $ktereOkoMamPouzitLucko = $BrDEN?"img/org_black.png'>":"img/org.png'>";
    echo("<img alt='$tileName' src='" . base_url() . $ktereOkoMamPouzitLucko);
  } else {
    $ktereOkoMamPouzitLucko = $BrDEN?"img/open_black.png'>":"img/open.png'>";
    echo("<img alt='$tileName' src='" . base_url() . $ktereOkoMamPouzitLucko);
  }
  echo "</div><div class='tileBack'><span class='tileName'><b>$tileName</b></span><br>";
  $funkcepus = array("curr" => "aktivní člen PúSu", "currfe" => "aktivní členka PúSu", "pass" => "poradce PúSu", "passfe" => "poradkyně PúSu", "web" =>"weboděj", "passweb" => "webokněžník");
  if(array_key_exists($tileFunc, $funkcepus)){
    echo "<span class='tileFunction'><i>" . $funkcepus[$tileFunc] . "</i></span><br>";
  } else {
    echo "<span class='tileFunction'><i>" . $tileFunc . "</i></span><br>";
  }
  if($tileEmail!="") {
    $tileEmailShow = str_replace("@", "<wbr>@", $tileEmail);
    echo "<span class='tileEmail'><a href='mailto:$tileEmail' style=\"border-bottom: none\"><span class=\"fas fa-envelope\"></span></a></span>";
  }
  echo "</div></div></div>";
}

?>
<style>
#headerTextMoving::after{
  content:none;
}
</style>
<script>
function showMap() {
  if(window.matchMedia("(max-width: 600px)").matches) {
    window.open("https://goo.gl/maps/eayhaGp7Aiz", "_blank");
  } else {
    document.getElementById('homeMap').style.display='block';
  }
}

class RandomPositionImage{
  constructor(name, minheightpx, maxheightpx, randomrotate = false){
    this.rot = randomrotate;
    this.minheight = minheightpx;
    this.maxheight = maxheightpx;
    this.element = new Image;
    this.element.src = "<?=base_url()?>img/"+name;
    this.element.style.position = "absolute";
    this.element.style.transition = "1s";
    this.element.style. zIndex = "-100";
    this.element.onload = () => {
      this.ratio = this.element.width/this.element.height;
      console.log(this.ratio);
      this.render();
      document.body.appendChild(this.element);
    }
  }

  render(){
    var height = this.minheight + Math.random()*(this.maxheight-this.minheight);
    var width = this.ratio * height;
    this.element.style.height = parseInt(height)+"px";

    var windowHeight =  document.body.clientHeight;
    var windowWidth = document.body.clientWidth;

    this.element.style.top = (windowHeight-height)*Math.random()+"px";
    this.element.style.left = (windowWidth-width)*Math.random()+"px";
    if(this.rot)this.element.style.transform = "rotate("+parseInt(Math.random()*360)+"deg)";
  }
}

/*
var images = [];
images.push(new RandomPositionImage("motyl.svg", 50, 100, true));
images.push(new RandomPositionImage("motyl.svg", 100, 150, true));
images.push(new RandomPositionImage("ptak.svg", 50, 100));
images.push(new RandomPositionImage("snek.svg", 50, 100));
//images.push(new RandomPositionImage("kytka.svg", 50, 100));
images.push(new RandomPositionImage("beruska.svg", 100, 150, true));
window.addEventListener("resize", () => {
  images.forEach(img => img.render());
})
*/

</script>
<main id="main">
  <aside id="homeup">
    <a href="#" class="fas fa-arrow-circle-up"></a>
  </aside>
  <section>
    <h1 title="Brněnská přednášková noc"><span id="headerTextContainer"><span><span id="headerTextHide"></span>Br<span id="headerTextHidden">něnská přednášková </span><span id="headerTextSmallShow">NOC</span></span><span id="headerTextMoving">noc<?php if($BrDEN):?>&mdash;<span style="text-transform: none">BrDEN</span><?php endif?></span></span></h1>
  <!--  <img src="<?=base_url()?>img/placeholder.jpg" alt="Úvodní obrázek BrNOCi" width="100%">-->
  </section>
<section class="homeContainer" id="homenavINFO">
  <!--<h1>Informace o akci</h1>-->
  <p>

</p>
  <?php /*O BrNOCi*/?>
  <section>
    <span class="fas fa-question"></span><span>
      BrNOC je&nbsp;Brněnská přednášková noc, kde studenti přednášejí studentům o&nbsp;svých odborných pracích, zájmech anebo prostě o&nbsp;tématech, která jim připadají důležitá. Jedná se o&nbsp;největší akci svého druhu v&nbsp;ČR!
    </span>
  </section>
  <?php /*Kdy*/?>
  <section>
    <span class="far fa-clock"></span><span>
      Sedmý ročník se bude konat koncem letošního roku, datum ještě bude upřesněno. Můžete se těšit na úžasné přednášky, workshopy a jako již tradičně na Mystery room.
    </span>
  </section>
  <?php /*Kde*/?>
  <section>
    <span class="fas fa-map-marker-alt"></span>
    <span id="homeMapContainer" onclick="showMap()">
      <a>Budova vyššího gymnázia Jarošky na&nbsp;třídě Kapitána Jaroše 14, 658 70, Brno</a>
      <iframe id="homeMap" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5213.870391748544!2d16.6098571!3d49.2017889!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4712945c6c7feeb5%3A0x6bfcee2fbcd3d158!2zR3ltbsOheml1bSB0xZnDrWRhIEthcGl0w6FuYSBKYXJvxaFl!5e0!3m2!1sen!2scz!4v1536324546564" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </span>
  </section>
  <?php /*Kontakt*/?>
  <section>
     <span class="fas fa-id-card"></span><span class="contacts">
      <span class="fas fa-envelope"></span><span><a href="mailto:pus@brnoc.cz">pus@brnoc.cz</a></span>
      <span class="fab fa-facebook-square"></span><span><a href="https://www.facebook.com/brnoc.cz">Facebook</a></span>
      <span class="fab fa-instagram"></span><span><a href="https://www.instagram.com/brnoc5/">Instagram</a></span>
    </span>
  </section>
  <?php /*Gramatika*/?>
  <section>
    <span class="fas fa-pen-fancy"></span><span>
      Oficiální název akce je BrNOC &mdash; brněnská přednášková noc, lze použít i&nbsp;zkratkové slovo BrNOC (tj. můžete jej libovolně skloňovat a&nbsp;odvozovat z&nbsp;něj, např. přídavné jméno BrNOCí).<br>
      Prosíme nepoužívejte ani&nbsp;BrNoC, ani&nbsp;Brnoc, možnou alternativou je ještě BRNOC.
    </span>
  </section>
</section>



<section class="homeContainer" id="homenavPUS">
  <h1>PúS</h1>
  <h2>Pravoúhlý sněm</h2>
  <h3 style="text-align: center">Pravoúhlé trigonium (nejvyšší orgán Pravoúhlého spolku)</h3>
  <article class="pus trigonium">
    <?php dlazdice($BrDEN, false, "Daniel Perout", "prezident", "none", "daniel.perout@brnoc.cz"); ?>
    <?php dlazdice($BrDEN, false, "Lubor Čech", "triumvir", "none", "lubor.cech@brnoc.cz"); ?>
    <?php dlazdice($BrDEN, false, "Hana Slámová", "triumvir", "none", "hanka.514@seznam.cz"); ?>

  </article>

  <h3 style="text-align: center">Aktuální Pravoúhlý sněm</h3>
  <article class="pus">
    <?php dlazdice($BrDEN, 2, "Daniel Perout", "hlavní organizátor", "none", "daniel.perout@brnoc.cz"); ?>
    <?php dlazdice($BrDEN, 2, "Lubor Čech", "právník", "none", "lubor.cech@brnoc.cz"); ?>
    <?php dlazdice($BrDEN, 2, "Hana Slámová", "ministryně financí", "none", "hanka.514@seznam.cz"); ?>
    <?php dlazdice($BrDEN, 2, "Jakub Rádl", "web", "none", "me@jradl.cz"); ?>
    <?php dlazdice($BrDEN, 2, "Vojtěch Blažík", "grafomág", "none", "vojta.blazik@brnoc.cz"); ?>
    <?php dlazdice($BrDEN, 2, "Kateřina Havlová", "arbiter elegancie", "none", "katerina.havlova@brnoc.cz"); ?>
    <?php dlazdice($BrDEN, 2, "Lenka Procházková", "motivátorka", "none", "lenka.prochazkova@brno.cz"); ?>
    <?php dlazdice($BrDEN, 2, "Ráchel Konvalinková", "grafička", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Gabriela Wernerová", "currfe", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Otto Jirka", "curr", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Vojtěch Holan", "curr", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Jan Doležal", "curr", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Jakub Koňárek", "curr", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Anna Hronová", "currfe", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Kateřina Dvořáková", "currfe", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Lukáš Kyncl", "curr", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Jana Slámová", "currfe", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Šárka Štecherová", "currfe", "none", ""); ?>
    <?php dlazdice($BrDEN, 2, "Kristýna Koksová", "currfe", "none", ""); ?>
  </article>

  <h3 style="text-align: center">Rada starších</h3>
  <article class="pus">
    <?php dlazdice($BrDEN, 1, "Ronald Luc", "bývalý řiditel", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Petr Zelina", "passweb", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Veronika Valouchová", "proviant sacharidů", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Ivana Krumlová", "holka pro&nbsp;všechno", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Štěpán Balážik", "pass", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Martin Kurečka", "passweb", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Martin Modrý", "bývalý grafik", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Aneta Fajstlová", "bývalý ministr financí", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Minh Tran Anh", "passfe", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Ondřej Buček", "celebrita", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Vojtěch Horáček", "ruce a nohy", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Jan Česnek", "bývalý řiditel", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Andy Haupt", "hledač placek", "none", "andyh99@seznam.cz"); ?>
    <?php dlazdice($BrDEN, 1, "Berenika Chromá", "generátor textů", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Olga Krumlová", "kryptografka", "none", ""); ?>
    <?php dlazdice($BrDEN, 1, "Tomáš Vondrák", "pass", "none", ""); ?>
  </article>
</section>

  <h1>Spřízněné akce</h1>
  <section id="sprizneneAkce" class="homeContainer">
    <a class="notA" href="http://hranol.gybon.cz">
    <div class="akce">
      <img src="<?=base_url()?>img/HraNoL.png">
      <div class="akceName">HraNoL</div>
    </div>
    </a>
    <a class="notA" href="http://pnoc.cz">
    <div class="akce">
      <img src="<?=base_url()?>img/pnoc.png">
      <div class="akceName">PNOC</div>
    </div>
    </a>
    <a class="notA" href="http://prednaskovy-maraton.thilisar.cz">
    <div class="akce">
      <img src="<?=base_url()?>img/pnoc.png" alt="Logo Přednáškového maratonu" style="visibility: hidden">
      <div class="akceName">Přednáškový maraton</div>
    </div>
    </a>
    <a class="notA" href="http://plnoc.cz">
    <div class="akce">
      <img src="<?=base_url()?>img/plnoc.png" alt="Logo PilsNOCi">
      <div class="akceName">PLNOC</div>
    </div>
    </a>
  </section>

  <h1>BrNOCí archiv</h1>

  <section>
    <a class="notA" href="<?=base_url()?>archive/IV">
    <div class="akce">
      <img src="<?=base_url()?>img/archive/IV/header.jpg">
      <div class="akceName">BrIVOC (14.&mdash;15. 12. 2018)</div>
    </div>
    </a>
    <p>
    <a class="notA" href="<?=base_url()?>archive/V">
    <div class="akce">
      <img src="<?=base_url()?>img/archive/V/header.jpg">
      <div class="akceName">BrNOC 5 (22.&mdash;23. 3. 2019)</div>
    </div>
    </a>
    <p>
    <a class="notA" href="<?=base_url()?>archive/VI">
    <div class="akce">
      <img src="<?=base_url()?>img/archive/VI/header.png">
      <div class="akceName">BrNOC 6 (13.&mdash;14. 3. 2019)</div>
    </div>
    </a>
  </section>
  <!---<h1></h1>
  <h1>Feedback</h1>
  <section id="feedbackSection">

    <iframe src="https://forms.monday.com/forms/embed/20046507fb4fa8a1664491602fa57c52" width="100%" height="1000" style="border: 0; box-shadow: 5px 5px 56px 0px rgba(0,0,0,0.25);"></iframe>
  </section>--->
