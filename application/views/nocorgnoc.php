<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>NOCOrgNOC</title>
    <link rel="stylesheet" href="<?=base_url()?>css/nocorgnoc/main.css">
  </head>
  <body>
    <h1 onclick="vzum()">NOCOrgNOC</h1>
    <h2>Přednášková noc pro organizátory přednáškových nocí</h2>

    <main>
      <article> <!--Oč jde?-->
        <h3>Oč jde? & Kontakt</h3>
        <p>Soustředění podobné různým soutěžním či&nbsp;se<wbr>minárním, na&nbsp;kterém se budou prohlubovat organizační, prezentační a&nbsp;formální dovednosti, zároveň bude i&nbsp;teambuildingem mezi jednotlivými orgtýmy.</p>
        <p>
          Telefonický kontakt: <a href="tel:+420777081711">+420&nbsp;777&nbsp;081&nbsp;711</a> (Dan)<br>
          Emailový kontakt: <a href="mailto:daniel.perout@brnoc.cz">daniel.perout@brnoc.cz</a>
        </p>
      </article>
      <article> <!--Pro koho-->
        <h3>Pro koho?</h3>
        <p>Připravuješ přednáškovou noc? Mají tvé organizátorské schopnosti ještě rezervy? Jsi společenský a chceš poznat jiné podobně laděné lidi?<br>Pak jsi našel tu správnou akci!</p>
        <!--<p>Uvažuješ o účasti? Pak neváhej a kontaktuj některého z organizátorů této akce (viz Kontakt). Nabídka je časově a kapacitně omezena. Deadline přihlašování je 30.10.2019.</p>-->
        <p>Přihlašování je uzavřeno.</p>
      </article>
      <article> <!--Časoprostor-->
        <h3>Časoprostor</h3>
        <p>7.&mdash;8. 12. 2019</p>
        <p>Třebíč (kraj Vysočina)<br>
        <a href="http://www.abahoa.cz/stredisko/61401/pronajem">Hálkova 4</a></p>
      </article>
      <article> <!--Organizátoři-->
        <h3>Organizátoři</h3>
        <table>
          <tr>
            <td>BrNOC</td>
            <td><a href="mailto:daniel.perout@brnoc.cz">Daniel Perout</a></td>
          </tr>
          <tr>
            <td>PraNOC</td>
            <td>Martin Urban</td>
          </tr>
          <tr>
            <td>HraNoL</td>
            <td>Kristýna Kratochvílová</td>
          </tr>
          <tr>
            <td>PLNOC</td>
            <td>Jakub Štěpánek</td>
          </tr>
          <tr>
            <td>&pi; noc</td>
            <td>Lucie Peterková</td>
          </tr>
        </table>
      </article>
      <article class="doprava"> <!--Doprava neorg-->
        <h3>Dojezdové informace (účastníci)</h3>
        <table>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENNSp8Q:BR5aWQS04zObtzoTDn2C9Ms4foOpJt4swNhH:rnmBdLnYHO2udfGKjZfeew--">Brno</a></td>
            <td>Brno &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENOpNjMdVVKddDcl9n2nSDgzb:XDQs7H.7FJGYohGToLCI5PR:ZEI7vGU5MkWThTtptoY5joeAOjmwKsOClte4HXGY3i:W2lrbglr.rGOVZTBYm2o3PJAw6xMB1PpGvOnnFjo7aIgfPGoWuZU:tPZTNA.mCX0Qu8AZXPLSybA9O.Z">Praha</a></td>
            <td>Praha &rarr; Havlíčkův Brod &rarr; Jihlava &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENNjGKsQIuraLrQlTrgO593YG:MImNC4lcjNkvsqBMCy:mgb5kUFoRwj81pPHaTqmVQ--">Plzeň</a></td>
            <td>Plzeň &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENLvSEcchU4q5PKmXNbCwXLoogjhXsjgZwCQDlJV1V0olKyBVmFNuduU6GMoOYvN16Zu99vsilBPSfSJB5zNNIbEX80DO3E7eVP1EQ.9xojwkVWr4u5tptvkhg1kdMyAm1kgy0xQyexDvhJ91tYVKxv5c:nfKUurxZW6miM4:dWyS">Ostrava</a></td>
            <td>Ostrava &rarr; Brno &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENK5UbPC5ckqi::FRnlOGudYITCkOHR:4m2wUbkcWRWpaHBbHk:riKF5GKAKi2s8Kajvloc74TV5KjgQeDKUUgl4OioJhhNX:DYgFcwMMN.Yf8LhJhm.uN4Soh9hsO292L4qf7jicg3N4WsctNhb477c3jO0A8d9qNrPPvlHJgIiK">Hradec Králové</a></td>
            <td>Hradec Králové &rarr; Pardubice &rarr; Brno &rarr; Třebíč</td>
          </tr>
        </table>
        <p>Informace se zobrazí po kliknutí na město.</p>
      </article>
      <article class="doprava"> <!--Doprava org-->
        <h3>Dojezdové informace (organizátoři)</h3>
        <table>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENNjGKsQIuraL.Sdv9gczwqbWeS4fzorH7f7a8AG:YH1qMLxN0a2sed5VSEy12VCB:Q--">Brno</a></td>
            <td>Brno &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENE6AFz6uSaftUS1g47rjY53weeuhVmse0eCR3BqnN11M49Grua3NsSdJHodddT2UhaIuJT.pjNjNUWKVVCtCuCgcFLdvMGpKbbYZ:7.HxqwWV3iPsfNUBW8-">Praha</a></td>
            <td>Praha &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENNjGKsQIuraLZQrt8AMnrwIIgcL57dD6I2aPXd3aJ3RNV:UagKSWLJMD0DMv9zyfnA--">Plzeň</a></td>
            <td>Plzeň &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENLvSEcchU4q5jdy6uA5nFMpUlZ.1rL2qwMSJflWrV708HRSS3j7jr.AkLuR6ANZ4YJCR2VJxKMljQhMOQIUdmtrcn.d311efacdq4But7CDF890LVFFI3PsaFdRWfuQZyX1w9uim:ouyzowTyzrGmOLI3ZRclL0tfFicCI38FSVN">Ostrava</a></td>
            <td>Ostrava &rarr; Brno &rarr; Třebíč</td>
          </tr>
          <tr>
            <td><a target="_blank" href="https://idos.idnes.cz/vlakyautobusymhdvse/spojeni/prehled/?p=Fyb5SSWpsOpfNdTbad2ENK5UbPC5ckqi4Li33alcMM1zStgbroybeZ2I6skqbh2Cs0ZIX.vnb7WK2oCVZg:H7GeLmBbKifsHwM7cbr54WiFBclpxsR4iT6qOf.7AATVrkbGCXK87IEQ.6K1VhA4XcScc8Ecq47EP29pGFdQW8lqLXsSVpJwSlqapU2t4kTMj">Hradec Králové</a></td>
            <td>Hradec Králové &rarr; Brno &rarr; Třebíč</td>
          </tr>
        </table>
        <p>Informace se zobrazí po kliknutí na město.</p>
      </article>
      <article> <!--Se sebou-->
        <h3>Co se sebou?</h3>
        <ul>
          <li>venkovní i vnitřní oblečení (doproučujeme dostatek teplého)</li>
          <li>přezůvky</li>
          <li>spacák a karimatku</li>
          <li>blok a psací potřeby</li>
          <li>hudební nástroj, zpěvník (dobrovolně)</li>
        </ul>
      </article>
      <article> <!--Zdraví-->
        <h3>Bezpečnost a zdravotní omezení</h3>
        <p>Akce není pořádána oficiální organizací, bezpečnost je plně v&nbsp;rukou jednotlivých účastníků. Organizátoři za&nbsp;rodiče nezletilých účastníků <em>nepřebírají</em> odpovědnost.<br>V případě alergie či jiného omezení kontaktujte organizátory.</p>
      </article>
      <article> <!--Program-->
        <h3>Program</h3>
        <p>Příjezd je plánován během sobotního brzkého odpoledne, probíhá organizovaný program (seznamovací, herní, přednáškový, kulturní&hellip;), odjezd poté během nedělního odpoledne.</p>
        <p>Přednášet budou zajímaví bývalí organizátoři přednáškových nocí, přednášející a náplň přednášek bude upřesněna na začátku akce.</p>
      </article>
      <article> <!--Cena-->
        <h3>Cena a jídlo</h3>
        <p>Za ubytování, stravu a program je předběžně vyčíslena cena 500&#8239;Kč, předběžně bude placena záloha ve výši 200&#8239;Kč.</p>
        <p>Jídlo je zajištěno v ceně (večeře + snídaně + oběd + svačinky v&nbsp;průběhu).</p>
      </article>
    </main>
  </body>
  <style id="style"></style>
  <style id="stylewide" media="(min-width: 768px)"></style>
  <script>
    var defaultCollection = document.getElementsByTagName("article");
    var n = defaultCollection.length;

    for (var i = 1; i <= n; i++) {
      document.getElementById("style").innerHTML += "#lol" + i + "{grid-row:" + i +"}\n"
      var column = i%2+1;
      document.getElementById("stylewide").innerHTML += "#lol" + i + "{grid-row:" + (i + (i%2))/2 +";\ngrid-column:" + column +"}\n"
    }

    function vzum() {
      var newArray = [];
      for (var i = 0; i < n; i++) {
        (Math.random()>=0.5)?newArray.push(defaultCollection[i]):newArray.unshift(defaultCollection[i]);
      }
      for (var i = 1; i <= newArray.length; i++) {
        newArray[i-1].id = "lol" + i;
      }
    }
  </script>
</html>