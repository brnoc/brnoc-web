
<?php if($messages):?>
  <?php foreach($messages as $message):?>
    <?php if($message["text"]):?>
    <div class="msg <?=$message["type"]?>"><?=$message["text"]?></div>
    <?php endif;?>
  <?php endforeach;?>
<?php endif;?>
<section id="socksSection">
  <?php /*
  <h2>Ponožky</h2>
  <?php if(0):?>
    <form action="<?=base_url()?>user/order_socks" method="post">

      Jeden pár ponožek stojí 75&nbsp;Kč.
      <?php if($_SESSION['user']['organizer']):?>
      Protože jsi organizátor, můžeš si objednat jeden pár zdarma.<br>
      <?php elseif($_SESSION['user']['speaker']):?>
      Protože jsi předášející, můžeš si objednat jeden pár zdarma.<br>
      <?php endif;?>
      </p>
      <?php if($messages):?>
        <?php foreach($messages as $message):?>
          <?php if($message["text"]):?>
          <div class="msg <?=$message["type"]?>"><?=$message["text"]?></div>
          <?php endif;?>
        <?php endforeach;?>
      <?php endif;?>
      <br>
      <table class="socksTable">
        <thead>
          <tr><th colspan="3">Objednávání ponožek dle velikosti</th></tr>
          <tr><td>Velikost ponožek</td><td>Počet objednaných ponožek</td><td>Objednáno</td></tr>
        </thead>
        <tbody>
          <?php foreach ($velikostPonozky as $key => $value):?>
           <tr>
             <td><?=$value?></td>
             <td><input name="socks<?=$key?>" type="number" value="<?=isset($socks[$key])?$socks[$key]:0?>" min="0" max="100" placeholder="ponožky velikosti <?=$value?>"></td>
             <td style="padding:10px;"><?=$ordered[$key]?>/<?=$maxPonozky[$key]?></td>
           </tr>
         <?php endforeach;?>
        </tbody>
      </table>
      <input type="submit" id="submit" value="Objednat"/>
    </form>
  <?php else:?>
    Objednávání ponožek již bylo zastaveno. Přebytky bude možné zakoupit na akci.</p>
  <?php endif;?>
  <div class="socksContainer">
    <img src="<?=base_url()?>img/ponozky1.jpg">
    <img src="<?=base_url()?>img/ponozky2.jpg">
  </div>
  *&nbsp; používány evropské (francouzské) jednotky &mdash; viz <a href="<?=$tabulkaPonozky?>">tabulka</a>.
</section>*/?>
<section id="cupSection" onclick="cupSectionClicked()">
  <h2>Merch</h2>

  <h3>Kelímky</h3>
  <p>Stejně jako na minulých ročnících bude i tentokrát možné si na&nbsp;akci zapůjčit plastový kelímek za&nbsp;vratnou zálohu <strong><?=make_price(30)?></strong>.</p>
  <h3>Bloky</h3>
  <p>Na akci bude možné si zakoupit papírový blok formátu A4, 50 listů, za <strong><?=make_price(50)?></strong>.</p>
  <h3>Trička</h3>
  <p>Na letošní, tj. šesté, BrNOCi budou merchem trička, které si můžeš předběžně <strong>zarezervovat</strong> (tričko ti schováme a&nbsp;nemusíš se tak bát, že by na&nbsp;tebe už nezbylo). Měsíc před akcí bude možné trička i&nbsp;objednat (tzn. rovnou je zaplatit), takže na akci už si tričko pouze vyzvedneš.<br>
  Zarezervovat si můžeš maximálně 3 trička.<br>
  Cena triček bude <strong><?=make_price(150)?></strong>.<br>
  Více informací se dozvíš v našich <a href="<?=base_url()?>legal">podmínkách</a>.</p>
  <div style="display:grid; grid-template-columns:1fr 1fr; margin: 0 5vw">
    <img src="<?=base_url()?>img/shirt_front.png" style="width:100%"/>
    <img src="<?=base_url()?>img/shirt_back.png" style="width:100%"/>
  </div>
</section>
<section>
  <h3>Tvé objednávky:</h3>
  <form action="<?=base_url()?>user/order" method="post">
  <div style="display:grid; grid-template-columns:1fr; grid-column-gap: 1em">

    <span style="padding-top:.5em">S: <?=json_decode($_SESSION["user"]["orders"])->S?></span><!--<input type="number" name="s" value="<?=json_decode($_SESSION["user"]["orders"])->S?>">-->
    <span style="padding-top:.5em">M: <?=json_decode($_SESSION["user"]["orders"])->M?></span><!--<input type="number" name="m" value="<?=json_decode($_SESSION["user"]["orders"])->M?>">-->
    <span style="padding-top:.5em">L: <?=json_decode($_SESSION["user"]["orders"])->L?></span><!--<input type="number" name="l" value="<?=json_decode($_SESSION["user"]["orders"])->L?>">-->
  </div>
  <!--<input name="submit" type="submit" value="Změnit" style="float:right"/>-->
  </form>
</section>
