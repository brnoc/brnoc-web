<section id="newTalk">
  <h2>Nová přednáška</h2>
  <a href="<?=base_url()?>talks/new"><div>Přihlásit novou přednášku...</div></A>
  <div>Pokud chcete mít na BrNOCi workshop, napište nám na <a href="mailto:pus@brnoc.cz">pus@brnoc.cz</a></div>
</section>

<section id="talks">
  <h2>Moje přednášky čekající na schválení</h2>
  <?php foreach($temp_talks as $talk):?>
    <?=$talk->title?>
  <?php endforeach;?>
  <h2>Moje schválené přednášky</h2>
  <?php
  foreach ($talks as $key => $value) {
    echo "<a href=\"".base_url()."talks/view/$value->id\">";
    echo "<div class=\"talk\">";
      echo "<div class=\"title\">$value->title</div>";
      echo "<div class=\"desc\">$value->short_desc</div>";
    echo "</div>";
    echo "</a>";
  }
  ?>
</section>
