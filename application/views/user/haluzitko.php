<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/haluzitko.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/haluzitko-areas.css">
    <link rel="icon" type="image/png" href="<?=base_url()?>img/penrose.png">
    <title>BrNOC Timetable</title>
</head>
<body>
<section class="fullscreen">
    <div id="stuff">
        <div id="lectureStore" class="lectureStore scrolldiv"></div>

        <div class="ttDiv scrolldiv">
            <table id="timeTable" class="timeTable">
                <tr>
                    <th>Místnost A</th>
                    <th>Místnost B</th>
                    <th>Místnost C</th>
                    <th>Místnost D</th>
                </tr>
            </table>
        </div>
    </div>

    <span style="font-size:30px;cursor:pointer;z-index:100000" onclick="openSideBar()">&#9776;</span>

    <div id="sidebar" class="sidebar">
        <a style="cursor:pointer" class="closebtn" onclick="closeSideBar()">&times;</a>
        <div id="scoreDiv" class="sbText">
            Skóre: <b id="score">0.00</b>
            <hr id="progressLine"/>
        </div>
        <hr class="divider"/>
        <a id="haluzBtn" class="sbText" style="cursor:pointer" onclick="haluzitko.vyhaluz()">
            Vyhaluz
        </a>
        <a class="sbText" style="cursor:pointer" onclick="haluzitko.fillTT();">
            Náhodně<br>naplnit
        </a>
        <a class="sbText" style="cursor:pointer" onclick="haluzitko.clearTT()">
            Vyprázdnit
        </a>
        <a class="sbText" style="cursor:pointer" onclick="resetShape()">
            Tvar
        </a>
        <a class="sbText" style="cursor:pointer" onclick="export2html()">
            Export HTML
        </a>
        <a class="sbText" style="cursor:pointer" onclick="exportToDatabase()">
            Save
        </a>
        <a class="sbText loadListTitle" style="cursor:pointer" onclick="loadTimetableDefs()">
            Load
        </a>
        <ul id="loadList" style="visibility:hidden" class="loadList">
        </ul>
        <div id="ruleStatus"></div>
        <img id="loadGif" src="<?=base_url()?>img/trans3.gif" />
    </div>
</section>

    <script>
        function openSideBar() {
            document.getElementById("sidebar").style.width = "200px";
        }
        function closeSideBar() {
            document.getElementById("sidebar").style.width = "0";
        }
    </script>

    <script src='<?=base_url()?>js/haluzitko/axios.min.js'></script>
    <script src='<?=base_url()?>js/haluzitko/dragula.js'></script>
    <script src='<?=base_url()?>js/haluzitko/libgif.js'></script>

    <script src='<?=base_url()?>js/haluzitko/iofeatures.js'></script>
    <script src='<?=base_url()?>js/haluzitko/rules.js'></script>
    <script src='<?=base_url()?>js/haluzitko/haluzitko.js'></script>
    <script src='<?=base_url()?>js/haluzitko/draglogic.js'></script>
</body>
