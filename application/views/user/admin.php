<h2>Administrace</h2>
<style>
.link {
  width: 90%;
  margin: 20px auto;
  text-align: center;
  padding: 20px 0px;
  border: 2px solid white;
  border-radius: 5px;
  background-color: rgba(0, 0, 0, 0); }
  .link:hover, .link:active, .link:focus {
    background-color: white;
    border-color: black;
    color: black; }
    .link:hover a, .link:active a, .link:focus a {
      color: #333;
      border-color: #333; }
  .link:active {
    outline: none; }
  .link:focus {
    outline-color: white; }
</style>
<a href="<?=base_url()?>admin">
  <div class="link">
    Pro vstup do administrace klikněte zde.
  </div>
</a>
