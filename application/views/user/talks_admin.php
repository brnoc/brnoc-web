<section id="stats">
  <h2>Statistika</h2>
  Počet přednášejících: <?=$talks_info["speakers"]?> <br>
  Počet schválených přednášek: <?=$talks_info["talks"]?> <br>
  Počet schávelných přednáškominut: <?=$talks_info["talk_minutes_confirmed"]?> (<?=intval($talks_info["talk_minutes_confirmed"]/1440)?>d <?=intval($talks_info["talk_minutes_confirmed"]/60%24)?>h <?=intval($talks_info["talk_minutes_confirmed"]%60)?>min) <br>
  Počet všech přednáškominut: <?=$talks_info["talk_minutes_all"]?> (<?=intval($talks_info["talk_minutes_all"]/1440)?>d <?=intval($talks_info["talk_minutes_all"]/60%24)?>h <?=intval($talks_info["talk_minutes_all"]%60)?>min) <br>
</section>

<section id="toBeConfirmed">
  <h2>Přednášky ke schválení</h2>
  <?php foreach($talks_temp as $talk):?>
  <div class="talkWrapper">
    <div class="talkHead" onclick="toggle(this.parentElement)">
      <div class="talkTitle"><?=$talk['title']?></div>
      <div class="talkSpeaker"><?=$talk['speaker']?></div>
    </div>
    <div class="talkShort" onclick="toggle(this.parentElement)">
        <div><?=$talk['short']?></div>
    </div>
    <div class="talkLong talkSpeakerInfo" onclick="toggle(this.parentElement)">
      <div>
        Schválené přednáškominuty přednášejícího: <?php echo $stm = $talks_info["speaker_info"][$talk["speaker_id"]]["talk_minutes_confirmed"]?>m (s touto přednáškou: <span class="<?=($stm + $talk["length"]>120)?"over":""?>"><?=$stm + $talk["length"]?>m</span>)
      </div>
    </div>
    <div class="talkLong" onclick="toggle(this.parentElement)">
        <div>
          <?=$talk['long']?>
          </p>
          Délka: <?=$talk['length']?> minut.
        </div>
    </div>
    <div class="controls">
        <a href="<?=base_url()?>talks/confirm/<?=$talk['id']?>"><div class="btn yes">Potvrdit</div></a>
        <a href="<?=base_url()?>talks/reject/<?=$talk['id']?>"><div class="btn no">Odmítnout</div></a>
    </div>
  </div>
  <?php endforeach;?>
  <?php if(!$talks_temp)echo "Žádné přednášky ke schválení.";?>
</section>

<section id="confirmed">
  <h2>Schválené přednášky</h2>
  <?php foreach($talks as $talk):?>
    <div class="talkWrapper">
      <div class="talkHead" onclick="toggle(this.parentElement)">
        <div class="talkTitle"><?=$talk['title']?></div>
        <div class="talkSpeaker"><?=$talk['speaker']?></div>
      </div>
      <div class="talkShort" onclick="toggle(this.parentElement)">
          <div><?=$talk['short']?></div>
      </div>

      <div class="talkLong" onclick="toggle(this.parentElement)">
          <div>
            <?=$talk['long']?>
            </p>
            Délka: <?=$talk['length']?> minut.
          </div>
      </div>
      <div class="controls">
          <a class="notA" href="<?=base_url()?>talks/view/<?=$talk['id']?>">
            <div class="btn yes">Detail</div>
          </a>
      </div>
    </div>
  <?php endforeach;?>
</section>


<script>
function toggle(el){
  var talks = document.getElementsByClassName('talkWrapper');
  if(el.classList.contains("active"))
    el.classList.remove("active");
  else{
    Array.prototype.forEach.call(talks,talk => {
      talk.classList.remove('active');
    });
    el.classList.add('active');
  }
}
</script>
