<?= isset($message)?"<div class=\"msg $message[0]\">$message[1]</div>":""?>
<?php if(isset($message2))echo$message2?"<div class=\"msg $message2[0]\">$message2[1]</div>":""?>
<?=validation_errors('<div class="msg error">','</div>')?>
<section id="paymentSection">
  <div class="left">
    <h2>Platby</h2>
    <?php if(1):?>
    <?php

    $pay =70-$_SESSION['user']['paid'];
    if ($_SESSION['user']['speaker'] || $_SESSION['user']['organizer']){
      $pay-=70;
    }
    $pay -= $_SESSION['user']['disc'];
    ?>
    <p>
      Tvůj variabilní symbol: <b><?=$_SESSION['user']['vs']?></b><br>
      Číslo účtu: <b>2201495870/2010</b><br>
      Jedná se o transparentní účet, takže všechny zadané údaje budou veřejně viditelné.
      Číslo účtu bude během listopadu změněno, takže je možné, že ve výpisu transparentního účtu neuvidíš svou platbu.
    </p>
    <p>
      Účast na akci stojí při platbě předem <b>70&nbsp;Kč</b>. (Na místě se platí 90&nbsp;Kč)<br>
      <?php
      if($_SESSION['user']['organizer'])
      echo "Protože jsi organizátor, neplatíš vstupné.<br>";
      elseif($_SESSION['user']['speaker'])
      echo "Protože jsi předášející, neplatíš vstupné.<br>";
      ?>
      <?php if($_SESSION['user']['disc']):?>
      Byla ti poskytnuta sleva <?=$_SESSION['user']['disc']?>&nbsp;Kč.<br>
      <?php endif;?>
      Máš zaplaceno: <?=$_SESSION['user']['paid']?>&nbsp;Kč.
    </p>
    <p>
      Zbývá ti tedy doplatit <strong style="color:<?=$pay?"red":"green"?>"><?=$pay?></strong> Kč.
      (Pokud to nesedí, počkej pár dní, nebo nám napiš.)
    </p>
  </div>
  Zaplať naskenováním QR kódu:<br>
  <div id="qr"></div>

  <script src="<?=base_url()?>js/qrcode.js"></script>
  <script>
  var qr = new QRCode("qr");
  qr.makeCode("SPD*1.0*ACC:CZ5520100000002201495870*AM:<?=$pay?>*CC:CZK*MSG:Platba za BrNOC*X-VS:<?=$_SESSION["user"]["vs"]?>*RN:Pravoúhlý sněm");
  </script>

  <?php endif;?>

</section>
<section id="profilePictureSection">
  <h2>Profilový obrázek</h2>
  <img src='<?=$_SESSION['user']['img']?>' id="pic"/>
  <form action="<?=base_url()?>user/img" method="post" enctype="multipart/form-data">
    <input type="file" name="img" id="picInp" class="inputfile" onchange="load()"/>
    <label for="picInp" class="custom-file-upload">Vyberte obrázek</label>
    <input type="submit" value="Změnit"/>
  </form>
  Obrázek nesmí být větší než 1&nbsp;MB a musí být v jednom z formátů: JPG, JPEG, PNG, SVG.
</section>
<section id="dataSection">
  <h2>Osobní údaje</h2>
  <form action="<?=base_url()?>user/setData" method="post">
    <table>
      <tr>
        <td><label for="name">Přihlašovací jméno: </label></td>
        <td><input name="name" type="text" value="<?=$_SESSION['user']['name']?>"/></td>
      </tr>
      <tr>
        <td><label for="firstname">Jméno: </label></td>
        <td><input name="firstname" type="text" value="<?=$_SESSION['user']['firstname']?>"/></td>
      </tr>
      <tr>
        <td><label for="lastname">Příjmení:</label></td>
        <td><input name="lastname" type="text" value="<?=$_SESSION['user']['lastname']?>"/></td>
      </tr>
      <tr>
        <td><label for="email">Email: </label></td>
        <td><input name="email" type="email" value="<?=$_SESSION['user']['email']?>"/></td>
      </tr>
    </table>
    <input type="submit" name="submit" value="Změnit"/>
  </form>
</section>
<section id="passwordSection">
  <h2>Změna hesla</h2>
  <form action="<?=base_url()?>user/setPassword" method="post">
    <table>
      <tr>
        <td><label for="old">Staré heslo:</label></td>
        <td><input name="old" type="password" autocomplete="current-password"></td>
      </tr>
      <tr>
        <td><label for="new">Nové heslo:</label></td>
        <td><input name="new" type="password" autocomplete="new-password"></td>
      </tr>
      <tr>
        <td><label for="new2">Nové heslo znovu:</label></td>
        <td><input name="new2" type="password" autocomplete="new-password"></td>
      </tr>
    </table>
    <input type="submit" name="submit" value="Změnit"/>
  </form>
</section>


<section id="contactSection">
  <h2>Kontakt</h2>
  Pokud jsi organizátor, tak se tyto informace zobrazí na tvé kartě v představení Pravoúhlého sněmu.
  Pokud jsi přednášející, budou tyto informace zobrazeny v detailu tvé přednášky.
  <div>
    Připojit nový kontakt (prázdný kontakt bude smazán):
    <?php
    $networks = [
      "facebook" => ["facebook.com/","fab fa-facebook-square"],
      "messenger" => ["m.me/","fab fa-facebook-messenger"],
      "instagram" => ["instagram.com/","fab fa-instagram"],
      "twitter" => ["twitter.com/","fab fa-twitter-square"],
      "github" => ["github.com/","fab fa-github-square"],
      "reddit" => ["reddit.com/u/","fab fa-reddit-square"],
      "youtube" => ["youtube.com/user/","fab fa-youtube-square"],
      "telegram" => ["t.me/","fab fa-telegram"],
      "viber" => ["chats.viber.com/", "fab fa-viber"],
      "twitch" => ["twitch.tv/", "fab fa-twitch"],
      "spotify" => ["open.spotify.com/user/", "fab fa-spotify"],
      "pornhub" => ["pornhub.com/user/", "fab fa-pornhub"],
      "vkontakte" => ["vk.com/","fab fa-vk"],
    ];
    $user_networks = json_decode($_SESSION['user']['contact']);
    ?>
    <div class="icons" id="icons">
      <?php foreach ($networks as $key => $value) {
        if(!isset($user_networks->$key)){
          if($key != "pornhub")
            echo "<i onclick=\"move(this,'$key','$value[0]')\" class=\"$value[1]\"></i>";
          else
            echo "<img onclick=\"move(this,'$key','$value[0]')\" src=\"".base_url()."img/ph.svg\" height=\"30px\"/>";
        }
      }?>
    </div>
    <form id="form" method="post"action="<?=base_url()?>user/setContact">
      <div>
        <?php
          if($user_networks)
          foreach($user_networks as $key => $value){
            if($key != "pornhub")
            echo "
            <div class=\"wrapper\">
              <i onclick=\"move(this,'$key','".$networks[$key][0]."')\" class=\"".$networks[$key][1]."\"></i>
              <div class=\"input\">
                <span>".$networks[$key][0]."</span><input name=\"$key\" type=\"text\" value=\"$value\"/>
              </div>
            </div>
            ";
            else
            echo "
            <div class=\"wrapper\">
              <img onclick=\"move(this,'$key','$value[0]')\" src=\"".base_url()."img/ph.svg\" height=\"30px\"/>
              <div class=\"input\">
                <span>".$networks[$key][0]."</span><input name=\"$key\" type=\"text\" value=\"$value\"/>
              </div>
            </div>
            ";
          }
        ?>
      </div>
      <input type="submit" name="submit" value="Změnit"/>
    </form>
  </div>
</section>

<section id="deleteSection">
  <h2>Zrušit registraci a smazat účet</h2>
  Pokud se z nějakých důvodů nemůžeš na akci přijít, zruš prosím svou registraci a uvolni místo ostatním. Stiskni pro smazání svého uživatelského účtu. Budou smazána veškerá Tvá osobní data a Tebou zaregistrované přednášky.
  <br><div><a href="<?=base_url()?>user/deleteAcc" class="notA"><div class="btn deleteAcc">Smazat</div></a></div>
</section>


<script>
    function load() {
        var file = document.getElementById('picInp').files[0];
        var reader  = new FileReader();
        reader.onload = function(e)  {
            var image = document.getElementById('pic');
            image.src = e.target.result;
         }
         reader.readAsDataURL(file);
     }

     function move(icon,name,label){
       if (!(icon.parentElement == document.getElementById('icons'))) return;
       var div = document.createElement('div');
       var input = document.createElement('input');
       var form = document.getElementById('form');
       var span = document.createElement('span');
       var wrapper = document.createElement('div');

       span.innerHTML = label;
       input.type="text";
       input.name=name;
       div.appendChild(span);
       div.appendChild(input);
       div.className = "input";
       wrapper.appendChild(icon);
       wrapper.appendChild(div);
       wrapper.className= "wrapper";
       form.children[0].appendChild(wrapper);
     }

     var inputs = document.querySelectorAll( '.inputfile' );
       Array.prototype.forEach.call( inputs, function( input )
       {
       	var label	 = input.nextElementSibling,
       		labelVal = label.innerHTML;

       	input.addEventListener( 'change', function( e )
       	{
       		var fileName = '';
       		if( this.files && this.files.length > 1 )
       			fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
       		else
       			fileName = e.target.value.split( '\\' ).pop();

       		if( fileName )
       			label./*querySelector( 'span' ).*/innerHTML = "<span><span class='fas fa-file-upload' style='margin-right: 1em; font-size: 1.5em'></span>" + fileName + "</span>";
       		else
       			label.innerHTML = labelVal;
       	});
       });

</script>
