<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<main style="display: grid; grid-template-columns: 100%; grid-template-rows: auto auto auto; justify-items: center; align-items: center; grid-row-gap: 10vh; margin-bottom: auto;">
  <h1><?=$title?></h1>
  <div>
    <img src="<?=base_url()?>/img/closed.png" style="position: relative">
    <span style="position: absolute; width: 100%; left: 0; margin-top: -180px; text-align: center; font-size: 300%; font-weight: bold"><?=$message?></span>
  </div>
  <h2 style="text-align: center"><?=$text?></h2>
</main>
