<?php
$poznamky = array(
  "povinný údaj",
);

function hvezdicka($i) {
  echo "&nbsp;<a href=\"#pozn$i\" class=\"footnote\">";
  for($j=0; $j<$i; $j++) echo "*";
  echo "</a>";
}

function generateFootnote($poznamky) {
  unset($key);
  $i=1;
  $hvezdicky = "";
  foreach ($poznamky as $key => $value) {
    $hvezdicky .= "*";
    echo "<div id=\"pozn" . $i . "\">$hvezdicky &mdash; $value</div>";
    $i++;
  }
}
 ?>
<main id="main">
  <h1>Závazná registrace na BrNOC (6)</h1>
  <section id="formSection">
    <div class="formErrors"><?= validation_errors("<div class=\"msg error\"><i class=\" fas fa-exclamation-triangle\"></i>&nbsp;&nbsp;","</div>")?></div>
    <form action="<?=base_url()?>sign/up" method="post" accept-charset="utf-8" class="center">
      <fieldset>
        <legend>Přihlašovací údaje<?=hvezdicka(1)?></legend>
        <?=inputTemplate("<input name=\"name\" type=\"text\" value=\"" . set_value('name') . "\" placeholder=\"např. hugo.kokoska\" autocomplete=\"username\" required>", "Přihlašovací jméno")?><br>
        <?=inputTemplate("<input name=\"password\" type=\"password\" placeholder=\"alespoň 5 znaků, klidně i emoji 😉\" value=\"" . set_value('password') ."\" autocomplete=\"new-password\" required>", "Heslo")?><br>
        <?=inputTemplate("<input name=\"password2\" type=\"password\" placeholder=\"znovu heslo poprosíme\" value=\"" . set_value('password2') . "\" autocomplete=\"new-password\" required>", "Heslo znovu")?><br>
      </fieldset>
      <fieldset>
        <legend>Osobní údaje<?=hvezdicka(1)?></legend>
        <?=inputTemplate("<input name=\"email\" type=\"email\" placeholder=\"např. hugo.kokoska@brnoc.cz\" value=\"" . set_value('email') . "\" required>", "Email")?><br>
        <?=inputTemplate("<input name=\"firstname\" type=\"text\" placeholder=\"např. Hugo\" value=\"" . set_value('firstname') . "\" required>", "Jméno")?><br>
        <?=inputTemplate("<input name=\"lastname\" type=\"text\" placeholder=\"např. Kokoška\" value=\"" . set_value('lastname') . "\" required>", "Příjmení")?><br>
      </fieldset>
      <fieldset>
        <legend>Škola</legend>
        Ze statistických důvodů nás zajímá, kam chodíš na&nbsp;školu. Tento údaj nebudeme s&nbsp;nikým sdílet ani k&nbsp;ničemu než anonymní statistice využívat.
        <?=inputTemplate("<input name=\"school\" type=\"text\" placeholder=\"např. Gymnázium Brno, tř. Kpt. Jaroše 14\" value=\"" . set_value('school') . "\" required>", "Škola")?><br>
      </fieldset>
      <fieldset onclick="schovejSouhlasRodicu()">
        <legend>Věk<?=hvezdicka(1)?></legend>
        <?php
        $message = [
          "V první den akce mi bude nejvýše 13 let",
          "V první den akce mi bude alespoň 14 a nejvýše 17 let.",
          "V první den akce mi bude alespoň 18 let."];
          for($i=0; $i<3; $i++) {
            echo "<label class=\"radioContainer\">" .$message[$i] . "<input id=\"souhlasRodicuc$i\" name=\"age\" type=\"radio\" value=\"". ($i+1) . "\"";
            if($i==2) echo " checked";
            echo "><span class=\"checkmark\"></span></label>";
          }?>
          Platí-li některé podmínky současně, pak zvolte tu podmínku s nižším věkovým ohraničením.<br><br>
        <span id="souhlasRodicu">Účastníci mladší 18 let potřebují  <a href="<?=base_url()?>docs/consent.pdf" download="Souhlas s účastí na BrNOCi.pdf"><span class="far fa-file-pdf"></span> souhlas zákonného zástupce.</a></span>
      </fieldset>
      <?php if(0):?>
        <fieldset>
          <legend>BrBloky<?=hvezdicka(1)?></legend>
          Bloky je možné objednat i po registraci. Zaplatit musíte předem (tj. není možné koupit či zaplatit na&nbsp;akci).
          Jeden pár ponožek stojí 75&nbsp;Kč. Pokud budeš přednášet, dostaneš jeden z objednaných bloků zdarma. <?=(rand(1,5)==1)?"A Horste, to se vyplatí!":""?> <br>
          <label>Chci si objednat: <input type="number" min="0" max="10" name="order" value="0" id="brblokInput" onkeydown="brblok()" onupdate="brblok() "onclick="brblok()"/> BrBlok<span id="brblok">ů</span>.</label>
        </fieldset>
      <?php elseif(0):?>
        <fieldset>
          <legend>BrBloky<?=hvezdicka(1)?></legend>
          Objednávání ponožek bylo zastaveno. Přebytky bude možné zakoupit na akci.
        </fieldset>
      <?php endif;?>
      <!--<fieldset>
        <legend>Trička</legend>
        <div style="display:grid; grid-template-columns:1fr 1fr">
          <img src="<?=base_url()?>img/shirt_front.png" style="width:100%"/>
          <img src="<?=base_url()?>img/shirt_back.png" style="width:100%"/>
        </div>
        <div style="text-align:justify">
          <p>Na letošní, tj. šesté, BrNOCi budou merchem trička, které si můžeš předběžně <strong>zarezervovat</strong> (tričko ti schováme a&nbsp;nemusíš se tak bát, že by na&nbsp;tebe už nezbylo). Měsíc před akcí bude možné trička i&nbsp;objednat (tzn. rovnou je zaplatit), takže na akci už si tričko pouze vyzvedneš.<br>
          Zarezervovat si můžeš maximálně 3 trička.<br>
          Více informací se dozvíš v našich <a href="<?=base_url()?>legal">podmínkách</a>.</p>
        </div>
        <div style="display:grid; grid-template-columns:auto 1fr; grid-column-gap: 1em">
          <span style="padding-top:.5em">S</span><input type="number" name="s" value="0">
          <span style="padding-top:.5em">M</span><input type="number" name="m" value="0">
          <span style="padding-top:.5em">L</span><input type="number" name="l" value="0">
        </div>
      </fieldset>-->
      <fieldset>
        <legend>Mystery room <?=hvezdicka(1)?></legend>
        <?=inputTemplate("<input type=\"text\" name=\"mystery\" placeholder=\"např. nejlepší způsob cesty do školy v županu, má rád Višnu višně?, sexuální atraktivita rypoušů, 33 způsobů, jak odmítnout sex\">", "Napadá tě nějaké téma do Mystery Room?")?><br>
      </fieldset>
      <fieldset class="trms">
        <legend>General Data Protection Regulation<?=hvezdicka(1)?></legend>
        <label class="radioContainer">Souhlasím s <a href="<?=base_url()?>legal">podmínkami</a> užívání stránek a&nbsp;ochranou osobních údajů.<input name="terms" type="checkbox" required><span class="checkmark"></span></label>
        <label class="radioContainer">Souhlasím se zasíláním propagačních a&nbsp;informačních emailů<input name="newsletter" type="checkbox" checked><span class="checkmark"></span></label>
      </fieldset>
      <fieldset>
        <strong>Upozornění:</strong><br>
        Registrace na akci je <span style="text-transform: uppercase">závazná</span>.<br>
        Pokud bys náhodou nemohl dorazit, svoji registraci prosím neprodelně zruš v&nbsp;uživatelské sekci.
        </p>
        <input name="submit" type="submit" value="Registrovat"/>
      </fieldset>
      <footer>
        <?php generateFootnote($poznamky);?>
      </footer>
    </form>

    <div>
      Již jsi registrován? Přihlaš se <a href="<?=base_url()?>sign/in">zde</a>.
    </div>

  </section>
</main>
<script>
function schovejSouhlasRodicu() {
    if(document.getElementById("souhlasRodicuc2").checked) {
      document.getElementById("souhlasRodicu").style.display="none";
    } else {
      document.getElementById("souhlasRodicu").style.display="initial";
    }
  }
  schovejSouhlasRodicu();

function brblok(){
  var brb = document.getElementById("brblokInput").value;
  document.getElementById("brblok").innerHTML = (brb == 1)? "": (brb > 1 && brb < 5)?"y":"ů";
}
</script>
