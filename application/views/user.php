<?php
echo "<script>host='".base_url()."';</script>";
?>
<main id="user">
<h1><?=$_SESSION['user']['firstname']?> &bdquo;<?=$_SESSION['user']['name']?>&ldquo; <?=$_SESSION['user']['lastname']?></h1>
<?php
  function faktorizuj($n) {
    $zbytek = $n;
    for ($i = 2; $i <= sqrt($zbytek); $i++) {
        while ($zbytek % $i == 0) {
            $zbytek = $zbytek / $i;
            $factors[] = $i;
        }
    }
    if ($zbytek > 1) {
        $factors[] = $zbytek;
    }

    return $factors;
  }
  $numMenu = count($menu);
  if($numMenu>=2) {
    $numMenuFactors = faktorizuj($numMenu);
    sort($numMenuFactors);
    if($numMenuFactors[count($numMenuFactors)-1]<6) {
      $menuClass = "UM".$numMenuFactors[count($numMenuFactors)-1];
    } else {
      $menuClass = "";
    }
  } else {
    $menuClass = "";
  }
?>
<div id="userMenu" class=<?php echo $menuClass;?>>
  <?php
    foreach($menu as $value){
      if($value[2])
        echo "<a href=\"".base_url().$value[0]."\" ><div class=\"active\">".$value[1]."</div></a>";
      else
        echo "<a href=\"".base_url().$value[0]."\"><div>".$value[1]."</div></a>";
    }
  ?>
</div>
