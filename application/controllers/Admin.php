<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
    $this->load->model('user_model');
		$this->load->model('talks_model');
    if(!$this->user_model->rank('admin')){
			return redirect(base_url()."user");
			exit;
		}
	}

	public function index(){
		$this->load->view("admin/header",['page' => "home"]);
		$this->load->view("admin/home");
		$this->load->view("admin/footer");
	}

	public function attendance(){
		$this->load->view("admin/header",['page'=>"attendance", "stylesheets" => ["attendance"], "scripts" => ["jquery-1.12.3.min"]]);
		$this->load->view("admin/attendance",[
			"payments" => $this->user_model->update_payments(),
			"talks" => $this->user_model->get_talks_by_users()
		]);
		$this->load->view("admin/footer");
	}

	public function talks(){
		$this->load->view("admin/header",['page'=>"talks", "stylesheets" => ["talks","tables"], "scripts" =>["jquery-1.12.3.min","tables","charts"], ]);
		$this->load->view("admin/talks", [
			'talks' => $this->talks_model->get(false,true,true),
			'info' => $this->talks_model->get_stats(),
			'pref' => $this->talks_model->get_preferences(),
			'feedback' => $this->talks_model->get_preferences(true),
		]);
		$this->load->view("admin/footer");
	}

	public function users(){
		$this->load->view("admin/header",['page'=>"users","scripts" => ["jquery-1.12.3.min", "tables", "charts"],"stylesheets" => ["tables","users"] ]);
		$this->load->view("admin/users", ["users" => $this->user_model->get_user_data(false), "stats" => $this->user_model->get_statistics()]);
		$this->load->view("admin/footer");
	}

	public function haluzitko(){
		$this->load->view("admin/header",['page'=>"haluzitko", "stylesheets" => ["haluzitko", "haluzitko-areas"]]);
		$this->load->view("admin/haluzitko");
		$this->load->view("admin/footer");
	}

	public function mystery(){
		$this->load->model('mystery_model');
		$this->load->view("admin/header",['page'=>"mystery", "scripts" => ["tables", "jquery-1.12.3.min"], "stylesheets" => ["mystery","tables"]]);
		$this->load->view("admin/mystery", ['topics' => $this->mystery_model->get()]);
		$this->load->view("admin/footer");
	}
}
