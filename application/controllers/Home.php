<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index()
	{
		$this->config->load("BrDEN");
		$BrDEN = $this->config->item("BrDEN");
		$this->load->model('user_model');
		$this->load->view('header');
		$this->load->view('home_mezinoc',["users" => $this->user_model->count_all_users(), "BrDEN" => $BrDEN]);
		$this->load->view('footer');
	}
}
