<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index()
	{
    redirect(base_url().'home');
	}

  public function emailSuc(){
    $data['title']="Email úspěšně ověřen";

    $this->load->view('header',$data);
    $this->load->view('mess/emailSuc.php',$data);
    $this->load->view('footer',$data);

  }

  public function emailFail(){
    $data['title']="Email úspěšně ověřen";

    $this->load->view('header',$data);
    $this->load->view('mess/emailFail.php',$data);
    $this->load->view('footer',$data);

  }
}
