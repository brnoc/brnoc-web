<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archive extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index(){
		$this->load->view('header');
		$this->load->view('archive/archive');
		$this->load->view('footer');
	}

	public function IV($site = false)
	{
		$this->load->view('header', ["scripts" => ["tables"], "stylesheets" => ["tables"]]);
    	switch($site){
			case "videos": $this->load->view('archive/IV/videos'); break;
			case "talks": $this->load->view('archive/IV/talks'); break;
			case "gallery": $this->load->view('archive/IV/gallery'); break;
			case "stats": $this->load->view('archive/IV/stats'); break;
      		default: $this->load->view('archive/IV/home');
    }
		$this->load->view('footer');
	}

	public function V($site = false)
	{
		$this->load->view('header', ["scripts" => ["tables"], "stylesheets" => ["tables"]]);
		switch($site){
			case "videos": $this->load->view('archive/V/videos'); break;
			case "talks": $this->load->view('archive/V/talks'); break;
			case "gallery": $this->load->view('archive/V/gallery'); break;
			case "stats": $this->load->view('archive/V/stats'); break;
			default: $this->load->view('archive/V/home');
		}
		$this->load->view('footer');
	}

	public function VI($site = false)
	{
		$this->load->view('header', ["scripts" => ["tables"], "stylesheets" => ["tables"]]);
		switch($site){
			case "videos": $this->load->view('archive/VI/videos'); break;
			case "talks": $this->load->view('archive/VI/talks'); break;
			case "gallery": $this->load->view('archive/VI/gallery'); break;
			case "stats": $this->load->view('archive/VI/stats'); break;
			default: $this->load->view('archive/VI/home');
		}
		$this->load->view('footer');
	}
}
