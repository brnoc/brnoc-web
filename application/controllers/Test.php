<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->model('talks_model');
    $this->load->database();
	}

	public function index($id = 1)
	{
		print_r($this->talks_model->get_preferences());
	}
}
