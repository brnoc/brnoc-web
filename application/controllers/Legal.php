<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Legal extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index()
	{
		$data['title']="Podmínky";

		$this->load->view('header',$data);
		$this->load->view('legal',$data);
		$this->load->view('footer',$data);

	}
}
