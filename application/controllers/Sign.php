<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
    $this->load->helper('url');
		$this->load->helper('danikuv_form');
    $this->load->model('user_model');
    $this->load->helper('form');
    $this->load->library('form_validation');
		$this->load->library('session');
	}

	public function index()
	{
		$data['title']="Login/Registrace";
		$this->load->view('header',$data);
		$this->load->view('logform',$data);
		$this->load->view('footer',$data);

	}

  public function in($m = false)
  {
    $data['title']="Přihlášení";
		$data['stylesheets'][] = "regform";

		if($m == "rsuc")
			$data['rsuc'] = ["success", "Registrace úspěšná, můžete se přihlásit."];

    $this->form_validation->set_rules('name',"přihlašovací jméno",'required');
    $this->form_validation->set_rules('password',"heslo",'required');
		$this->form_validation->set_message('required',"Prosím zadejte {field}.");


		if($this->form_validation->run() === FALSE){
	    $this->load->view('header',$data);
			$this->load->view('logform',$data);
			$this->load->view('footer',$data);
		}
		else{
			if($this->user_model->signIn()){
				redirect(base_url().'user');
			}
			else{
				$data['message'] = ["error", "Špatné jméno nebo heslo"];
				$this->load->view('header',$data);
				$this->load->view('logform',$data);
				$this->load->view('footer',$data);
			}
		}
  }

	public function terms(){
		if($this->input->post('terms')) return true;
		return false;
	}

	public function s(){
		if(intval($this->input->post('s')) + intval($this->input->post('m')) + intval($this->input->post('l')) > 10)return false;
		return true;
	}

  public function up()
  {
		/*
		$data['title']="Login/Registrace";
		$this->load->view('header',$data);
		$this->load->view('sign/stop',$data);
		$this->load->view('footer',$data);
		return;*/


    $this->form_validation->set_rules('name',"Přihlašovací jméno",'required|min_length[3]|max_length[100]|is_unique[users.name]',["is_unique" => "Toto přihlašovací jméno je již zabrané."]);
		$this->form_validation->set_rules('email',"Email",'required|valid_email|is_unique[users.email]', ["is_unique" => "Tento email je již zabraný."]);
    $this->form_validation->set_rules('password',"Heslo",'required|min_length[5]|max_length[100]');
    $this->form_validation->set_rules('password2',"Heslo znovu",'required|matches[password]',["matches" => "Hesla se musejí shodovat."]);
    $this->form_validation->set_rules('firstname',"Jméno",'');
    $this->form_validation->set_rules('lastname',"Příjmení",'');
		$this->form_validation->set_rules('mystery',"Mystery Room",'required');
		$this->form_validation->set_rules('terms',"Podmínky",'callback_terms',["terms" => "Prosím přečtěte si a akceptujte podmínky."]);
		$this->form_validation->set_rules('s',"Trička",'callback_s',["s"=>"Maximálně si můžeš objednat 3 trička."]);

		$this->form_validation->set_message('required',"Pole {field} je povinné.");
		$this->form_validation->set_message('min_length',"Minimální délka pro {field} je {param} znaků.");
		$this->form_validation->set_message('max_length',"Maximální délka pro {field} je {param} znaků.");
		$this->form_validation->set_message('valid_email,"Prosím zadejte správný email."');



    if($this->form_validation->run() === FALSE){
			$data['stylesheets'][] = "regform";
			$data['title']="Registrace";
			$this->config->load("ponozky");
			$malePonozky = $this->config->item("male_ponozky");
			$velkePonozky = $this->config->item("velke_ponozky");
			$tabulkaPonozky = $this->config->item("tabulka_ponozky");
			$velikostPonozky = $this->config->item("velikost_ponozky");
			$data["malePonozky"] = $malePonozky;
			$data["velkePonozky"] = $velkePonozky;
			$data["tabulkaPonozky"] = $tabulkaPonozky;
			$data["velikostPonozky"] = $velikostPonozky;

      $this->load->view('header',$data);
	 	$this->load->view('regform',$data);
		//$this->load->view('regstop');
      $this->load->view('footer',$data);
    }
    else{
      $this->user_model->signUp();
      redirect(base_url().'sign/in/rsuc');
    }


  }

  public function out()
  {
    $this->user_model->signOut();
    redirect(base_url().'home');
  }
}
