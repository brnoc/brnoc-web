<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mystery extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->model('mystery_model');
	}

	public function index()
	{
		$this->load->model('user_model');
		if(!$this->user_model->perm('mystery')) return redirect(base_url()."sign/in");
		$this->load->view('header',["title"=>"Mystery room"]);
		$this->load->view('mystery',["topics" => $this->mystery_model->get()]);
		$this->load->view('footer');
	}
}
