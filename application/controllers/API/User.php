<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('talks_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
    $this->load->library('form_validation');
		$this->load->database();
  }

  public function vote(){
		if(!isset($_SESSION["user"])){
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
    	exit;
		}
		header("Content-type: application/json");
		$data = $this->user_model->vote(false);
		echo  json_encode(
			[
			"data" => $data,
			"message" => $data?"Vote successful.":"Error",
			"status" => $data?"success":"error",
		]
		);
	}

	public function feedback(){
		if(!isset($_SESSION["user"])){
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
    	exit;
		}
		header("Content-type: application/json");
		$data = $this->user_model->vote(true);
		echo  json_encode(
			[
			"data" => $data,
			"message" => $data?"Vote successful.":"Error",
			"status" => $data?"success":"error",
		]
		);
	}
}
