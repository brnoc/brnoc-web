<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Talks extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('talks_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
    $this->load->library('form_validation');
		$this->load->model('talks_model');
		$this->load->database();
  }

  public function get_votes(){
    header("Content-Type: application/json");
		header("Access-Control-Allow-Origin: *");
		$this->db->select('pref');

		$data = [];
		$result = $this->db->get("users")->result();
		foreach($result as $user)$data[] = json_decode($user->pref);
		echo json_encode($data);
  }

	public function get($id = false){
		header("Content-Type: application/json");
		header("Access-Control-Allow-Origin: *");
		echo json_encode($this->talks_model->get($id));
	}

	public function get_with_pauses(){
		header("Content-Type: application/json");
		header("Access-Control-Allow-Origin: *");
		echo json_encode($this->talks_model->get_with_pauses());
	}

	public function confirm($id = false, $mail = false){
		$status = $this->talks_model->confirm($id, $mail);
		if($status == 401) return header("HTTP/1.1 401 Unauthorized");
		if($status == 0) return http_response_code(400);
		header("Content-Type: application/json");
		echo json_encode("Successfully confirmed talk $id");
	}

	public function reject($id = false, $mail = false){
		$status = $this->talks_model->reject($id, $mail);
		if($status == 401) return header("HTTP/1.1 401 Unauthorized");
		if($status == 0)return http_response_code(400);
		header("Content-Type: application/json");
		echo json_encode("Successfully rejected talk $id");
	}
}
