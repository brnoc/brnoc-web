<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedules extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('talks_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
    $this->load->library('form_validation');
		$this->load->model('talks_model');
		$this->load->database();
  }

	public function get(){
		if($this->user_model->perm('haluzitko')){
			header("Content-Type: application/json");
			header("Access-Control-Allow-Origin: http://localhost:63342");
			header("Access-Control-Allow-Credentials: true");
			echo json_encode($this->db->get("schedules")->result());
		}
		else{
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
			exit;
		}
	}

	public function set(){
		if($this->user_model->perm('haluzitko')){
			header("Content-Type: application/json");
			header("Access-Control-Allow-Origin: http://localhost:63342");
			header("Access-Control-Allow-Credentials: true");

			$id = $this->input->post("id");
			$data = $this->input->post("data");

			if(!$id){
				echo json_encode($this->db->insert("schedules",["data" => $data]));
			}
			else{
				$this->db->where(["id" => $id]);
				$this->db->set(["data" => $data]);
				echo json_encode($this->db->update("schedules"));
			}

		}
		else{
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
			exit;
		}
	}

	public function select($id = false){
		if(!$this->user_model->perm('haluzitko')){
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
			exit;
		}
		if(!$id)$id = $this->input->get("id");
		$this->db->where(["id" => $id]);
		$schedule = json_decode($this->db->get("schedules")->result()[0]->data);

		foreach($schedule->lectures as $key => $value){
			if(isset($value->from)){
				$id = $key;
				$from = intVal($value->from);
				$to = intVal($value->from)+intval($value->duration);
				$room = intVal($value->room)+1;
				$this->db->set([
					"first_block" => $from,
					"last_block" => $to,
					"room" => $room,
				]);
				$this->db->where(["id" => $id]);
				$this->db->update("talks");
				echo "id:$id, from:$from, to:$to, room:$room\n";
			}

		}

		$pauses=[];

		foreach($schedule->pauses as $value){
			$from = intval($value->from);
			$to = intVal($value->from)+intVal($value->duration)-1;
			$pauses[] = [$from,$to];
			echo "from: $from, to: $to\n";
		}
		$this->db->set(["value" => json_encode($pauses)]);
		$this->db->where(["key" => "schedule_pauses"]);
		$this->db->update("config");
	}

	public function shift(){
		if(!$this->user_model->perm('haluzitko')){
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
			exit;
		}
		$this->db->query("UPDATE config SET value = '[[0,1],[6,6],[13,13],[18,19],[26,26],[31,63],[70,70],[77,80],[87,87],[94,94]]' WHERE `key` = 'schedule_pauses';");
		$this->db->query("UPDATE talks SET first_block = first_block-1, last_block = last_block-1 WHERE first_block> 35;");
	}

	public function config(){
		header("Content-Type: application/json");
		header("Access-Control-Allow-Origin: *");
		$this->db->select("value");
		$this->db->where(["key" => "schedule_config"]);
		echo $this->db->get("config")->result()[0]->value;
	}
}
