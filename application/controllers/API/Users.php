<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('talks_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
    $this->load->library('form_validation');
		$this->load->database();
  }

  public function get(){
		if(!$this->user_model->perm('read_user_data')){
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
    	exit;
		}
    header("Content-Type: application/json");
    echo  json_encode(
			[
			"data" => $this->user_model->get(),
			"message" => "Data loaded.",
			"status" => "success",
		]
		);
  }

	public function set(){
		if(!$this->user_model->perm('alter_user_data')){
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
			exit;
		}
		header("Content-Type: application/json");
		echo  json_encode(
			[
			"data" => $this->user_model->set(),
			"message" => "Data set.",
			"status" => "success",
		]
		);
	}

	public function set_role(){
		if(!$this->user_model->perm('alter_user_data')){
			header("HTTP/1.1 401 Unauthorized");
			echo "401 - Unauthorized";
			exit;
		}
		header("Content-Type: application/json");
		echo  json_encode(
			[
			"data" => $this->user_model->set_role(),
			"message" => "Data set.",
			"status" => "success",
		]
		);
	}

}
