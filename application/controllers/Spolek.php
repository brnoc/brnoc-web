<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spolek extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('spolek');
		$this->load->view('footer');
	}
}
