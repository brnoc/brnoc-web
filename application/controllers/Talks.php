<?php
class Talks extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->helper('form');

		$this->load->library('session');
		$this->load->model('talks_model');
		$this->load->model('user_model');
		if(!$this->user_model->perm('talks')){
			redirect(base_url()."user");
			die;
		}
	}

  public function index(){
    $this->view();
  }

	public function new(){
		$data["stylesheets"] = ["newtalk", "regform"];

		$this->load->library('form_validation');
		$this->form_validation->set_rules('title','Název','required');
		$this->form_validation->set_rules('long_desc','Krátký popis','required');
		$this->form_validation->set_rules('short_desc','Dlouhý popis','required');
		$this->form_validation->set_rules('length','Délka','required|integer');

		if($this->form_validation->run()){
			$this->talks_model->create_temp();
			redirect(base_url().'talks/added');
		}
		else{
			$this->load->view('header',$data);
			$this->load->view('talks/new');
			$this->load->view('footer');
		}
  }

	public function added(){
		$this->load->view('header');
		$this->load->view('talks/added');
		$this->load->view('footer');
	}

	public function confirm($id = false){
		if($id){
			$talk = $this->talks_model->confirm($id);
			$this->load->view('header');
			$this->load->view('talks/confirmed',["talk" => $talk]);
			$this->load->view('footer');
		}
		else{
		redirect(base_url()."user");
		}
	}

	public function reject($id = false){
		if($id){
			$talk = $this->talks_model->reject($id);
			$this->load->view('header');
			$this->load->view('talks/rejected',["talk" => $talk]);
			$this->load->view('footer');
		}
		else{
			redirect(base_url()."user");
		}
	}

	public function undo(){
		$this->talks_model->undo();
		redirect(base_url()."user/talks_admin");
	}

	public function edit($id = false){
		if(!$this->talks_model->perm($id)){
			redirect(base_url()."user/talks");
		}
		else{
			$talk = $this->talks_model->get($id)[0];
			$this->load->view('header');
			$this->load->view('talks/edit',["talk" => $talk]);
			$this->load->view('footer');
		}
	}

	public function view($id = false){
		$talk = $this->talks_model->get($id)[0];
		$perm = $this->talks_model->perm($id);
		$this->load->view('header');
		$this->load->view('talks/view',["talk" => $talk, "perm" => $perm]);
		$this->load->view('footer');
	}

	public function update($id = false){
		if(!$this->talks_model->perm($id)){
			redirect(base_url()."user/talks");
			return false;
		}
		else{
			$this->talks_model->update($id);
			redirect(base_url()."talks/view/$id");
		}
	}

	public function delete($id,$true = false){
		if(!$this->talks_model->perm($id)){
			redirect(base_url()."user/talks");
			return false;
		}
		if(!$true){
			$talk = $this->talks_model->get($id)[0];
			$this->load->view('header');
			$this->load->view('talks/delete',["talk" => $talk]);
			$this->load->view('footer');
		}
		else{
			$this->talks_model->delete($id);
			//redirect(base_url()."user/talks_admin");
		}
	}

}
