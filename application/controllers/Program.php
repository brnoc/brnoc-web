<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->model('talks_model');
		$this->load->model('user_model');

	}

	public function index() {
		/*if(isset($_SESSION["user"])){
			$this->feedback();
			return;
		}*/
		//if(!$this->user_model->rank("admin"))$this->wait();
		if(0);
		else if(time() < strtotime("30.11.2019")) $this->list();
		else if(time() < strtotime("9.12.2019")) $this->vote();
		else if(time() < strtotime("15.12.2019")) $this->schedule();
		else $this->feedback();

		/*
		$this->load->view('program/signInPls');
		*/
	}

	public function list(){
		$talks = $this->talks_model->get(false,true);
		$this->load->view('header', ["title" => "Seznam přednášek", "stylesheets" => ["program"]]);
		$this->load->view('program/list', ["talks" => $talks]);
		$this->load->view('footer');
	}

	public function vote(){
		if(!isset($_SESSION["user"]))return redirect(base_url()."program/list");
		$talks = $this->talks_model->get(false,true);
		$this->load->view('header', ["title" => "Hlasování", "stylesheets" => ["program"], "scripts" => ["jquery-1.12.3.min"]]);
		$this->load->view('program/vote',["talks" => $talks]);
		$this->load->view('footer');
	}

	public function feedback(){
		if(!isset($_SESSION["user"]))return redirect(base_url()."sign/in");
		$talks = $this->talks_model->get(false,true);
		$this->load->view('header', ["title" => "Feedback", "stylesheets" => ["program"], "scripts" => ["jquery-1.12.3.min"]]);
		$this->load->view('program/feedback',["talks" => $talks]);
		$this->load->view('footer');
	}

	public function schedule(){
		$data['title']="Program";
		$data['talks']=$this->talks_model->get();
		$data['pauses'] = $this->talks_model->get_pauses();
		$data['stylesheets'] = ["schedule.v1.4"];
		$data['scripts'] = ["scheduler", "jquery-1.12.3.min"];
		$this->load->view('header', $data);
		$this->load->view('program/schedule', $data);
		$this->load->view('footer', $data);
	}

	public function jidelna(){
		$data = [];
		$this->load->view('program/jidelna', $data);
	}

	public function wait(){
		$this->load->view('header');
		$this->load->view('wait');
		$this->load->view('footer');
	}
}
