<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index() {
		$data['title'] = "Kam se to díváš?";
		$data['message'] = "404";
		$data['text'] = "Stránku, kterou hledáš, jsme my nenašli.<br>Zkus kontaktovat <a>feedback</a> nebo zkontroluj URL.";

    $this->output->set_status_header('404');
		$this->load->view('header');
		$this->load->view('not_found', $data);
		$this->load->view('footer');
	}

	public function sign_in() {
		$data['title'] = "Přihlašování není dostupné";
		$data['message'] = "<span class='fas fa-exclamation-triangle'></span>";
		$data['text'] = "Tato funkce zatím není dostupná.<br>Přihlašování bude zahájeno v druhé polovině října.";

		$this->load->view('header');
		$this->load->view('not_found', $data);
		$this->load->view('footer');
	}

	public function program() {
		$data['title'] = "Seznam přednášek není dostupný";
		$data['message'] = "<span class='fas fa-dizzy'></span>";
		$data['text'] = "Tato funkce zatím není dostupná.<br>Seznam přednášek a&nbsp;program budou oznámeny přes náš&nbsp;<a class=\"fab fa-facebook-square\" href=\"https://www.facebook.com/brnoc.cz/\" style=\"border: none\"></a>.";

		$this->load->view('header');
		$this->load->view('not_found', $data);
		$this->load->view('footer');
	}
}
