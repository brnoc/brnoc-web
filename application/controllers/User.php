<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('talks_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
    $this->load->library('form_validation');
		$this->load->model('orders_model');
		$this->load->database();

		$this->form_validation->set_message('required',"Pole {field} je povinné.");
		$this->form_validation->set_message('min_length',"Minimální délka pro {field} je {param} znaků.");
		$this->form_validation->set_message('max_length',"Maximální délka pro {field} je {param} znaků.");
		$this->form_validation->set_message('valid_email,"Prosím zadejte správný email."');
		$this->form_validation->set_message('matches',"Hesla se musejí shodovat.");



	}

	private function getMenu($site = "profile"){

		$sites = [
			["profile","user/profile","Profil"],
			["orders","user/orders","Merch"],
			["talks","user/talks","Přednášky"],
			["admin","user/admin","Administrace"]
		];

		foreach($sites as $key => $val){
			if($this->user_model->perm($val[0]))
				$menu[$key] = [$val[1],$val[2],($site == $val[0])];
		}

		return $menu;
	}

	private function view($d = [],$view = false){
		if(!$view)
			$this->profile();

		$data = $d;
		$data['title']="Uživatelská sekce";
		$data['stylesheets'] = isset($data['stylesheets'])?array_merge($data['stylesheets'],["user","tables.1.0"]):["user","tables.1.0"];
		$data['scripts'] = isset($data['scripts'])?array_merge($data['scripts'],["jquery-1.12.3.min","tables.1.0"]):["jquery-1.12.3.min","tables.1.0"];
		$data['menu'] = $this->getMenu($view);


		$this->load->view('header',$data);
		$this->load->view('user',$data);
		$this->load->view('user/'.$view,$data);
		$this->load->view('footer',$data);

	}

	public function index()
	{
		if(!isset($_SESSION['user'])){
			redirect(base_url().'sign/in');
			return;
		}
		$this->profile();
	}


	/* -------- Form validation -------- */

	public function verify_password($str){

		if(password_verify($str,$_SESSION['user']['password']))
		return TRUE;
		else{
			$this->form_validation->set_message('verify_password','Zadali jste nesprváné heslo.');
			return false;
		}
	}

	public function check_name($str){
		if($str == $_SESSION['user']['name']){
			return true;
		}
		else{
			if($this->db->get_where('users',["name"=>$str])->row_array()){
				$this->form_validation->set_message('check_name','Zadané uživatelské jméno je již zabrané.');
				return false;
			}
				return true;
		}
	}

	public function check_email($str){
		if($str == $_SESSION['user']['email']){
			return true;
		}
		else{
			if($this->db->get_where('users',["email"=>$str])->row_array()){
				$this->form_validation->set_message('check_email','Zadaný email je již zabraný.');
				return false;
			}
				return true;
		}
	}

	/* -------- Update user data -------- */

	public function setData(){
		$this->form_validation->set_rules('name',"Přihlašovací jméno",'required|min_length[3]|max_length[100]|callback_check_name',["check_name" => "Zadané uživatelské jméno je již zabrané."]);
		$this->form_validation->set_rules('email',"Email",'required|valid_email|callback_check_email',["check_email" => "Zadaný email je již zabraný."]);

		if($this->form_validation->run())
		$this->user_model->setData();

		$this->profile();
	}

	public function setPassword(){
		$this->form_validation->set_rules('old',"Staré heslo",'required|callback_verify_password');
		$this->form_validation->set_rules('new',"Nové heslo",'required|min_length[5]|max_length[100]');
    $this->form_validation->set_rules('new2',"Nové heslo znovu",'required|matches[new]');



		$m = ["stylesheets" => ["profile"]];
		if($this->form_validation->run())
			if($this->user_model->setPassword())
				$m = ["message"=>["success","Heslo úspěšeně změněno."],"stylesheets"=>["profile"]];
		$this->view($m,"profile");
	}

	public function img(){

		$m = $this->user_model->setImg();

		$this->profile($m);
	}

	/* -------- Pages -------- */

	public function profile($m = []){
		if(!isset($_SESSION["user"]))return redirect(base_url()."sign/in");
		if($_SESSION['user']['role'] == "unverified")
			$d['message'] = ["alert","Pro možnost přidávání příspěvků prosím ověř svou emailovou adresu.<br><a href=\"".base_url()."user/sendver\">Znovu odeslat ověřovací email</a>"];
			$d['message2'] = $m;
		$d['stylesheets'] = ["profile"];
		$this->view($d,"profile");
	}

	public function sendver(){
		if($this->user_model->sendver())
			$this->view([
				"message" => ["success","Ověřovací email odeslán."],
				"stylesheets" => ["profile"],
			],"profile");
		else
			$this->view([
				"message" => ["error","Ověřovací email se nepodařilo odeslat."],
				"stylesheets" => ["profile"],
			],"profile");
	}

	public function verify($ver,$id){
		if(!$ver || !$id) redirect('home');

		$res = $this->user_model->verify($ver,$id);

		if($res == 1)
			$this->view([
				"message" => ["success","Email úspěšně ověřen."],
				"stylesheets" => ["profile"],
			],"profile");
		else if($res == 2)
			redirect(base_url()."message/emailSuc");
		else if($res == -1)
			redirect(base_url()."message/emailFail");
		else if($res == 0)
			$this->view([
				"message" => ["error","Ověření emailu se nezdařilo."],
				"stylesheets" => ["profile"],
			],"profile");
	}

	public function talks(){

		$this->view([
			"talks" => $this->talks_model->get_my_talks(),
			"temp_talks" => $this->user_model->get_my_temp_talks(),
			"stylesheets" => ["talks"],
		],"talks");
	}


		public function deleteAcc($id = false,$ver = false){
			if(!isset($_SESSION['user']))return redirect(base_url().'user');
			$data['title'] = "Delete account";
			if($id && $id!="false"){
				if(!$this->user_model->rank("Admin"))
				return redirect(base_url().'user');
				if($ver){
					$this->user_model->delete($id);
					return redirect(base_url()."user");
				}
				else{
					$data["id"] = $id;
					$data["user"] = $this->user_model->get_user_data($id);
					if($data["user"])$data["user"] = $data["user"][0];
					else {
						return redirect(base_url()."user");
					}

					$data["stylesheets"] = ["user_management","delete"];
					$this->load->view('header',$data);
					$this->load->view('deleteAcc',$data);
					$this->load->view('footer',$data);
				}
			}
			else{
				if($ver){
					$this->user_model->delete($_SESSION['user']['id']);
					return redirect(base_url().'user');
				}
				else{
					$data["id"] = "false";
					$data["user"] = $this->user_model->get_user_data($_SESSION['user']['id'])[0];
					$data["stylesheets"] = ["user_management","delete"];

					$this->load->view('header',$data);
					$this->load->view('deleteAcc',$data);
					$this->load->view('footer',$data);
				}
			}
		}


		public function setRole($id = false, $role = false){
			if(!$this->user_model->rank('Admin')) return redirect(base_url()."user");
			if(!in_array($role,["Admin","admin","speaker","user","unverified"])) return false;
			$this->user_model->setRole($id,$role);
			return redirect(base_url()."user/users");
		}

		public function forgotten($sent = false){
			$data = [
				"stylesheets" => ["recover"],
				"title" => "Zapomenuté heslo"
			];

			if($sent) $this->user_model->send_recover();

			$this->load->view('header',$data);
			$this->load->view($sent?'recover/sent':'recover/email');
			$this->load->view('footer');
		}

		public function recover(){
			$data = [
				"stylesheets" => ["recover"],
				"title" => "Obnovení hesla",
				"id" => $this->input->get('id'),
				"string" => $this->input->get('key'),
			];

			$this->form_validation->set_rules('pass',"Nové heslo",'required|min_length[5]|max_length[100]');
			$this->form_validation->set_rules('pass_again',"Nové heslo znovu",'required|matches[pass]');

			if($this->form_validation->run()){
				$this->user_model->set_password();
				redirect(base_url().'/user');
			}

			$this->load->view('header',$data);
			$this->load->view('recover/setnew',$data);
			$this->load->view('footer',$data);

		}

		public function setContact(){
			$this->user_model->set_contact();
			redirect(base_url().'user/profile#contactSection');
		}

		public function admin(){
			$this->load->view('header',["stylesheets" => ["user"]]);
			$this->load->view('user',["menu" => $this->getMenu()]);
			$this->load->view('user/admin');
			$this->load->view('footer');
		}


		public function orders($messages = false){
		/*	$this->config->load("ponozky");
			$malePonozky = $this->config->item("male_ponozky");
			$velkePonozky = $this->config->item("velke_ponozky");
			$tabulkaPonozky = $this->config->item("tabulka_ponozky");
			$velikostPonozky = $this->config->item("velikost_ponozky");
			$maxPonozky = $this->config->item("max_ponozky");
			$data["ordered"] = $this->orders_model->get_ordered_socks();
			$data["maxPonozky"] = $maxPonozky;
			$data["malePonozky"] = $malePonozky;
			$data["velkePonozky"] = $velkePonozky;
			$data["tabulkaPonozky"]=$tabulkaPonozky;
			$data["velikostPonozky"] = $velikostPonozky;
			$data["messages"] = $messages;*/


			$this->view(["title" => "Objednávky", "messages" => $messages],"orders");
		}

		public function order(){
			return;
			$m = $this->user_model->order();
			$this->orders([$m]);
		}

		public function newsletter(){
			$data = [
				"title" => "Newsletter",
				"scripts" => ["jquery-1.12.3.min"]
			];
			$this->view($data,"newsletter");
		}

		public function payments(){
			$data = [
				"title" => "Platby",
				"data" => $this->user_model->update_payments(),
			];
			$this->view($data,'payments');
		}

		public function haluzitko(){
			$this->view([
				"stylesheets" => ["haluzitko"],
				"scripts" => ["haluzitko"],
			],'haluzitko');
		}

	}
