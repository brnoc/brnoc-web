<?php
class Orders_model extends CI_Model {
  public function __construct(){
    $this->load->database();
    $this->load->library('session');
  }


  public function get_ordered_socks(){
    $this->db->select("socks");
    $socks = [0,0,0,0,0];
    $data = $this->db->get("users")->result();

    foreach ($data as $value) {
      foreach(json_decode($value->socks) as $key => $val){
        $socks[$key]+=$val;
      }
    }
    return $socks;
  }
}
