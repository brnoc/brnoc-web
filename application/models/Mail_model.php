<?php
class Mail_model extends CI_Model {
  public function __construct(){
    $this->load->database();
    $this->load->library('session');
    $this->load->helper('jmena_helper');
  }


  public function mail($user_id, $subject, $html){
    $this->db->select('email, firstname');
    $this->db->where(['id' => $user_id]);
    $user = $this->db->get('users')->result()[0];
    $mail = $user->email;

    $this->load->library('email');
    $this->email->set_newline("\r\n");
    $this->email->from('pus@brnoc.cz',"Pravoúhlý sněm");
    $this->email->to($mail);
    $this->email->subject("BrNOC: ".$subject);
    $this->email->message($html);

    $this->email->print_debugger();

    $this->email->send();

  }

}
