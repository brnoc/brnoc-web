<?php
class User_model extends CI_Model {
  public function __construct(){
    $this->load->database();
    $this->load->library('session');

  }

  public function random_string($lenght = 30){

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $lenght; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
  }



  public function signIn(){
    $username = $this->input->post('name');
    $password = $this->input->post('password');

    $query = $this->db->get_where('users',['name' => $username]);

    $res = $query->row_array();



    if(password_verify($password,$res['password'])){
      $this->session->set_userdata('user',$res);

      return true;
    }
    return false;
  }

  public function signOut(){
    unset($_SESSION['user']);
  }

  public function signUp(){

    $age = intval($this->input->post('age'));
    if($age < 1 || $age > 3)$age = 1;

    $ord = [
      "S" => intval($this->input->post('s')),
      "M" => intval($this->input->post('m')),
      "L" => intval($this->input->post('l')),
    ];

    $data = [
      'name' => html_escape($this->input->post('name')),
      'password' => password_hash($this->input->post('password'),PASSWORD_DEFAULT),
      'email' => html_escape($this->input->post('email')),
      'firstname' => html_escape($this->input->post('firstname')),
      'lastname' => html_escape($this->input->post('lastname')),
      'role' => 'unverified',
      'img' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAPFBMVEXk5ueutLfn6eqrsbTp6+zg4uOwtrnJzc/j5earsbW0ubzQ09XGysy4vcCorrHa3d7AxcfW2dvO0dTDx8obL96MAAAFWUlEQVR4nO2cWZLjIAxAbYGXgPF6/7sOdJbO5okNIgi3Xs1U/+aVhBAYKAqGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYQ4ISNkOk9az1noaWikh9S/CBGQ7deYkhP3ncH/UMlVHsZRVo6xU+YQVVc0A2UsCaCNOz3a/lmousnaEoilfgvcsubT5OkIjPvj9OPZLprkqp/X0fHacZepfux8ozIb43RxVlVsYbQC3+/04ZhZGWPp9gmV5MllFcU+G3qJYV6l/9mba/XpnxyGTMFYeATzT56HoL5iJYrVxEnxPDolahwhaRerlBlSYYFnWqRX+jxwDBuEliIby1A9690T/yqkhPBTb4Ag6+iG1xyoyeBCeqakGETRKCO1Q7KgqIgmSnTJgQTMsadbTkG7tGZLFRo54gmWpCAYRM4SuP03t8wKEdzMPEByJuIIEy+mMbUhtTsRqZ+4UiaVphdByPxlOqZ0eaZCT1DKSSlP8JKWWpi16klLra/bu4W9BUFoJw4IvaCd9QoYxhqGFkCFEGIZ2IBJqa/BnQ8eJ0IyItX3xCKFSA03QTv4qdOZ8QF38/kJnBQUmjqGiE8NIhnT2TcM/x6yQWuwGBH5RW0OkFrvBhmyYgeHxK83xZ4vjz/jH79qaOIZ0Ou/jr54irYAp7QkffxfjD+xERSmmlHYTo5QaSoXmL+zqywi9t6AUQnc7Bt2Q0HzvwD2J4aC0H+zAT1Nanw9tmqKfVFhoJSnW0dI7Q0INzZk/cGII+dQXsTrjOP7JPdQlFMGDewXuSCQ4Cn9AM6S0MrwH7yQ7ubnwCtpCmKogVrGhtWx6BCVPyZ0sfQDhZhfZOnoB4SNNaoVP+N5yvkKv434hrD8VE+VBeCGkoPY5CBbgr5iHoFP0PASWxV31C8pjMJ7KnN438VhnCJP6R+9D7u1uelJ7+FuAVu0YjaLOaAjesGHcGse+od2prQHQ9Vsc+zHfF82g6D7FUYgxpxL6ChSzWpcUfd3kG78rAFWnTm8shaiXA7wq+APISi9K3HE6qVFXB9G7ABKqYdJz08x6mio4yruXfwgAkA/AQVLUekFbDXruxtEoVdd1af/XyphxafQ0VIVzTf0r/XAxGqZuVOW5trwppe5R2rI24zxVRWaaTm4elei3tW3O33S2+uShCbKYOiP6vYtgYWd/O4FQL7Egh8Zs6kVXNPty1AXZLhxgWESA3s3S6JagpNUbNy+WPkoKRU1SVl2Ne1BBCDORKTwAk0EL371k3ZB4JRpgrqOcEC5/1o5D6mS1y/gY4fulN1NKR7uED6+dHx1VMset2zDBCJUmV0F/elAe0dF8fy8HhjrODfU1x+67+znQjrHq56pi+c3vUlJvfTEfk958K4z7XszHROivVBw5fTtB7xS/cckEvj4CHxTL2F9woPL59onqGPcjDgyJ/ZxizHsYck6ZoVdEvBe/ZUdBsHSniuKEUY4JJsH3xDm0AYaMYJyDN5Bqmn8P/ukwYoL4ipKaIPZYxDgYiw7mOc0Y9ycxwBNM2Gv/F7SXQfDvhyIhRpweNdYLQggIjRFFklXmCka1gRjPy+JRI+QpaUGEhxdkpAeS0Ai+5jYQnSh+CbxtSriOXgmrp6AJrZhWCTJM/eO3EFJsqPajzxw8hCFBRLvXGx1vQ/qF9Iz3Qy8Etn834nntNNZriBHwbcCzCaFvraG9qHjEa4mRUZJ69t8yoxCWYvYwjPOeZSx8PrllM92f2T8QsxqGXgMxxjuBEfF4VwryStJy/4yYV6HxKTU5zfeO3Tv8+aycLux+v92d/c2JU79TkGEYhmEYhmEYhmEYhmGQ+Qfu5VVs9enIIwAAAABJRU5ErkJggg==',
      'age' => $age,
      'orders' => json_encode($ord),
      'school' => html_escape($this->input->post('school')),
      'newsletter' => boolval($this->input->post('newsletter')),
      'disc' => -20
    ];
    $this->db->insert('users',$data);

    $id = intval($this->db->get_where('users',["name" => html_escape($this->input->post('name'))])->result()[0]->id);

    $this->db->query($this->db->update_string('users',["vs" => $id*91+1473],"id = $id"));

    if($topic = $this->input->post('mystery'))
    $this->db->insert('mystery_room',["topic" => $topic, "user" => $id]);

    return $this->sendver(["name"=>html_escape($this->input->post('name')),"mail"=>html_escape($this->input->post('email'))]);

  }

  /* -------- set user data -------- */

  public function setImg(){
    if(!isset($_FILES['img']['tmp_name'])) return ["error", "Chyba při nahrávání obrázku."];
    if($_FILES['img']['error'] == 1) return ["error", "Obrázek nesmí být větší než 1MB."];
    if($_FILES['img']['error']) return ["error", "Chyba při nahrávání obrázku."];
    if($_FILES['img']['tmp_name'] == "") return ["error", "Chyba při nahrávání obrázku."];
    if($_FILES['img']['size'] == 0) return ["error", "Chyba při nahrávání obrázku."];
    if($_FILES['img']['size'] > 1000000) return ["error", "Obrázek nesmí být větší než 1MB."];
    if(!in_array($_FILES['img']['type'],["image/png","image/jpg","image/jpeg","image/svg+xml"])) return ["error", "Obrázek musí být v jednom z formátů: JPG, JPEG, PNG, SVG"];
    $img=(base64_encode(file_get_contents(addslashes($_FILES['img']['tmp_name']))));
    $type=$_FILES['img']['type'];
    $img='data:'.$type.';base64,'.$img;

    $user = $_SESSION['user'];
    $id = $user['id'];

    $data = ['img' => $img];
    $where = "id = $id";



    $this->db->query($this->db->update_string('users',$data,$where));

    $user['img']=$img;
    $this->session->set_userdata('user',$user);


    return ["success","Obrázek úspěšně změněn."];
  }

  public function setData(){
    $user = $_SESSION['user'];
    $id = $user['id'];
    $data = [
      "name" => html_escape($this->input->post('name')),
      "firstname" => html_escape($this->input->post('firstname')),
      "lastname" => html_escape($this->input->post('lastname')),
      "email" => html_escape($this->input->post('email')),
    ];
    $where = "id = $id";
    $this->db->query($this->db->update_string('users',$data,$where));
    $query = $this->db->get_where('users',['id' => $id]);
    $res = $query->row_array();
    $this->session->set_userdata('user',$res);
  }

  public function setPassword($id = false){
    if(!isset($_SESSION['user']))return false;
    $user = $_SESSION['user'];
    $id = $user['id'];
    $data = [
      "password" => password_hash($this->input->post('new'),PASSWORD_DEFAULT),
    ];
    $where = "id = $id";
    $this->db->query($this->db->update_string('users',$data,$where));
    $query = $this->db->get_where('users',['id' => $id]);
    $res = $query->row_array();
    $this->session->set_userdata('user',$res);
    return true;
  }

  public function set_contact(){
    if(!isset($_SESSION['user']))return false;
    $networks = [
      "facebook" => ["facebook.com/","fab fa-facebook-square"],
      "messenger" => ["m.me/","fab fa-facebook-messenger"],
      "instagram" => ["instagram.com/","fab fa-instagram"],
      "twitter" => ["twitter.com/","fab fa-twitter-square"],
      "github" => ["github.com/","fab fa-github-square"],
      "reddit" => ["reddit.com/u/","fab fa-reddit-square"],
      "youtube" => ["youtube.com/user/","fab fa-youtube-square"],
      "telegram" => ["t.me/","fab fa-telegram"],
      "viber" => ["chats.viber.com/", "fab fa-viber"],
      "twitch" => ["twitch.tv/", "fab fa-twitch"],
      "spotify" => ["open.spotify.com/user/", "fab fa-spotify"],
      "pornhub" => ["pornhub.com/user/", "fab fa-pornhub"],
      "vkontakte" => ["vk.com/","fab fa-vk"],
    ];
    $contacts = [];
    foreach ($networks as $key => $value) {
      $name = html_escape($this->input->post($key));
      if(!empty($name))$contacts[$key] = $name;

    }

    $contacts = json_encode($contacts);

    $user = $_SESSION['user'];
    $id = $user['id'];
    $data = [
      "contact" => $contacts,
    ];
    $where = "id = $id";
    $this->db->query($this->db->update_string('users',$data,$where));
    $query = $this->db->get_where('users',['id' => $id]);
    $res = $query->row_array();
    $this->session->set_userdata('user',$res);
    return true;


  }


  /* -------- permission system -------- */
  public function perm($arg){
    if(!isset($_SESSION['user']))return false;
    $user = $_SESSION['user']['role'];
    $roles = [
      "Admin" => 100,
      "admin" => 90,
      "speaker" => 50,
      "User" => 20,
      "user" => 10,
      "Unverified" => 0,
      "unverified" => 0,
    ];
    $permissions = [
      "profile" => 0,
      "talks" => 1,
      "comment" => 0,
      "mystery" => 100,
      "talks_admin" => 90,
      "user_management" => 100,
      "attendance" => 90,
      "orders" => 1,
      "newsletter" => 90,
      "read_user_data" => 90,
      "alter_user_data" => 100,
      "payments" => 90,
      "haluzitko" => 90,
      "admin" => 90,
    ];

    $user = $roles[$user];
    if(!in_array($arg,array_keys($permissions)))return false;

    if($user >= $permissions[$arg]) return true;
    return false;
  }

  public function rank($q){
    if(!isset($_SESSION['user']))return false;

    $roles = [
      "Admin" => 100,
      "admin" => 90,
      "speaker" => 50,
      "User" => 20,
      "user" => 10,
      "Unverified" => 0,
      "unverified" => 0,
    ];

    if(!in_array($q,array_keys($roles)))return false;
    if($roles[$_SESSION['user']['role']]>=$roles[$q]) return true;
    return false;

  }


  /* ------ email verification ------ */

  private function send_verification_email($mail,$ver,$id){
    $this->load->library('email');
    $this->email->set_newline("\r\n");
    $this->email->from('pus@brnoc.cz',"Pravoúhlý sněm");
    $this->email->to($mail);
    $this->email->subject("Ověření uživatelského účtu");
    $this->email->message("
    Pro ověření vašeho uživatelského účtu klikněte <a href=\"".base_url()."user/verify/".$ver."/".$id."\">zde</a>.
    ");
    $status = $this->email->send();
    return $status;
  }

  public function sendver($reg = false){

    if(!isset($_SESSION['user']) && !$reg){
      redirect(base_url().'sign/in');
      return false;
    }
    if(isset($_SESSION['user']))
      if($_SESSION['user']['role']!="unverified"){
        redirect(base_url().'user');
        return false;
      }


    $randomString = $this->random_string();



    if(!$reg){
      $user = $_SESSION['user'];
      $id = $user['id'];

      $data = ['ver' => $randomString];
      $where = "id = $id";

      $this->db->query($this->db->update_string('users',$data,$where));

      $user['ver'] = $randomString;

      $this->session->set_userdata('user',$user);
      return $this->send_verification_email($_SESSION['user']['email'],$randomString,$id);
    }
    else{

      $data = ['ver' => $randomString];
      $name = $reg['name'];

      $user=$this->db->get_where('users',['name' => $name])->row_array();


      $id = $user['id'];

      $where = "id = $id";

      $this->db->query($this->db->update_string('users',$data,$where));
      return $this->send_verification_email($reg['mail'],$randomString,$id);
    }


  }


  public function verify($ver,$id){
    if(!isset($_SESSION['user'])){
      $verDB = $this->db->get_where('users',['id' => $id])->row_array()['ver'];
      if($ver = $verDB){
        $data = ['role' => "user", 'ver' => ""];
        $where = "id = $id";
        $this->db->query($this->db->update_string('users',$data,$where));
        return 2;
      }
      else{
        return -1;
      }
    }
    if($ver == $_SESSION['user']['ver']){
      $user = $_SESSION['user'];
      $id = $user['id'];

      $data = ['role' => "user", 'ver' => ""];
      $where = "id = $id";

      $this->db->query($this->db->update_string('users',$data,$where));

      $user['role'] = "user";
      $user['ver'] = "";

      $this->session->set_userdata('user',$user);

      return 1;
    }
    return 0;
  }

  public function get_user_data($id = false){
    if(!isset($_SESSION['user'])) return false;
    if(!$this->rank('Admin') && $id != $_SESSION['user']['id'])
      return false;
    if(!$id){
      $sql = "SELECT id,name,firstname,lastname,role,email FROM users";
      return $this->db->query($sql)->result();
    }
    else{
      $sql = "SELECT id,name,firstname,lastname,role,email FROM users WHERE id = ?";
      return $this->db->query($sql,[$id])->result();
    }
  }

  public function delete($id){
    if(!isset($_SESSION['user'])) return false;
    if(!$this->rank('Admin') && !$_SESSION['user']['id'] == $id) return false;
    $talks = $this->talks_model->get_my_talks($id);
    $sql = "SELECT id FROM talks WHERE speaker = ?";
    foreach ($this->db->query($sql,[$id])->result() as $post){
      if($this->input->post("$talks->id"))
        echo $this->talks_model->delete($talks->id);
    }
    $sql = "DELETE FROM users WHERE id = ?";
    $this->db->query($sql,[$id]);
    if($id = $_SESSION['user']['id']) unset($_SESSION['user']);
    return true;
  }

  public function setRole($id,$role){
    if(!$this->rank('Admin')) return redirect(base_url()."user");
    if(!in_array($role,["Admin","admin","speaker","user","unverified"])) return false;
    $data = ['role' => $role];
    $where = "id = $id";
    return $this->db->query($this->db->update_string('users',$data,$where));

  }

  public function send_recover(){
    $email = $this->input->post('email');
    $data = $this->db->get_where('users',['email' => $email])->result();
    if($data){
      $id = $data[0]->id;
      $rand = $this->random_string(50);
      $this->db->query($this->db->update_string('users',["ver" => $rand,"ver_exp" => time()+1800],"id = $id"));

      $this->load->library('email');
      $this->email->set_newline("\r\n");
      $this->email->from('pus@brnoc.cz',"Pravoúhlý sněm");
      $this->email->to($email);

      $this->email->subject("Obnovení uživatelského účtu");
      $this->email->message("
      Pro obnovení hesla k vašemu uživatelském účtu klikněte <a href=\"".base_url()."user/recover/?key=".$rand."&id=".$id."\">zde</a>. Odkaz je platný po dobu třiceti minut.
      ");
      return $this->email->send();
    }
  }

  public function set_password(){
    $id = intval($this->input->post('id'));
    $data = $this->db->get_where('users',["id" =>$id])->result();
    if(!$data)return false;
    $data = $data[0];
    $key = $this->input->post('key');

    if(($key != $data->ver) || (time() > $data->ver_exp)) return false;

    $data = [
      "password" => password_hash($this->input->post('pass'),PASSWORD_DEFAULT),
      "ver" => "",
      "ver_exp" => 0,
    ];

    $this->db->query($this->db->update_string('users',$data,"id = $id"));

  }

  public function order(){
    if(!$this->perm('orders'))return;

    $ord = [
      "S" => intval($this->input->post('s')),
      "M" => intval($this->input->post('m')),
      "L" => intval($this->input->post('l')),
    ];

    foreach($ord as $key => $val){
      if($val < 0)$ord[$key]=0;
    }

    if($ord["S"]+$ord["M"]+$ord["L"] > 3)
      return ["type" => "error", "text" => "Můžeš si maximálně 3 trička."];

    $id = $_SESSION['user']['id'];
    $data = json_encode($ord);

    $this->db->where(["id"=>$id]);
    $this->db->set(["orders"=>$data]);
    $this->db->update("users");

    $user = $_SESSION['user'];
    $user['orders'] = $data;
    $this->session->set_userdata(["user" => $user]);

    return ["type" => "success", "text" => "Objednávka byla úspěšně změněna."];
  }


  public function order_cups(){
    if(!$this->perm('orders'))return;
    $id = $_SESSION['user']['id'];

    $c = $this->input->post('cups')? intval($this->input->post('cups')):0;

    $this->db->query($this->db->update_string('users',["cup" => $c],"id = $id"));

    $user = $_SESSION['user'];
    $user['cup'] = $c;
    $this->session->set_userdata(["user" => $user]);

  }

  public function count_all_users(){
    $count = $this->db->query("SELECT COUNT(*) from users;")->result()[0]->{"COUNT(*)"};
    return $count;
  }

  public function send_mail($id, $mail){

  }

  public function get_my_temp_talks(){
    if(!isset($_SESSION['user']['id']))return false;
    if(!$this->perm("talks"))return false;

    return $this->db->get_where("talks",["speaker" => $_SESSION['user']['id'], "status" => "N"])->result();
  }



  public function update_payments(){
    $json = @file_get_contents("https://www.fio.cz/ib_api/rest/periods/FoJyUVWD2DdBvvLzTGqfdKkhw0MvXiTLMzkkEgf9E96CKpesBdN3F95N2orMntK5/2019-10-01/2019-12-31/transactions.json");

    if($json) $data = json_decode($json)->accountStatement->transactionList->transaction;
    else $data = [];
    $transactions = [];
    foreach ($data as $transaction) {
      $transactions[] = [
        "value" => isset($transaction->column1->value)?$transaction->column1->value:0,
        "vs" => isset($transaction->column5->value)?$transaction->column5->value:0,
      ];
    }

    $payments = [];
    foreach($transactions as $transaction){
      $payments[$transaction["vs"]] = isset($payments[$transaction["vs"]])?($payments[$transaction["vs"]]+$transaction["value"]):$transaction["value"];
    }

    $new = [];
    foreach($payments as $vs => $paid){
      $user = $this->db->get_where('users',["vs" => $vs])->result();
      if($user){
        $user = $user[0];
        if($user->paid != $paid){
          $this->db->set(["paid" => $paid]);
          $this->db->where(["id" => $user->id]);
          $this->db->update('users');
          $new[] = [
            "name" => $user->firstname." ".$user->lastname,
            "vs" => $user->vs,
            "value" => $paid - $user->paid,
          ];
        }
      }

    }
    return $new;
  }

  public function get_talks_by_users(){
    $this->db->where(["speaker" => 1]);
    $this->db->select("id");
    $users = $this->db->get("users")->result();
    $return = [];
    foreach ($users as $user) {
      $this->db->where(["speaker" => $user->id]);
      $return[$user->id] = ($talk = $this->db->get("talks")->result())? $talk[0]->title:"";
    }
    return $return;
  }

  public function get_statistics(){
    $this->db->select("created");
    $users = $this->db->get("users")->result();
    $data = [];
    foreach($users as $user){
      $created = substr($user->created,0,10);
      $data[$created] = isset($data[$created])?$data[$created]+1:1;
    }
    $return = [["Den", "Počet registrovaných úživatelů"]];
    foreach ($data as $key => $value) {
      $return[] = [$key, $value];
    }
    return $return;
  }

  //*** API ***//
  function get(){
    $columns = $this->input->get('columns');
    $columns = json_decode($columns);


    $where = $this->input->get('where');
    $where = json_decode($where);


    if(is_array($columns))
      foreach($columns as $col)
        if(is_string($col))
          if(in_array($col,["id","firstname","lastname","email","img","contact","speaker","organizer","orders","paid","attended","created","age","jizdne","vs", "released", "consent", "comment","disc"]))
            $this->db->select($col);

    if(is_array($where))
      foreach($where as $col)
        if(isset($col[0]) && isset($col[1]) && isset($col[2]))
          if(in_array($col[0],["id","firstname","lastname","email","img","contact","speaker","organizer","orders","paid","attended","created","age","jizdne","vs", "released", "consent", "comment","disc"]))
            if(in_array($col[1],["=","<",">","!="]))
              $this->db->where([$col[0]." ".$col[1] => $col[2]]);

    $this->db->from('users');
    return $this->db->get()->result();
  }

  function set(){
    $data = $this->input->post('columns');
    $data = json_decode($data);

    if(is_array($data))
      foreach($data as $col)
        if(isset($col[0]) && isset($col[1]))
          if(in_array($col[0],["id","firstname","lastname","email","img","contact","speaker","organizer","socks","paid","attended","created","age","jizdne","vs","released","consent","comment","disc"]))
            $this->db->set([$col[0] => $col[1]]);
          else echo "unkwnown column";

    $where = $this->input->post('where');
    $where = json_decode($where);

    if(is_array($where))
      foreach($where as $col)
        if(isset($col[0]) && isset($col[1]) && isset($col[2]))
          if(in_array($col[0],["id","firstname","lastname","email","img","contact","speaker","organizer","socks","paid","attended","created","age","jizdne","vs","released","consent","comment","disc"]))
            if(in_array($col[1],["=","<",">","!="]))
              $this->db->where([$col[0]." ".$col[1] => $col[2]]);
            else return "unkwnown operator";
          else return "unkwnown column";
        else return false;
    else return false;


    $sql = $this->db->get_compiled_update('users');
    if($status = $this->db->query($sql))
      return $sql;
    return $status;
  }

  function vote($feedback = false){
    $data = $this->input->post("vote");
    if(!$data)return false;
    if(!$feedback)$this->db->set(["pref" => $data]);
    else $this->db->set(["feedback" => $data]);
    $this->db->where(["id" => $_SESSION["user"]["id"]]);
    $user = $_SESSION["user"];
    if(!$feedback)$user["pref"] = $data;
    else $user["feedback"] = $data;
    $this->session->set_userdata(["user" => $user]);
    print_r($_SESSION['user']);
    return $this->db->update("users");

  }

  function set_role(){
    $role = $this->input->post('role');
    $id = $this->input->post('id');
    if(!in_array($role,["Admin","admin","user","speaker","unverified"])) return "Špatná role!";
    $this->db->set(["role"=>$role]);
    $this->db->where(["id"=>$id]);
    $this->db->update("users");
  }
}
