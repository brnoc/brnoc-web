<?php
class Talks_model extends CI_Model {
  public function __construct(){
    $this->load->database();
    $this->load->library('session');
    $this->load->helper('security');
    $this->load->model('user_model');
    $this->load->model('mail_model');
		$this->load->helper('jmena_helper');
  }

  public function get_my_talks($id = false){
    if(!isset($_SESSION['user'])) return false;
    if(!$id){
      $id = $_SESSION['user']['id'];
    }
    else{
      if(!$this->user_model->rank('Admin'))return false;
    }
    return $this->db->get_where("talks",["speaker"=>$id,"status" => "A"])->result();
  }

  private function get_name_by_id($id){
    $speaker = $this->db->get_where('users',["id" => $id])->row_array();
    if($speaker['firstname'] ||  $speaker['lastname'])
      $speaker = $speaker['firstname']." ".$speaker['lastname'];
    else
      $speaker = $speaker['name'];
    return $speaker;
  }


  private function process($data, $newline = false){
    $return = [];
    foreach ($data as $key => $value) {

      $speaker = $this->db->get_where('users',["id" => $value->speaker])->row_array();
      if($speaker['firstname'] ||  $speaker['lastname'])
        $speaker = $speaker['firstname']." ".$speaker['lastname'];
      else
        $speaker = $speaker['name'];

      $room = $value->room;

      $with = "";
      foreach(json_decode($value->w) as $k => $w){
        $with .= $this->get_name_by_id($w);
        if(count(json_decode($value->w))-1!=$k)$with.=", ";
      }

      $return[$key]=[
        "id" => $value->id,
        "title" => html_escape($value->title),
        "speaker" => $speaker,
        "speaker_id" => $value->speaker,
        "long" => $newline?str_replace("\r\n","<br>",html_escape($value->long_desc)):html_escape($value->long_desc),
        "short" => html_escape($value->short_desc),
        "img" => $value->img,
        "length" => $value->length,
        "subject" => html_escape($value->subject),
        "record" => $value->record,
        "special" => html_escape($value->special),
        "jizdne" => $value->jizdne,
        "pref_time" => $value->pref_time,
        "start" => $value->first_block,
        "end" => $value->last_block,
        "room" => $room,
        "type" => $value->type,
        "timestamp" => $value->timestamp,
        "status" => $value->status,
        "with" => $with,
        "special_room" => $value->special_room,
      ];
    }
    return $return;
  }


  public function get($id = false, $newline = false, $all = false){
    if($id) $this->db->where(["id" => $id]);
    if(!$all) $this->db->where(["status" => "A"]);
    return $this->process($this->db->get('talks')->result(),$newline);
  }

  public function perm($id = false){
    if(!$id) return false;
    if(!isset($_SESSION['user']['id']))return false;
    if($this->user_model->perm('talks_admin')) return true;
    $talk = $this->db->get_where('talks',["id" => $id])->result();
    $id = $_SESSION['user']['id'];
    if($id = $talk['speaker']) return true;
    return false;
  }

  public function create_temp(){
    $this->load->model('user_model');
    if(!$this->user_model->perm('talks')){
      redirect(base_url()."user");
      return false;
    }

    $length = $this->input->post('length');
    $length = intval($length);
    $length = $length - $length%15;
    if($length < 15)$length = 15;
    if($length > 90)$length = 90;

    $pref_time = intval($this->input->post('pref_time'));
    if($pref_time < 1)$pref_time = 1;
    if($pref_time > 3)$pref_time = 3;

    $user_id = $_SESSION['user']['id'];
    $data = [
      "speaker" => $user_id,
      "long_desc" => $this->input->post('long_desc'),
      "short_desc" => $this->input->post('short_desc'),
      "title" => $this->input->post('title'),
      "length" => $length,
      "pref_time" => $pref_time,
      "jizdne" => boolval($this->input->post('jizdne')),
      "subject" => $this->input->post('subject'),
      "special" => $this->input->post('special'),
      "record" => boolval($this->input->post('record')),
    ];

    $this->db->insert('talks',$data);

  }

  public function get_temp(){
    if(!$this->user_model->perm('talks_admin'))return false;

    $this->db->where(["status" => "N"]);
    return $this->process($this->db->get('talks')->result());
  }



  public function update($id = false){
    if(!$this->perm($id))return false;

    $length = $this->input->post('length');
    $length = intval($length);
    $length = $length - $length%15;
    if($length < 15)$length = 15;
    if($length > 90)$length = 90;

    $pref_time = intval($this->input->post('pref_time'));
    if($pref_time < 1)$pref_time = 1;
    if($pref_time > 3)$pref_time = 3;

    $data = [
      "long_desc" => $this->input->post('long_desc'),
      "short_desc" => $this->input->post('short_desc'),
      "title" => $this->input->post('title'),
      "length" => $length,
      "pref_time" => $pref_time,
      "jizdne" => boolval($this->input->post('jizdne')),
      "subject" => $this->input->post('subject'),
      "special" => $this->input->post('special'),
      "record" => boolval($this->input->post('record')),
    ];

    $where = "id = $id";

    $this->db->query($this->db->update_string('talks',$data,$where));
    return true;
  }

  public function delete($id = false){
    if(!$this->perm($id)) return false;

    $this->db->where("id",$id);
    $this->db->select("speaker");
    $data = $this->db->get("talks")->result();


    $this->db->delete('talks', ['id' => $id]);


    if($data){
      $id = $data[0]->speaker;
      if(!$this->db->get_where("talks",["speaker" => $id])->result()){
        $this->db->set("speaker",0);
        $this->db->where("id",$id);
        $this->db->update("users");
      }
    }

  }


  public function get_stats(){


    $talks_temp = $this->db->get_where("talks",["status"=>"N"])->result();
    $data["speakers"] = count($speakers = $this->db->get_where("users",["speaker" => 1])->result());
    $data["talks"] = count($talks = $this->db->get_where("talks",["status" => "A"])->result());
    $data["talk_minutes_confirmed"] = 0; foreach($talks as $talk)$data["talk_minutes_confirmed"] += ($talk->type != "W")?$talk->length:0;
    $data["talk_minutes_all"] = $data["talk_minutes_confirmed"]; foreach($talks_temp as $talk)$data["talk_minutes_all"] += ($talk->type != "W")?$talk->length:0;
    $speakers_temp = [];
    foreach($talks_temp as $talk){
      $speakers_temp[] = $this->db->get_where("users",["id" => $talk->speaker])->result()[0];
    }
    foreach(array_merge($speakers, $speakers_temp) as $speaker){
      $info["talk_minutes_confirmed"] = 0; foreach($talks as $talk)if($speaker->id == $talk->speaker)$info["talk_minutes_confirmed"] += $talk->length;
      $info["talk_minutes_all"] = $info["talk_minutes_confirmed"]; foreach($talks_temp as $talk)if($speaker->id == $talk->speaker)$info["talk_minutes_all"] += $talk->length;
      $data["speaker_info"][$speaker->id] = $info;
    }


    return($data);
  }

  public function get_pauses(){
    $this->db->select("value");
    $this->db->where(["key" => "schedule_pauses"]);
    return $this->db->get("config")->result()[0]->value;
  }

  public function get_pause_titles(){
    $this->db->select("value");
    $this->db->where(["key" => "schedule_pause_titles"]);
    return $this->db->get("config")->result()[0]->value;
  }


  public function get_preferences($feedback = false){
    $this->db->select($feedback?"feedback":"pref");
    $data = $this->db->get('users')->result();
    $this->db->select("id");
    $talk_ids = $this->db->get('talks')->result();

    $sum = [];
    $count = [];

    foreach ($data as $user) {
      foreach (json_decode($feedback?$user->feedback:$user->pref) as $key => $talk) {
        if(isset($sum[$key]) && $talk != null){
          $sum[$key]+=$talk;
          $count[$key]++;
        }
        elseif($talk != NULL){
          $sum[$key] = $talk;
          $count[$key] = 1;
        }
      }
    }

    $preferences = [];
    foreach ($talk_ids as $key => $value) {
      $id = $value->id;
      $preferences[$id] = (isset($sum[$id]))?[
        "sum" => $sum[$id],
        "count" => $count[$id],
      ]:["sum"=>0,"count"=>0];
    }

    return $preferences;
  }



  public function get_with_pauses(){
    $pauses = json_decode($this->get_pauses());
    $titles = json_decode($this->get_pause_titles());
    $talks = $this->get();
    foreach($pauses as $key => $pause){
      $talks[] = [
        "start" => $pause[0],
        "end" => $pause[1],
        "room" => "P",
        "title" => $titles[$key],
      ];
    }

    return $talks;


  }




  /*API*/
  public function confirm($id = false, $mail = false){
    if(!$this->user_model->perm('talks_admin'))return 401;
    if(!$id) return 0;

    $talk = $this->db->get_where('talks',["id" => $id])->result();
    if(!$talk)return 0;
    $user_id = $talk[0]->speaker;
    $user = $this->db->get_where('users',["id"=>$user_id])->result()[0];

    if($user->role == "Admin" || $user->role == "admin")$this->db->set(["speaker" => 1]);
    else $this->db->set(["speaker" => 1, "role" => "speaker"]);


    $this->db->where(["id" => $user_id]);
    $this->db->update('users');

    $this->db->set(["status" => "A"]);
    $this->db->where(["id" => $id]);
    $this->db->update('talks');

    $title = $this->db->get_where('talks',["id"=>$id])->result()[0]->title;

    if(!$mail) return 1;
    $this->mail_model->mail($user_id,"Přednáška schválena",$this->load->view('emails/schvalena_prednaska',
      ["user" => $user,"talk" => $title]
    ,true));
    return 1;
  }

  public function reject($id = false, $mail = false){
    if(!$this->user_model->perm('talks_admin'))return 401;
    if(!$id) return 0;

    $talk = $this->db->get_where('talks',["id" => $id])->result();
    if(!$talk)return 0;
    $user_id = $talk[0]->speaker;
    $user = $this->db->get_where('users',["id"=>$user_id])->result()[0];



    $this->db->set(["status" => "R"]);
    $this->db->where(["id" => $id]);
    $this->db->update('talks');

    $confirmed_talks = $this->db->get_where("talks",["speaker"=>$user_id,"status"=>"A"])->result();

    if(!$confirmed_talks){
      if($user->role == "Admin" || $user->role == "admin")$this->db->set(["speaker" => 0]);
      else $this->db->set(["speaker" => 0, "role" => "user"]);
      $this->db->where(["id" => $user_id]);
      $this->db->update('users');
    }

    $title = $this->db->get_where('talks',["id"=>$id])->result()[0]->title;

    if(!$mail) return 1;
    $this->mail_model->mail($user_id,"Přednáška odmítnuta",$this->load->view('emails/odmitnuta_prednaska',
      ["user" => $user,"talk" => $title]
    ,true));
    return 1;
  }



}
