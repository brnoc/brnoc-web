<?php
class Mystery_model extends CI_Model {
  public function __construct(){
    $this->load->database();
    $this->load->library('session');
  }

  public function get(){
    return $this->db->get("mystery_room")->result();
  }

}
