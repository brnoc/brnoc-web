<?php
function zmena($s,$a,$w){
  if($a != 0) return mb_substr($s,0,($a*-1)).$w;
  if($a == 0) return $s.$w;
}

function zavolejJmeno($jmeno){
  $kon = mb_substr($jmeno,(mb_strlen($jmeno)-3),3);
  if(in_array($kon, array("tek"))) return zmena($jmeno,3,"tku");
  if(in_array($kon, array("něk","nek"))) return zmena($jmeno,3,"nku");
  if(in_array($kon, array("šek"))) return zmena($jmeno,3,"šku");
  $ko = mb_substr($jmeno,(mb_strlen($jmeno)-2),2);
  if(in_array($ko, array("am","an","id","rt","ín","in","or","án","rd","ír","av","er","on","ém","nt","ít","ip","lf","im","ar","op","ub","ym","al","of","il","ef"))) return zmena($jmeno,0,"e");
  if(in_array($ko, array("la","na","ta","ra","ea","ka","sa","da","ma","va","ia","za"))) return zmena($jmeno,1,"o");
  if(in_array($ko, array("eš","ex","ej","is","el","áš","as","ěj","uš","ax","oš"))) return zmena($jmeno,0,"i");
  if(in_array($ko, array("ie","ce","en","er","ly","ří"))) return $jmeno;
  if(in_array($ko, array("ik","ch"))) return zmena($jmeno,0,"u");
  if(in_array($ko, array("ol"))) return zmena($jmeno,0,"o");
  if(in_array($ko, array("dr"))) return zmena($jmeno,1,"ře");
  if(in_array($ko, array("ek"))) return zmena($jmeno,3,"ku");
  if(in_array($ko, array("tr"))) return zmena($jmeno,2,"tře");
  return $jmeno;
}
