-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 25. říj 2018, 20:32
-- Verze serveru: 10.1.33-MariaDB
-- Verze PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `brnoc`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `mystery_room`
--

CREATE TABLE `mystery_room` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `topic` text COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `talks`
--

CREATE TABLE `talks` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `short_desc` varchar(160) COLLATE utf8mb4_czech_ci NOT NULL,
  `long_desc` varchar(1000) COLLATE utf8mb4_czech_ci NOT NULL,
  `speaker` int(11) NOT NULL,
  `img` mediumtext COLLATE utf8mb4_czech_ci NOT NULL,
  `subject` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL,
  `pref_time` int(1) NOT NULL,
  `jizdne` int(1) NOT NULL,
  `length` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record` int(1) NOT NULL,
  `special` text COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `talks_temp`
--

CREATE TABLE `talks_temp` (
  `id` int(11) NOT NULL,
  `speaker` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_czech_ci NOT NULL,
  `short_desc` varchar(160) COLLATE utf8mb4_czech_ci NOT NULL,
  `long_desc` varchar(1000) COLLATE utf8mb4_czech_ci NOT NULL,
  `length` int(11) NOT NULL,
  `img` mediumtext COLLATE utf8mb4_czech_ci NOT NULL,
  `subject` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL,
  `pref_time` int(1) NOT NULL,
  `jizdne` int(1) NOT NULL,
  `special` text COLLATE utf8mb4_czech_ci NOT NULL,
  `record` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `firstname` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `role` varchar(10) COLLATE utf8mb4_czech_ci NOT NULL,
  `contact` varchar(15000) COLLATE utf8mb4_czech_ci NOT NULL DEFAULT '[]',
  `img` mediumtext COLLATE utf8mb4_czech_ci NOT NULL,
  `ver` varchar(50) COLLATE utf8mb4_czech_ci NOT NULL,
  `speaker` tinyint(1) NOT NULL DEFAULT '0',
  `organizer` tinyint(1) NOT NULL DEFAULT '0',
  `socks` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL DEFAULT '0',
  `paid` int(3) NOT NULL DEFAULT '0',
  `attended` int(1) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `age` int(1) NOT NULL,
  `jizdne` int(1) DEFAULT '0',
  `vs` int(11) NOT NULL,
  `ver_exp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `mystery_room`
--
ALTER TABLE `mystery_room`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `talks`
--
ALTER TABLE `talks`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `talks_temp`
--
ALTER TABLE `talks_temp`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `mystery_room`
--
ALTER TABLE `mystery_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `talks`
--
ALTER TABLE `talks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `talks_temp`
--
ALTER TABLE `talks_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
